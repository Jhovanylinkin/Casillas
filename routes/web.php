<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/***********************************************************************************
RCS routes
************************************************************************************/
Route::get('/rcsavance', 'rcs_avanceController@Avance')->middleware('auth');
Route::get('/rcsavanc', 'rcs_avanceController@Tablainit')->middleware('auth');
Route::post('/rcsavanc', 'rcs_avanceController@Tablainit')->middleware('auth');
Route::get('/chartrcs','rcs_avanceController@chart');
Route::get('/rcs_eliminated','rcs_avanceController@eliminated');
Route::get('/tablarcsdeleted','rcs_avanceController@tablarcsdeleted');
Route::post('/tablarcsdeleted','rcs_avanceController@tablarcsdeleted');
Route::get('/rcs_aspirantes','rcs_aspirantesController@init');
Route::get('/rcs_aspirantestable','rcs_aspirantesController@tablainit');
Route::post('/rcs_aspirantesfiltrotabla','rcs_aspirantesController@tablainit');

Route::post('/regionesselect','rcs_avanceController@municipiosRegion')->middleware('auth');
Route::post('/casillasselect','rcs_avanceController@casillasMunicipio')->middleware('auth');
Route::post('/seccionselect','rcs_avanceController@seccionMunicipio')->middleware('auth');
Route::post('/dfflselect','rcs_avanceController@dfdl');
Route::post('/colonias', 'rcs_avanceController@colonias');
Route::post('/addcolonia','rcs_nuevoController@addcolonia');
Route::post('/savecolonia', 'rcs_nuevoController@savecolonia');

Route::get('/rcs_add','rcs_nuevoController@form');
Route::post('/rcs_add','rcs_nuevoController@store');
Route::post('rcs_addAspirante','rcs_aspirantesController@save');
Route::post('/rcs_replace','rcs_nuevoController@form');
Route::post('/rcs_replaceAspirantes','rcs_aspirantesController@init');
Route::post('/rcs_savereplace','rcs_nuevoController@savereplace');
Route::post('/delete_rcs','rcs_nuevoController@delete');
Route::post('/restorercs','rcs_nuevoController@restore');


/***********************************************************************************
RGS routes
************************************************************************************/
Route::get('/rgsavance','rgs_avanceController@avancergs')->middleware('auth');
Route::get('/rgsavancetable', 'rgs_avanceController@Tablainit')->middleware('auth');
Route::post('/rgsavancetable', 'rgs_avanceController@Tablainit')->middleware('auth');
Route::post('/rgs_addcasilla','rgs_avanceController@AddCasilla');
Route::get('/chartrgs','rgs_avanceController@chart');

Route::get('/rgs_add','rgs_nuevoController@form')->middleware('auth');
Route::post('/rgs_add','rgs_nuevoController@store');
Route::post('/rgs_toreplace','rgs_nuevoController@formtoreplace');
Route::post('/rgs_savereplace','rgs_nuevoController@savereplace');
Route::post('/rgs_setcasillas','rgs_nuevoController@setcasillas');


/***********************************************************************************
Planillas routes
************************************************************************************/
Route::get('/planilla','planillasController@init');
Route::get('/planillatablainit','planillasController@Tablainit');
Route::post('/planillatablainit','planillasController@Tablainit');
Route::post('/planillapopup','planillasController@popup');

Route::post('/planillasfiltroEstadoM','planillasController@estadoMunicipio');
Route::post('/partidosslect','planillasController@partido');
Route::post('/planillasdiputados','planillasController@diputados');
Route::post('/cargarplanillas','planillasController@CargarPlanilla');
/***********************************************************************************
Apantallas  routes
************************************************************************************/
Route::get('/pantalla1','pantalla1Controller@init');
Route::get('/pantalla2','pantalla2Controller@init');
Route::get('/pantalla3','pantalla3Controller@init');
Route::get('/pantalla4','pantalla4Controller@init');
Route::get('/pantalla5','pantalla5Controller@init');
/***********************************************************************************
Acta_casillas
************************************************************************************/
Route::get('/acta_casillas','actacasillas@init')->middleware('auth');
Route::post('/acta_casillas/tabla','actacasillas@tablainit')->middleware('auth');
Route::post('/acta_casillas/save','actacasillas@storage');
Route::post('/acta_casillas/delete','actacasillas@delete');
Route::post('/acta_casillas/Municipios','actacasillas@estadoMunicipio');
Route::post('/data.distritos','actacasillas@diputados');
/***********************************************************************************
PREP
************************************************************************************/
Route::get('/prep_grafica','prep_grafica@init')->middleware('auth');
Route::post('/prep_grafica/chart','prep_grafica@graph');
Route::post('/prep_grafica/tabla','prep_grafica@tabla');
Route::post('/prep_grafica/municipios','prep_grafica@estadoMunicipio');
Route::post('/prep_grafica/diputados','prep_grafica@diputados');
/***********************************************************************************
Resultado casillas
************************************************************************************/
Route::get('/casillas_resultados','casillas_resultadosController@init')->middleware('auth');
Route::post('/casillas_resultados/Casillas','casillas_resultadosController@casillas')->middleware('auth');
Route::post('/casillas_resultados/Municipios','casillas_resultadosController@getMunicipioTipoElección')->middleware('auth');
Route::post('/casillas_resultados/partidos','casillas_resultadosController@partidos')->middleware('auth');
Route::post('/casillas_resultados/Distritos','casillas_resultadosController@Distritos');
Route::post('/casillas_resultados/storageResults','casillas_resultadosController@storageResults')->middleware('auth');
/***********************************************************************************
App routes
************************************************************************************/
Auth::routes();
Route::get('/home', 'HomeController@graph')->name('home');
