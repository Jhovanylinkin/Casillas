"use strict";

var _optionsConocimiento;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var optionsConocimiento = (_optionsConocimiento = {
  chart: {
    renderTo: "graphPrep",
    spacingBottom: 20,
    type: "column",
    options3d: {
      enabled: true,
      alpha: 10,
      beta: 0,
      depth: 50
    }
  },
  title: {
    text: ""
  },
  credits: {
    enabled: false
  },
  xAxis: {
    categories: [],
    labels: {
      style: {
        fontFamily: "arial",
        fontSize: "0.8rem",
        align: "center",
        verticalAlign: "middle",
        fontWeight: "bold"
      },
      useHTML: true,
      rotation: 0,
      formatter: function formatter() {
        return "<div class=\"text\"> " + this.value.siglas + " </div><img src=\"" + this.value.imagen + "\" width=\"25px\" height=\"auto;\" />";
      }
    }
  },
  yAxis: {
    title: {
      text: ""
    }
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true
      }
    }
  }
}, _defineProperty(_optionsConocimiento, "credits", {
  enabled: false
}), _defineProperty(_optionsConocimiento, "tooltip", {
  // useHTML: true,
  // formatter: function () {
  //   return '<center>' + '<b>' + this.point.name + ': ' + '</b>' + '</font>' + this.point.y + '<br/><img src="logos_partidos/' + this.point.imagen + '" width="30px" height="30px" /></center>';
  // } ,
}), _defineProperty(_optionsConocimiento, "series", []), _optionsConocimiento);

function postChart() {
  $.ajax({
    type: "post",
    url: "/prep_grafica/chart",
    dataType: "json",
    data: {
      TipoEleccion: $("#TipoEleccion").val(),
      Municipio: $("#Municipio").val()
    },
    success: function success(json) {
      if (json.length != 0) {
        optionsConocimiento.xAxis.categories = json[0]["data"];
        optionsConocimiento.series[0] = json[1];
        var chartConocimiento = new Highcharts.Chart(optionsConocimiento);
      }
    },
    async: false
  });
}