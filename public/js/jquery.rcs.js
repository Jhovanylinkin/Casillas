//funcion para realzar el submit en el formulario de rcs
$(document).ready(function () {
    $('.datepicker').datepicker();
    var urlZ = $(location).attr('href').split('rcsa')[1];
    if (urlZ == 'vance') {
        get_datosTablaIni();
    }
    $(document).on('submit', '#newrcs', function (e) {
        e.preventDefault();
        if ($('#CASILLASselect').val() == 1) {
            $.ajax({
                type: 'POST',
                url: '/rcs_addAspirante',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                timeout: 60000,
                success: function success(data) {
                    parent.bootbox.alert('<strong>Perfecto!</strong><br><br><pre>' + data.success + '</pre>');
                    clearForm();
                } });
        } else {
            $.ajax({
                type: 'POST',
                url: '/rcs_add',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                timeout: 60000,
                success: function success(data) {
                    parent.bootbox.alert('<strong>Perfecto!</strong><br><br><pre>' + data.success + '</pre>');
                    clearForm();
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    parent.bootbox.alert('<strong>Ocurri\xF3 un error.</strong><br><br><pre>' + jqXHR.responseJSON.message + '</pre>');
                }
            });
        }
    });
    $(document).on('submit', '#replacercs', function (e) {
        e.preventDefault();
        if ($('#CASILLASselect').val() == 1) {
            bootbox.alert('<h1>Ya se encuentra registrado en aspirantes.</h1>');
        }
        if ($('#aspitante').val() == 1 && $('#CASILLASselect').val() != 1) {
            $.ajax({
                type: 'POST',
                url: '/rcs_add',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                timeout: 60000,
                success: function success(data) {
                    parent.bootbox.alert('<strong>Perfecto!</strong><br><br><pre>' + data.success + '</pre>');
                    clearForm();
                }
            });
        } else {
            $.ajax({
                type: 'POST',
                url: '/rcs_savereplace',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                timeout: 60000,
                success: function success(data) {
                    parent.bootbox.alert('<strong>Mensage</strong><br><br><pre>' + data.success + '</pre>');
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    parent.bootbox.alert('<strong>Ocurri\xF3 un error.</strong><br><br><pre>' + jqXHR.responseJSON.message + '</pre>');
                }
            });
        }
    });

    $(document).on('click', '#abrir-filtro', function (e) {
        var formulariobusqueda = document.getElementById('form-filtro');
        var buttoncerrar = document.getElementById('cerrar-filtro');
        var btnbusqueda = document.getElementById('busqueda-filtro');
        formulariobusqueda.style.display = "";
        buttoncerrar.style.display = "";
        btnbusqueda.style.display = "";
        var button = this;
        button.style.display = "none";
    });
    $(document).on('click', '#cerrar-filtro', function (e) {
        var formulariobusqueda = document.getElementById('form-filtro');
        var buttonabrir = document.getElementById('abrir-filtro');
        var btnbusqueda = document.getElementById('busqueda-filtro');
        buttonabrir.style.display = "";
        formulariobusqueda.style.display = "none";
        var button = this;
        button.style.display = "none";
        btnbusqueda.style.display = "none";
    });
    var regiones = $('#REGIONselect');
    regiones.on('change', function () {
        if (regiones.val() !== '-1' && regiones.val() !== '') {
            $.ajax({
                url: '/regionesselect',
                type: 'POST',
                dateType: 'json',
                data: { regionID: regiones.val() }
            }).done(function (response) {
                var $municipios = $('#MUNICIPIOselect');
                if (response.status == 'success') {
                    $municipios.html(response.html);
                }
            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        }
    });
    var seccion = $('#seccionIDD');
    seccion.on('change', function () {
        if (seccion.val() !== '-1' && seccion.val() !== '') {
            $.ajax({
                url: '/casillasselect',
                type: "POST",
                dateType: 'json',
                data: { casillaID: seccion.val() }
            }).done(function (response) {
                var $municipios = $('#CASILLASselect');
                if (response.status == 'success') {
                    $municipios.html(response.html);
                }
            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        }
    });

    var municipio = $('#MUNICIPIOselect');
    municipio.on('change', function () {
        if (municipio.val() !== '-1' && municipio.val() !== '') {
            $.ajax({
                url: '/seccionselect',
                type: "POST",
                dateType: 'json',
                data: { municipioID: municipio.val() }
            }).done(function (response) {
                var $seccionesSelect = $('#seccionIDD');
                if (response.status == 'success') {
                    $seccionesSelect.html(response.html);
                }
            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        }
    });
    municipio.on('change', function (event) {
        if (municipio.val() !== '-1' && municipio.val() !== '') {
            $.ajax({
                url: '/colonias',
                type: "POST",
                dateType: 'json',
                data: { municipioID: municipio.val() }
            }).done(function (response) {
                var $Colonias = $('#Colonias');
                if (response.status == 'success') {
                    $Colonias.html(response.html);
                }
            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        }
    });
    var SeccionforDFDL = $('#seccionIDD');
    SeccionforDFDL.on('change', function (e) {
        if (SeccionforDFDL.val() !== '-1' && SeccionforDFDL.val() !== '') {
            $.ajax({
                url: '/dfflselect',
                type: "POST",
                dateType: 'json',
                data: { SECCION: SeccionforDFDL.val() }
            }).done(function (response) {
                $('#DFederal').val(response.df.DISTRITO);
                $('#DLocal').val(response.df.DL);
            }).fail(function (jqXHR, textStatus, error) {
                parent.bootbox.alert('<strong>Ocurri\xF3 un error.</strong><br><br><pre>' + error + '</pre>');
            });
        }
    });

    $('#Colonias').on('change', function (event) {
        var x = $('#Colonias');
        if (x.val() == 1) {
            f_popup_info('', '/addcolonia', 'Agregrar Colonia');
        }
    });

    $(document).on('submit', '#form-filtro', function (e) {
        var $datosTabla = $('#datosTablavancercs');
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/rcsavanc',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function success(data) {
                $datosTabla.html(data);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                parent.bootbox.alert('<strong>Ocurri\xF3 un error.</strong><br><br><pre>' + jqXHR.responseJSON.message + '</pre>');
            }
        });
    });
});
function get_datosTablaIni() {
    var $datosTabla = $('#datosTablavancercs');
    $.ajax({
        url: '/rcsavanc',
        type: "GET",
        dateType: 'json',
        data: { sid: Math.random() },
        success: function success(data) {
            $datosTabla.html(data);
        }

    });
}
function f_popup_info(data, url, title) {
    //Ver planilla 
    $("#ModalTitleInfo").html(title);
    $.ajax({
        type: 'POST',
        url: url,
        cache: false,
        data: { ID: data },
        async: true,
        success: function success(datos) {
            $("#ModalBody").html(datos);
            $('#ModalInfo').modal();
        },
        timeout: 60000,
        error: function error(XMLHttpRequest, textStatus, errorThrown) {
            parent.bootbox.alert("<strong>Ocurrió un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
        }
    });
}
function clearForm() {
    var urlxD = $(location).attr('href').split('rcs')[1];
    if (urlxD == '_add') {
        document.getElementById('newrcs').reset();
    }
}