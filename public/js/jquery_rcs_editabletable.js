$(document).ready(function () {
    $('#tablarcs').Tabledit({
        url: '/wismichu',
        editButton: false,
        deleteButton: false,
        hideIdentifier: true,
        columns: {
            identifier: [[5, 'INEP'], [5, 'INEP']],
            editable: [
                [0, 'REGION'],
                [1, 'MUNICIPIO'],
                [3, 'SECCION'],
                [4, 'CASILLA'],
                [5, 'INEP'],
                [6, 'NOMBREP'],
                [7, 'INES'],
                [8, 'NOMBRES']
            ]
        },
        onSuccess: function (data) {
            console.log(data);
        }
    });
});