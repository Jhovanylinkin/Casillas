
var $datosTabla = $("#datosTabla");

$(document).ready(function () {
    get_datosTablaIni();
    $('body').on('click', '.pagination a', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];
        get_datosTabla(page);
        window.history.pushState("", "", url);
    });
});

function get_datosTabla(page) {
    $.ajax({
        url: '/data.temas-tabla?page=' + page,
        type: "POST",
        dateType: 'json',
        data: { sid: Math.random() }

    }).done(function (response) {
        if (response.status == 'success') {
            $datosTabla.html(response.html);
        }
    }).fail(function (jqXHR, textStatus, error) {
        console.log("Post error: " + error);
    });
}

function get_datosTablaIni() {
    $.ajax({
        url: '/data.temas-tabla',
        type: "POST",
        dateType: 'json',
        data: { sid: Math.random() }

    }).done(function (response) {
        if (response.status == 'success') {
            $datosTabla.html(response.html);
        }
    }).fail(function (jqXHR, textStatus, error) {
        console.log("Post error: " + error);
    });
}

function f_popup_info(uid, url, title) {
    //Ver planilla 
    $("#ModalTitleInfo").html(title);
    $.ajax({
        type: "GET",
        //url: "index.php?sid="+Math.random(),
        url: url,
        cache: false,
        data: { sid: Math.random(), temaID: uid },
        async: true,
        //beforeSend: function(objeto){
        //alert("Adiós, me voy a ejecutar");

        //},
        success: function success(datos) {
            //alert("Datos Info"+datos);

            $("#ModalBody").html(datos);
            $('#ModalInfo').modal();
        },
        timeout: 60000,
        error: function error(XMLHttpRequest, textStatus, errorThrown) {
            bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
        }

    });
}
function f_mensaje_sistema(mensaje) {
    bootbox.alert("<strong>Mensaje del Sistema</strong><br><br><pre>" + mensaje + "</pre>");
}

function f_delete_row(uid, url, title) {
    //eliminar registro
    bootbox.dialog({
        message: "Desea usted eliminar el Tramite:<strong>" + title + "</strong>",
        title: "Mensaje del Sistema",
        buttons: {
            danger: {
                label: "Cancelar",
                className: "red",
                callback: function callback() {
                    //alert("uh oh, look out!");
                    bootbox.hideAll();
                }
            }, success: {
                label: "Eliminar",
                className: "green-haze",
                callback: function callback() {
                    //alert("great success");
                    $.ajax({
                        type: "POST",
                        url: url,
                        cache: false,
                        data: { temaID: $.trim(uid) },
                        async: true,
                        //beforeSend: function(objeto){
                        //alert("Adiós, me voy a ejecutar");

                        //},
                        success: function success(datos) {
                            //alert("Datos Info"+datos);
                            //bootbox.hideAll();
                            if ($.trim(datos) == "Ok") {

                                get_datosTablaIni();
                                bootbox.hideAll();
                            } else {
                                bootbox.hideAll();
                                bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>" + html + "</pre>");
                            }
                        },
                        timeout: 60000,
                        error: function error(XMLHttpRequest, textStatus, errorThrown) {
                            bootbox.hideAll();
                            bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
                        }

                    });
                }
            }
        }
    });
}