'use strict';
var FormValidation1 = function () {

    // basic validation
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = parent.$('#form1');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {

                },
                rules: {
                   idCobertura: {
                        required: true
                    },
                    NombreTema: {
                        required: true
                    },
                    DescripcionTema: {
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");  
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    
                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                }

                
            });

            jQuery.validator.addClassRules("RangoIni", {
                required: true       
            });

    }


    var handleSubmit1 = function() {

            var formOpciones = {
                beforeSubmit: function()
                {

                    if(!parent.$('#form1').valid())
                    {
                        
                        parent.bootbox.alert("<strong>Cometio un error.</strong><br><br><pre>Verifique la información y vuelva a intentarlo.</pre>");
                        return false;
                    }
                    else {
                        
                        $("#btnSave").attr('disabled', 'true');
                    }
                },
                url: '/temas/'+$('#Act').val(),
                type: 'post',
                data: $('#form1').serialize(),
                success: function(datos)
                {
                    
                    //alert(datos)
                    $("#btnSave").removeAttr('disabled');            
                    //$('#frmCaptura').trigger("reset");
                    if($.trim(datos) == "Ok")
                    {           
                        //alert("Enviar A Guardar")
                        $.ajax({ 
                            url:'/data.temas-tabla',
                            type: "POST",
                            dateType:'json',
                            data:({sid:Math.random()})

                        })
                        .done(function(response){
                            if(response.status=='success'){
                                $("#datosTabla").html(response.html)
                            }
                        }).fail(function (jqXHR, textStatus, error) {
                            console.log("Post error: " + error);
                        });

                        parent.$('#ModalInfo').modal('toggle');
                    }else 
                    {
                        parent.bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>"+datos+"</pre>");
                    }
                    

                    
                    
                },
                timeout:60000,
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $("#btnSave").removeAttr('disabled');
                    parent.bootbox.alert("<strong>Ocurrio un error de Network. Intentalo de nuevo.</strong><br><br><pre>"+errorThrown+"</pre>");
                }
            };
            
            $('#form1').ajaxForm(formOpciones);

    }


    

    return {
        //main function to initiate the module
        init: function () {
            handleValidation1();
            handleSubmit1();
        }

    };

}();

$(document).ready(function(){
    FormValidation1.init();
});