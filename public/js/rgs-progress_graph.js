var a = void 0;
$.ajax({
    type: 'GET',
    url: '/chartrgs',
    contentType: false,
    cache: false,
    processData: false,
    timeout: 60000,
    success: function success(data) {
        a = data;
    },
    async: false
});
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Avance Estructura Electoral'
    },
    xAxis: {
        categories: ['Reclutados', 'Capacitados', 'Encuadrados']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total avance'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: Highcharts.theme && Highcharts.theme.textColor || 'gray'
            }
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: Highcharts.theme && Highcharts.theme.background2 || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: Highcharts.theme && Highcharts.theme.dataLabelsColor || 'white'
            }
        }
    },
    series: [{
        name: 'Capturado',
        data: [a.Reclutados, a.Capacitados, a.Encuadrados]
    }, {
        name: 'Faltante',
        data: [a.TotalRGS - a.Reclutados, a.TotalRGS - a.Capacitados, a.TotalRGS - a.Encuadrados]
    }]
});