$(document).ready(function () {
    var pagetotable = 1;
    var urlZ = $(location).attr('href').split('rgsa')[1];
    if (urlZ == 'vance') {
        get_datosTablaIni();
    }
    $(document).on('click', '#abrir-filtro', function (e) {
        var formulariobusqueda = document.getElementById('form-filtro');
        var buttoncerrar = document.getElementById('cerrar-filtro');
        var btnbusqueda = document.getElementById('busqueda-filtro');
        formulariobusqueda.style.display = "";
        buttoncerrar.style.display = "";
        btnbusqueda.style.display = "";
        var button = this;
        button.style.display = "none";
    });
    $(document).on('click', '#cerrar-filtro', function (e) {
        var formulariobusqueda = document.getElementById('form-filtro');
        var buttonabrir = document.getElementById('abrir-filtro');
        var btnbusqueda = document.getElementById('busqueda-filtro');
        buttonabrir.style.display = "";
        formulariobusqueda.style.display = "none";
        var button = this;
        button.style.display = "none";
        btnbusqueda.style.display = "none";
    });
    var regiones = $('#REGIONselect');
    regiones.on('change', function () {
        if (regiones.val() !== '-1' && regiones.val() !== '') {
            $.ajax({
                url: '/regionesselect',
                type: "POST",
                dateType: 'json',
                data: { regionID: regiones.val() }
            }).done(function (response) {
                var $municipios = $('#MUNICIPIOselect');
                if (response.status == 'success') {
                    $municipios.html(response.html);
                }
            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        }
    });
    var municipio = $('#MUNICIPIOselect');
    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        var page = $(this).attr('href').split('?page=')[1];
        var $datosTabla = $('#datosTablavancergs');
        var $filtro = $('#form-filtro');
        var XS = new FormData($filtro);
        var NOMBRE = $('#NOMBRE').val();
        var INE = $('#INEinput').val();
        var SECCION = $('#SECCIONinput').val();
        var REGION = $('#REGIONselect').val();
        var MUNICIPIO = $('#MUNICIPIOselect').val();
        var CASILLA = $('#CASILLASselect').val();
        var RADIO = $('input[name = radio]:checked').val();
        pagetotable = page;
        $.ajax({
            type: 'post',
            url: '/rgsavancetable?page=' + page + '&NOMBRE=' + NOMBRE + '&INE=' + INE + '&SECCION=' + SECCION + '&REGION=' + REGION + '&MUNICIPIO=' + MUNICIPIO + '&CASILLA=' + CASILLA + '&radio=' + RADIO,
            data: new FormData($filtro),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function success(data) {
                $datosTabla.html(data);
            },
            error: function error(XMLHttpRequest, textStatus, errorThrown) {
                bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
            }

        });
    });
    $(document).on('submit', '#newrgs', function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/rgs_add',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function success(data) {
                bootbox.alert('<strong>Perfecto!</strong><br><br><pre>' + data.success + '</pre>');
                clearForm();
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                bootbox.alert('<strong>Ocurrio un error.</strong><br><br><pre>' + jqXHR.responseJSON.message + '</pre>');
            }
        });
    });
    $(document).on('submit', '#replacergs', function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/rgs_savereplace',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function success(data) {
                bootbox.alert('<strong>Mensage</strong><br><br><pre>' + data.success + '</pre>');
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                bootbox.alert('<strong>Ocurrio un error.</strong><br><br><pre>' + jqXHR.responseJSON.message + '</pre>');
            }
        });
    });
    $(document).on('submit', '#form-filtro', function (e) {
        var $datosTabla = $('#datosTablavancergs');
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/rgsavancetable',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function success(data) {
                $datosTabla.html(data);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                bootbox.alert('<strong>Ocurrio un error.</strong><br><br><pre>' + jqXHR.responseJSON.message + '</pre>');
            }
        });
    });

    var Sexo = $('#asdsexo');
    if (Sexo.val() == "M") {
        Sexo.text('Mujer');
    } else {
        Sexo.text('Hombre');
    }

    var a = true;
    $('#CASILLASselect').on('click', function CASILLAinit() {
        if (a) {
            a = false;
            $.ajax({
                url: '/casillasselect',
                type: "POST",
                dateType: 'json',
                data: { casillaID: municipio.val() }
            }).done(function (response) {
                var $municipios = $('#CASILLASselect');
                if (response.status == 'success') {
                    $municipios.html(response.html);
                }
            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        }
    });
    var b = true;
    $('#seccionIDD').on('click', function SECCIONinit() {
        if (b) {
            b = false;
            $.ajax({
                url: '/seccionselect',
                type: "POST",
                dateType: 'json',
                data: { municipioID: municipio.val() }
            }).done(function (response) {
                var $seccionesSelect = $('#seccionIDD');
                if (response.status == 'success') {
                    $seccionesSelect.html(response.html);
                }
            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        }
    });
    municipio.on('change', function (event) {
        if (municipio.val() !== '-1' && municipio.val() !== '') {
            $.ajax({
                url: '/colonias',
                type: "POST",
                dateType: 'json',
                data: { municipioID: municipio.val() }
            }).done(function (response) {
                var $Colonias = $('#Colonias');
                if (response.status == 'success') {
                    $Colonias.html(response.html);
                }
            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        }
    });
    municipio.on('change', function () {
        if (municipio.val() !== '-1' && municipio.val() !== '') {
            $.ajax({
                url: '/seccionselect',
                type: "POST",
                dateType: 'json',
                data: { municipioID: municipio.val() }
            }).done(function (response) {
                var $seccionesSelect = $('#seccionIDD');
                if (response.status == 'success') {
                    $seccionesSelect.html(response.html);
                }
            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        }
    });
    var SeccionforDFDL = $('#seccionIDD');
    SeccionforDFDL.on('change', function (e) {
        if (SeccionforDFDL.val() !== '-1' && SeccionforDFDL.val() !== '') {
            $.ajax({
                url: '/dfflselect',
                type: "POST",
                dateType: 'json',
                data: { SECCION: SeccionforDFDL.val() }
            }).done(function (response) {
                $('#DFederal').val(response.df.DISTRITO);
                $('#DLocal').val(response.df.DL);
            }).fail(function (jqXHR, textStatus, error) {
                bootbox.alert('<strong>Ocurrio un error.</strong><br><br><pre>' + error + '</pre>');
            });
        }
    });
    $('#Colonias').on('change', function (event) {
        var x = $('#Colonias');
        if (x.val() == 1) {
            f_popup_info('', '/addcolonia', 'Agregrar Colonia');
        }
    });
    $(document).on('hidden.bs.modal', '#ModalInfo', function () {
        var page = pagetotable;
        var $datosTabla = $('#datosTablavancergs');
        var $filtro = $('#form-filtro');
        var XS = new FormData($filtro);
        var NOMBRE = $('#NOMBRE').val();
        var INE = $('#INEinput').val();
        var SECCION = $('#SECCIONinput').val();
        var REGION = $('#REGIONselect').val();
        var MUNICIPIO = $('#MUNICIPIOselect').val();
        var CASILLA = $('#CASILLASselect').val();
        var RADIO = $('input[name = radio]:checked').val();
        $.ajax({
            type: 'post',
            url: '/rgsavancetable?page=' + page + '&NOMBRE=' + NOMBRE + '&INE=' + INE + '&SECCION=' + SECCION + '&REGION=' + REGION + '&MUNICIPIO=' + MUNICIPIO + '&CASILLA=' + CASILLA + '&radio=' + RADIO,
            data: new FormData($filtro),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function success(data) {
                $datosTabla.html(data);
            },
            error: function error(XMLHttpRequest, textStatus, errorThrown) {
                bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
            }

        });
    });
});

function get_datosTablaIni() {
    var $datosTabla = $('#datosTablavancergs');
    $.ajax({
        url: '/rgsavancetable',
        type: "GET",
        dateType: 'json',
        data: { sid: Math.random() },
        success: function success(data) {
            $datosTabla.html(data);
        }
    });
}
function f_popup_info(data, url, title) {
    //Ver planilla 
    $("#ModalTitleInfo").html(title);
    $.ajax({
        type: 'POST',
        url: url,
        cache: false,
        data: { ID: data },
        async: true,
        success: function success(datos) {
            $("#ModalBody").html(datos);
            $('#ModalInfo').modal();
        },
        timeout: 60000,
        error: function error(XMLHttpRequest, textStatus, errorThrown) {
            bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
        }
    });
}
function clearForm() {
    var urlxD = $(location).attr('href').split('rgs')[1];
    if (urlxD == '_add') {
        document.getElementById('newrgs').reset();
    }
}