$(document).ready(function () {
    $(document).on('submit', '#form-filtro-aspirantes', function (e) {
        var $datosTabla = $('#aspirantesTable');
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/rcs_aspirantesfiltrotabla',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function success(data) {
                $datosTabla.html(data);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                parent.bootbox.alert('<strong>Ocurri\xF3 un error.</strong><br><br><pre>' + jqXHR.responseJSON.message + '</pre>');
            }
        });
    });
});
get_datosTablaInit();
function get_datosTablaInit() {
    var $datosTabla = $('#aspirantesTable');
    $.ajax({
        url: '/rcs_aspirantestable',
        type: "GET",
        dateType: 'json',
        data: { sid: Math.random() },
        success: function success(data) {
            $datosTabla.html(data);
        }

    });
}