'use strict';

var Vars = {
  Estado: 11
};

$(document).ready(function () {
  console.log(Vars.Estado);

  $("#actacasillasForm").on("submit", function (ev) {
    ev.preventDefault();
    $.ajax({
      type: "POST",
      url: "/acta_casillas/save",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      timeout: 60000,
      success: function success(data) {
        Glow("" + data.message, "success");
        get_datosTablaIni();
      },
      error: function error(data) {
        Glow("[" + data.status + "] El partido ya se encuantra registrado", "danger");
      }
    });
  });

  $("#TipoEleccion").on("change", function (ev) {
    if ($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 4) {
      PresidenteRepublicaFiltro();
    }
    if ($(this).val() == 3) {
      $("#labelC").text("Distritos federales");
      rollbackfilter();
      getDFLC("DIPUTADO FEDERAL", Vars.Estado);
    }
    if ($(this).val() == 5) {
      $("#labelC").text("Distritos locales");
      rollbackfilter();
      getDFLC("DIPUTADO LOCAL", Vars.Estado);
    }
    if ($(this).val() == 6) {
      $("#labelC").text("Municipio");
      rollbackfilter();
      getMunicipios(Vars.Estado);
    }
    get_datosTablaIni();
  });
  $("#Municipio").on("change", function (ev) {
    get_datosTablaIni();
  });
});
get_datosTablaIni();

function get_datosTablaIni() {
  var $datosTabla = $("#ActadeResultados");
  $.ajax({
    url: "/acta_casillas/tabla",
    type: "POST",
    dateType: "json",
    data: {
      TipoEleccion: $("#TipoEleccion").val(),
      Municipio: $("#Municipio").val(),
      Partido: $("#Partido").val()
    },
    success: function success(data) {
      $datosTabla.html(data);
    },
    error: function error() {}
  });
}

function PresidenteRepublicaFiltro() {
  document.getElementById("comboMunicipio").style.display = "none";
  $("#Municipio").val($("#Municipio > option:first").val());
  document.getElementById("Municipio").disabled = true;
}

function rollbackfilter() {
  document.getElementById("comboMunicipio").style.display = "";
  document.getElementById("Municipio").disabled = false;
}

function getDFLC(DIPUTADO, ESTADO) {
  $.ajax({
    url: "/data.distritos",
    type: "POST",
    dateType: "json",
    data: {
      DIPUTADO: DIPUTADO,
      ESTADO: ESTADO
    }
  }).done(function (response) {
    $("#Municipio").html(response.html);
  }).fail(function (jqXHR, textStatus, error) {
    Glow("" + error, "danger");
  });
}

function getMunicipios(ESTADO) {
  $.ajax({
    url: "/acta_casillas/Municipios",
    type: "POST",
    dateType: "json",
    data: {
      ESTADO: ESTADO
    }
  }).done(function (response) {
    $("#Municipio").html(response.html);
  }).fail(function (jqXHR, textStatus, error) {
    Glow("" + error, "danger");
  });
}

function Glow(Message, TipeMessage) {
  setTimeout(function () {
    $.bootstrapGrowl("" + Message, {
      ele: "body", // which element to append to
      type: "" + TipeMessage, // (null, 'info', 'danger', 'success')
      offset: {
        from: "top",
        amount: 130
      }, // 'top', or 'bottom'
      align: "right", // ('left', 'right', or 'center')
      width: 300, // (integer, or 'auto')
      delay: 2000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
      allow_dismiss: false, // If true then will display a cross to close the popup.
      stackup_spacing: 10 // spacing between consecutively stacked growls.
    }, 100);
  });
}

/// tabla functions
$(document).on('click', '.pagination a', function (e) {
  e.preventDefault();
  var TipoEleccion = $("#TipoEleccion").val();
  var Municipio = $("#Municipio").val();
  var Partido = $("#Partido").val();
  var page = $(this).attr('href').split('?page=')[1];
  var $datosTabla = $('#ActadeResultados');
  $.ajax({
    type: 'POST',
    dateType: "json",
    url: "acta_casillas/tabla?page=" + page,
    data: {
      TipoEleccion: $("#TipoEleccion").val(),
      Municipio: $("#Municipio").val(),
      Partido: $("#Partido").val()
    },
    timeout: 60000,
    success: function success(data) {
      $datosTabla.html(data);
    },
    error: function error(XMLHttpRequest, textStatus, errorThrown) {
      bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
    }
  });
});