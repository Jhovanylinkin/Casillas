$('#containerChart').highcharts({
chart: {
    type: 'spline'
},
title: {
    text: 'Instalación de Casillas Jornada Electoral 2014-2015'
},
subtitle: {
    text: '19 de Julio del 2015'
},
xAxis: {
    title: {
        text: 'Horario'
    },
    labels: {
        overflow: 'justify'
    },
    categories: [
        '8-9',
        '9-10',
        '10-11',
        '11-12',
        '12-13',
        '13-14',
        '14-15',
        '15-16',
        '16-17',
        '17-18',
        '18-19',
        '19-20',
    ],
    crosshair: true,
},
yAxis: {
    title: {
        text: 'Total de Casillas'
    },
    min: 0,
    minorGridLineWidth: 0,
    gridLineWidth: 0,
    alternateGridColor: null,

},
plotOptions: {
spline: {
lineWidth: 4,
states: {
    hover: {
        lineWidth: 5
    }
},
marker: {
    enabled: false
},
// pointInterval: 3600000, // one hour
// pointStart: Date.UTC(2015, 6, 19, 8, 0, 0)
}
},
tooltip: {
    shared: true,
    crosshairs: true
},
series: [{
    name: 'Incidencias',
    data: [
        61,
        16,
        3,
        5,
        5,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
    ]
}, {
    name: 'Instaladas',
    data: [
        5114,
        267,
        30,
        2,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
    ]
}],
navigation: {
    menuItemStyle: {
        fontSize: '10px'
    }
}
});
