<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Municipio extends Model
{
    protected $table = 'cat_municipio';
    public $timestamp = false;

    public function cat_municipios($idEntidad){
    	return Municipio::where('idEntidad', $idEntidad)->orderBy('Municipio', 'ASC')->get();
    }

    public function cat_seccion($idMunicipio){
        return DB::table('cat_municipio AS A')
                ->select('Seccion')
                 ->join('entidad_dto_mpio_seccion AS B', function($join){
                    $join->on('A.Clave', "=", 'B.Municipio')
                         ->on('A.Entidad', "=", 'B.Entidad');
                 })
                 ->where('A.id', "=", $idMunicipio)
                 ->orderBy('B.Seccion', 'ASC')
                 ->get();
    }

    public function cat_seccion_by_cve($CveMpio, $idEntidad){
        return DB::table('cat_municipio AS A')
                ->select('Seccion')
                 ->join('entidad_dto_mpio_seccion AS B', function($join){
                    $join->on('A.Clave', "=", 'B.Municipio')
                         ->on('A.Entidad', "=", 'B.Entidad');
                 })
                 ->where('A.Entidad', '=', $idEntidad)
                 ->where('A.Clave', '=', $CveMpio)
                 ->orderBy('B.Seccion', 'ASC')
                 ->get();
    }

    public function dataComboMunicipiosRegion($estadoId, $Region){
        return DB::table('cat_municipio')
            ->select('id', 'Clave', 'Municipio', 'Region')
            ->where('Entidad', $estadoId)
            ->where('Region', $Region)
            ->get();
    }

    public function users(){
        return $this->belongsTo('App\User','idMunicipio');
    }

    public function entidad(){
        return $this->belongsTo('App\Entidad', 'idEntidad');
    }

    public function dataComboMpioByUserNivel($user, $Entidad){
        $TipoNivel = $user->idTipoNivel;
        $uidNivel = $user->uidNivel;

        if($TipoNivel == 1){
            return DB::table('cat_municipio')->selectRaw('id, Clave, Municipio')->where('Entidad', $Entidad)->where('id', $user->idMunicipio)->first();
        }
        if($TipoNivel == 4 || $TipoNivel == 5 || $TipoNivel == 6  || $TipoNivel == 7 ){
            return DB::table('cat_municipio')->selectRaw('id, Clave, Municipio')->where('Entidad', $Entidad)->where('Clave', $user->RCveMpio)->first();
        }

        $queryFull = DB::table('cat_municipio AS M')->selectRaw('M.id, M.Clave, M.Region, M.Municipio')->where('M.Entidad', $Entidad);

        switch ($TipoNivel) {
            case 3:
                $queryFull->where('M.Region', $uidNivel);
                break;
            case 8:
                $queryFull->whereRaw("M.Clave IN (SELECT MUNICIPIO FROM numeralia WHERE ID_ESTADO = $Entidad AND DISTRITO=$uidNivel)");
                break;
            case 9:
                $queryFull->whereRaw("M.Clave IN (SELECT MUNICIPIO FROM numeralia WHERE ID_ESTADO = $Entidad AND DL=$uidNivel)");
                break;
            default:
                break;
        }

        return $queryFull->orderBy('M.Municipio')->get();
    }
}
