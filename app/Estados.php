<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
    protected $table ='cat_entidad';
    public $timestamps = false;
}
