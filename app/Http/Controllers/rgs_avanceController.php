<?php

namespace App\Http\Controllers;
use DB;
use App\rgs;
use App\rcs;
use App\Casillas;
use App\Municipio;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;

class rgs_avanceController extends Controller
{
    public function avancergs(){
        $USERLOGGED = Auth::user();
        if($USERLOGGED->idTipoNivel==1){
            abort(403,'Forbidden');
        }
        $mun = new Municipio;
        $obj = new rcs;
        $Entidad = 11;
        $Region = $obj->get_regiones_by_nivel($USERLOGGED, $Entidad);
        $Municipios = $mun->dataComboMpioByUserNivel($USERLOGGED, $Entidad);
        // dd($Municipios);
        return view('rgs.rgs_avance',[
            'USRlgd' => $USERLOGGED,
            'Regiones' => $Region,
            'cat_municipios' => $Municipios
        ]);

    }


    public function chart(){
        $Entidad = 11;
        $USERLOGGED = Auth::user();
        $RGS = new rgs;
        $Reclutados = $RGS->chartRecruited(1,$USERLOGGED,$Entidad);
        $Capacitados = $RGS->chartRecruited(2,$USERLOGGED,$Entidad);
        $Encuadrados = $RGS->chartRecruited(3,$USERLOGGED,$Entidad);
        $TototalRGS = $RGS->totalRGS($USERLOGGED,$Entidad);
        $response = [
            'Reclutados' => $Reclutados,
            'Capacitados' => $Capacitados,
            'Encuadrados' => $Encuadrados,
            'TotalRGS' => $TototalRGS
        ];
        return $response;
    }

    public function Tablainit(Request $request){
            $USERLOGGED = Auth::user();
            $Entidad=11;
            $RGS= new rgs;
            $collect= $RGS->dataTable($USERLOGGED,$Entidad,$request);
            $collect=collect($collect);
            if ($request->has('radio')) {
                if($request->radio==3){
                    $collect= $collect->where('INE',null)->where('Nombre',null);
                }
                if($request->radio==4){}
            }
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();// Create a new Laravel collection from the array data
            $itemCollection = $collect;// Define how many items we want to be visible in each page
            $perPage = 10;// Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all(); // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);// set url path for generted links
            $paginatedItems->setPath($request->url());
            return view ('rgs.tabla.rgs_tablaavance',[
                'Tabla' => $paginatedItems
            ]);

    }
    public function AddCasilla(Request $request){
        $USERLOGGED = Auth::user();
        $mun = new Municipio;
        $obj = new rcs;
        $Entidad = 11;
        $Region = $obj->get_regiones_by_nivel($USERLOGGED, $Entidad);
        $Municipios = $mun->dataComboMpioByUserNivel($USERLOGGED, $Entidad);
        return view('rgs.modals.rgs_addCasilla',[
            'idRGS' => $request->ID,
            'Regiones' => $Region,
            'cat_municipios' => $Municipios,
            'Secciones' => [],
            'USERLOGGED' => $USERLOGGED
        ]);
    }
}
