<?php

namespace App\Http\Controllers;

use DB;
use App\rcs;
use App\Casillas;
use App\Municipio;
use App\rcs_aspirantes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;

class rcs_avanceController extends Controller
{
    public function Avance(Request $request){
        $USERLOGGED = Auth::user();
        if($USERLOGGED->idTipoNivel==1){
            abort(403,'Forbidden');
        }
        $mun = new Municipio;
        $obj = new rcs;
        $Entidad = 11;

        $Municipios = $mun->dataComboMpioByUserNivel($USERLOGGED, $Entidad);
        $cat_casillas = $obj->get_casillas_by_nivel($USERLOGGED, $Entidad);
        $Region = $obj->get_regiones_by_nivel($USERLOGGED, $Entidad);
        $asp =  new rcs_aspirantes;
        $aspirantes = $asp->dataTable($USERLOGGED,$Entidad,$request,1);
        return view ('rcs.rcs_avance',[
            'cat_municipios' => $Municipios,
            'Regiones' => $Region,
            'Cat_Casillas' => $cat_casillas,
            'USRlgd' => $USERLOGGED,
            'ASPIRANTES' => $aspirantes
        ]);
    }
    public function chart(){
        $USERLOGGED = Auth::user();
        $Entidad=11;
        $objChart = new rcs;
        $Casillas = Casillas::where('estado',11)->count();
        $MetaTotalRCs=$objChart->totalRCS($USERLOGGED,$Entidad);
        $Propietario1 = $objChart->chartToUserLevel(1,$USERLOGGED,$Entidad);
        $Propietario2 = $objChart->chartToUserLevel(2,$USERLOGGED,$Entidad);
        $Propietario3 = $objChart->chartToUserLevel(3,$USERLOGGED,$Entidad);
        $Propietario4 = $objChart->chartToUserLevel(4,$USERLOGGED,$Entidad);
        $PAN = $objChart->chartToUserLevel(5,$USERLOGGED,$Entidad);
        $PRD = $objChart->chartToUserLevel(6,$USERLOGGED,$Entidad);
        $MVC = $objChart->chartToUserLevel(7,$USERLOGGED,$Entidad);
        $response = [
            'Casillas' => $Casillas,
            'TotalRCS' => $MetaTotalRCs=$MetaTotalRCs/4,
            'Propietario1' => $Propietario1,
            'Propietario2' => $Propietario2,
            'Suplente1' => $Propietario3,
            'Suplente2' => $Propietario4,
            'PAN' => $PAN,
            'PRD' => $PRD,
            'MVC' => $MVC,
        ];
        return $response;
    }

    public function municipiosRegion(Request $request){
        $Region=$request->regionID;
        $Municipios=Municipio::select('Clave','Municipio','Region')->where('idEntidad',11)->where('Region',$Region)->get();
        $response = [
            'status'=>'success',
            'html'=>view('rcs.select.municipios',compact('Municipios'))->render()
        ];
        return response()->json($response);
    }

    public function casillasMunicipio(Request $request){
        $MUNICIPIO=$request->casillaID;
        $Cat_Casillas=Casillas::distinct()->select('CASILLA')->where('ESTADO',11)->where('SECCION',$MUNICIPIO)->get();
        $response = [
            'status'=>'success',
            'html'=>view('rcs.select.casillas',compact('Cat_Casillas'))->render()
        ];
        return response()->json($response);
    }
    public function seccionMunicipio(Request $request){
        $MUNICIPIO=$request->municipioID;
        $Cat_Secciones=Casillas::distinct()->select('SECCION')->where('ESTADO',11)->where('MUNICIPIO',$MUNICIPIO)->get();
        $response = [
            'status'=>'success',
            'html'=>view('rcs.select.seccion',compact('Cat_Secciones'))->render()
        ];
        return response()->json($response);
    }
    public function dfdl(Request $request){
        $DFLD= DB::select("SELECT DISTRITO, DL FROM numeralia where ID_ESTADO=11 and SECCION =$request->SECCION ;");
        $DFLD = collect($DFLD);
        $response = [
            'df' => $DFLD->first(),
        ];
        return $response;
    }
    public function colonias (Request $request) {
        $Colonias = DB::select("SELECT distinct Colonia as nombreCol FROM gto_colonias where CVE_MPIO=$request->municipioID order by Colonia;");
        $Colonias = collect($Colonias);
        $response = [
            'status' => 'success',
            'html' => view('rcs.select.colonias',compact('Colonias'))->render()
        ];
        return response()->json($response);
    }
    public function eliminated(){
        $USERLOGGED = Auth::user();
        if($USERLOGGED->idTipoNivel==2 || $USERLOGGED->idTipoNivel==3){
        return view ('rcs.rcs_eliminated');
        }else{
            abort(403,'Forbidden');
        }
    }
    public function tablarcsdeleted (Request $request){
        $obj = new rcs;
        $USERLOGGED = Auth::user();
        $result=$obj->tablaDeleted($USERLOGGED);
        $collect=collect($result);
        // Get current page form url e.x. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();// Create a new Laravel collection from the array data
        $itemCollection = $collect;// Define how many items we want to be visible in each page
        $perPage = 10;// Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all(); // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);// set url path for generted links
        $paginatedItems->setPath($request->url());
        return view ('rcs.tabla.rcs_tabladeleted',[
            'Tabla' => $paginatedItems
        ]);
    }
    public function Tablainit(Request $request){
        $USERLOGGED = Auth::user();
        $Entidad = 11;
        $obj_tabla = new rcs;
        $result = $obj_tabla->dataForTable($USERLOGGED, $Entidad, $request);
        $collect=collect($result);
        if ($request->has('radio')) {
            if($request->radio==1) {
                $collect= $collect->where('INEP1',null)->where('NombreP1',null)->where('INEP2',null)->where('NombreP2',null);
            }
            if($request->radio==2){
                $collect = $collect->where('INES1',null)->where('NombreS1',null)->where('INES2',null)->where('NombreS2',null);
            }
            if($request->radio==3){
                $collect = $collect->where('INEP1',null)->where('NombreP1',null)->where('INEP2',null)->where('NombreP2',null);
                $collect = $collect->where('INES1',null)->where('NombreS1',null)->where('INES2',null)->where('NombreS2',null);
            }
            if($request->radio==4){
            }
        }
        // Get current page form url e.x. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();// Create a new Laravel collection from the array data
        $itemCollection = $collect;// Define how many items we want to be visible in each page
        $perPage = 10;// Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all(); // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);// set url path for generted links
        $paginatedItems->setPath($request->url());
        return view ('rcs.tabla.rcs_tablaavance',[
            'Tabla' => $paginatedItems
        ]);
    }
}
