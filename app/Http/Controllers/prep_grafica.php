<?php

namespace App\Http\Controllers;
use DB;
use App\partidos;
use App\Municipio;
use App\distrito_local;
use App\distrito_federal;
use App\casillas_resultados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class prep_grafica extends Controller
{
    public $Entidad=27;

    public function init(Request $request){
        $UserLogged = Auth::user();
        $Municipios = new  Municipio;
        $TipoE = DB::table('cat_tipo_eleccion')->get();
        $Muni = $Municipios->dataComboMpioByUserNivel($UserLogged,$this->Entidad);
        return view('prep_grafica.prep_grafica',[
            'TipoE' => $TipoE,
            'Muni' => $Muni
        ]);
    }
    public function graph(Request $request){
        $CResultados = new casillas_resultados;
        $toChartPartidos = $CResultados->partidosTabla($request,null,$this->Entidad);
        $totales = $CResultados->partidosTabla($request,1,$this->Entidad);
        $category = array();
        $series1 = array();
        $series1['type'] = 'column';
        $resultadosArray = array();
        foreach ($toChartPartidos as $toChartPartidoskey) {
                $CampoSave = $toChartPartidoskey->CampoSave;
                foreach($totales as $totalesKey) {
                    $resultadosArray['Result'][] = array('R'=>$totalesKey->$CampoSave);
            }
        }
        $increment = 0;
        foreach($toChartPartidos as $value){
            $series1['data'][] = array('name'=>$value->Siglas,'y'=>$resultadosArray['Result'][$increment]['R'],'color' =>$value->Color,'imagen' =>$value->Logo);
            $category['data'][] = array('siglas'=>$value->Siglas,'imagen' =>$value->Logo);
            $increment++;
        }
        $result = array();
        array_push($result,$category);
        array_push($result,$series1);
        return  json_encode($result, JSON_NUMERIC_CHECK);
    }

    public function tabla(Request $request){
        $toTable = new casillas_resultados;
        $responseFront = $toTable->Tabla($request,$this->Entidad);
        $Partidos = $toTable->partidosTabla($request,null,$this->Entidad);
        $responseFront->withPath('prep_grafica');
        return view('prep_grafica.tabla.prep_tabla',[
            'dataToTable' => $responseFront,
            'Partidos' => $Partidos
        ]);
    }

    public function diputados(Request $request){
        if($request->DIPUTADO=='DIPUTADO FEDERAL'){
            $DF = distrito_federal::select('Distrito','Cabecera')->where('Entidad',$request->ESTADO)->get();
        }
        if ($request->DIPUTADO=='DIPUTADO LOCAL') {
            $DF = distrito_local::select('Distrito','Cabecera')->where('Entidad',$request->ESTADO)->get();
        }
        $response = [
            'status'=>'success',
            'html'=>view('prep_grafica.select.distritos',compact('DF'))->render()
        ];
        return response()->json($response);
      }

      public function estadoMunicipio(Request $request){
        $Municipios = Municipio::select('Clave','Municipio')->where('idEntidad',$request->ESTADO)->get();
        $response = [
            'status'=>'success',
            'html'=>view('prep_grafica.select.municipios',compact('Municipios'))->render()
        ];
        return response()->json($response);
    }
}
