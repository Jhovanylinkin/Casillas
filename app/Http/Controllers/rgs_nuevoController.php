<?php

namespace App\Http\Controllers;
use DB;
use App\rgs;
use App\rcs;
use App\partidos;
use App\Municipio;
use App\Casillas;
use App\Abogados;
use App\CasaAzul;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class rgs_nuevoController extends Controller
{
    public function form(){
        $USERLOGGED = Auth::user();
        if($USERLOGGED->idTipoNivel==1){
            abort(403,'Forbidden');
        }
        $mun = new Municipio;
        $obj = new rcs;
        $Entidad = 11;
        $Municipios = $mun->dataComboMpioByUserNivel($USERLOGGED, $Entidad);
        $Partidos = partidos::select('id','Partido')->get();
        $casaAzul= CasaAzul::all();
        $Abogados = Abogados::all();
        return view('rgs.rgs_nuevoregistro',[
            'municipios' => $Municipios,
            'Secciones' => [],
            'Casilla' => [],
            'USRLGD' => $USERLOGGED,
            'Partidos' => $Partidos,
            'CasaAzul' => $casaAzul,
            'Abogados' => $Abogados
        ]);
    }


    public function formtoreplace(Request $request){
        $USERLOGGED =  Auth::user();
        if ($request->has('INE')){
        $toEdit = DB::select("Select S.CVE_MPIO AS ID_MUNICIPIO, S.MUNICIPIO AS NombreMunicipio, P.Partido, R.idPartido, S.SECCION, R.INE, R.Nombre, R.Paterno, R.Materno, R.CURP, R.LADA, R.TelCasa, R.Celular, R.Comentarios, R.Colonia, R.Cpostal, R.Calle, R.NoExterior, R.NoInterior, R.Sexo, R.FechaNacimiento, R.OCR, R.DFederal, R.DLocal, R.email, R.facebook, R.twitter, R.Promotor, R.Capacitacion, Ab.Nombre as NombreAbogado, R.idAbogado, CAz.NombreResponsable as NombreCasaAzul, R.idCasaAzul from ( Select SECCION, MUNICIPIO, CVE_MPIO from gto_secciones ) S left join ( select ID_ESTADO, idPartido, ID_MUNICIPIO, Nombre, Paterno, Materno, INE, CURP, LADA, TelCasa, Celular, Comentarios, Colonia, Cpostal, Calle, NoExterior, NoInterior, Sexo, FechaNacimiento, OCR, SECCION, DFederal, DLocal, email, facebook, twitter, Promotor, Capacitacion, idAbogado, idCasaAzul from rgs ) R on ( R.ID_MUNICIPIO = S.CVE_MPIO and R.SECCION = S.SECCION ) left join ( select id, Partido from partidos ) P on (P.id = R.idPartido) left join ( select id, Nombre from abogados ) Ab on ( Ab.id = R.idAbogado ) left join ( select id, NombreResponsable from casa_amiga )CAz  on ( CAz.id = R.idCasaAzul ) where R.INE like '%$request->INE%'");
        $toEdit= collect($toEdit);
            if($USERLOGGED->idTipoNivel==1){
            abort(403,'Forbidden');
        }
            $Municipios= Municipio::Select('Municipio','Clave')->where('idEntidad',11)->get();
            $Secciones = DB::select("SELECT DISTINCT SECCION FROM gto_secciones");
            $Casilla = DB::raw('select distinct casilla from casillas where ESTADO = 11;');
            $Casilla = DB::select($Casilla);
            $Partidos = partidos::select('id','Partido')->get();
            $casaAzul= CasaAzul::all();
            $Abogados = Abogados::all();
            return view('rgs.rgs_replace',[
                'municipios' => $Municipios,
                'Secciones' => $Secciones,
                'Casilla' => $Casilla,
                'USRLGD' => $USERLOGGED,
                "TOEDIT" => $toEdit,
                'Partidos' => $Partidos,
                'CasaAzul' => $casaAzul,
                'Abogados' => $Abogados
            ]);
        }
    }
    public function store(Request $request){
	    if(rgs::where('INE', $request->INE)->exists()){
            return response()->json([
                'success' => false,
                'message' => 'El INE ya se encuentra registrado'
            ], 401);
        }

        if(rcs::where('INE', $request->INE)->exists()){
            return response()->json([
                'success' => false,
                'message' => 'El INE ya se encuentra registrado en '
            ], 401);
        }
        
        $RGS = new rgs;
        $RGS->idCandidato=1;
        $RGS->idPartido=$request->idPartido;
        $RGS->ID_ESTADO=11;
        $RGS->ID_MUNICIPIO=$request->ID_MUNICIPIO;
        $RGS->INE=strtoupper($request->INE);
        $RGS->CURP=strtoupper($request->CURP);
        $RGS->Nombre=strtoupper($request->Nombre);
        $RGS->Paterno=strtoupper($request->Paterno);
        $RGS->Materno=strtoupper($request->Materno);
        $RGS->LADA=$request->LADA;
        $RGS->TelCasa=$request->TelCasa;
        $RGS->Celular=$request->Celular;
        $RGS->Comentarios=strtoupper($request->Comentarios);
        $RGS->Colonia=strtoupper($request->Colonia);
        $RGS->Cpostal=$request->Cpostal;
        $RGS->Calle=strtoupper($request->Calle);
        $RGS->NoExterior=$request->NoExterior;
        $RGS->NoInterior=$request->NoInterior;
        $RGS->Sexo=$request->Sexo;
        $RGS->FechaNacimiento=$request->FechaNacimiento;
        $RGS->OCR=strtoupper($request->OCR);
        $RGS->SECCION=$request->SECCION;
        $RGS->DFederal=$request->DFederal;
        $RGS->DLocal=$request->DLocal;
        $RGS->email=$request->email;
        $RGS->facebook=$request->facebook;
        $RGS->twitter=$request->twitter;
        $RGS->idCasaAzul=$request->idCasaAzul;
        $RGS->idAbogado=$request->idAbogado;
        $RGS->Promotor = strtoupper($request->Promotor);
        $RGS->Capacitacion = $request->Capacitacion;
        $RGS->FechaCreate=new \DateTime();
        $RGS->UserCreate=$request->user()->id;

        if($RGS->save()){
            return response()->json([
                'success' => 'Se guardo correctamente'
            ]);
        }
    }
    public function savereplace(Request $request){
        $toUPDATE=[
            'ID_MUNICIPIO' => $request->ID_MUNICIPIO,
            'idPartido' => $request->idPartido,
            'SECCION' => $request->SECCION,
            'Cpostal' => $request->Cpostal,
            'Calle' => strtoupper($request->Calle),
            'NoExterior' => $request->NoExterior,
            'NoInterior' => $request->NoInterior,
            'FechaNacimiento'=> $request->FechaNacimiento,
            'OCR' => strtoupper($request->OCR),
            'DFederal' => $request->DFederal,
            'DLocal' => $request->DLocal,
            'LADA' => $request->LADA,
            'email' => $request->email,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'INE' => strtoupper($request->INE),
            'CURP' => strtoupper($request->CURP),
            'Nombre' => strtoupper($request->Nombre),
            'Paterno' => strtoupper($request->Paterno),
            'Materno' => strtoupper($request->Materno),
            'Sexo' => $request->Sexo,
            'TelCasa' => $request->TelCasa,
            'Celular' => $request->Celular,
            'Colonia' => strtoupper($request->Colonia),
            'Comentarios' => strtoupper($request->Comentarios),
            'Promotor' => strtoupper($request->Promotor),
            'Capacitacion' => $request->Capacitacion,
            'idCasaAzul' => $request->idCasaAzul,
            'idAbogado' => $request->idAbogado


        ];
        if(DB::table('rgs')->where('INE',$request->INEANTERIOR)->update($toUPDATE)){
            return response()->json([
                'success' => 'Se guardo correctamente'
            ]);
        }else{
            return response()->json([
                'success' => 'No hay nada que actualizar'
            ]);
        }
    }
    public function setcasillas(Request $request){
        $flag = DB::select("SELECT * FROM casillas where ESTADO=11 and MUNICIPIO=$request->Municipio and SECCION=$request->SECCION and CASILLA='$request->Casilla' and idRg is not null;");
        $flag = collect($flag); $flag=$flag->isEmpty();
        if($flag==false){
            return [
                'success' => false,
                'message' => 'La casilla ya se encuentra asignada a un RGS'
            ];
        }else{
            $Total=rgs::where('id',$request->idRGS)->sum('TotalCasillas');
            $rg = rgs::find($request->idRGS);
            $RGStatus = true;
            $update=['idRg' => $request->idRGS];
            if($RGStatus && DB::table('casillas')->where('ESTADO',11)->where('MUNICIPIO',$request->Municipio)->where('SECCION',$request->SECCION)->where('CASILLA',$request->Casilla)->update($update)){
                if(DB::table('rgs')->where('id', $request->idRGS)->update(['TotalCasillas' => $Total+1])){
                    return [
                    'success' => true,
                    'message' => 'Casilla asignada'
                    ];
                }
            }
        }
    }
}
