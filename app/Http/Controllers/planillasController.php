<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;
use Excel;
use App\planillas;
use App\Entidad;
use App\Municipio;
use App\distrito_local;
use App\distrito_federal;
use Illuminate\Pagination\LengthAwarePaginator;

class planillasController extends Controller
{
    public function init(){
        $Elecciones = planillas::select('TIPOELECCION')->distinct()->get();
        $Estados = Entidad::select('id','Entidad')->get();
        $Partidos = planillas::select('PARTIDO')->distinct()->get();
        return view('planillas.planillas',[
            'TipoE' => $Elecciones,
            'Estados' => $Estados,
            'Partidos' => $Partidos
        ]);
    }


    public function estadoMunicipio(Request $request){
        $Municipios = Municipio::select('Clave','Municipio')->where('idEntidad',$request->ESTADO)->get();
        $response = [
            'status'=>'success',
            'html'=>view('planillas.select.municipios',compact('Municipios'))->render()
        ];
        return response()->json($response);
    }

    public function partido(Request $request){
        $Partidos = planillas::select('PARTIDO')->where('TIPOELECCION',$request->TIPOELECCION)->distinct()->get();
        $response = [
            'status'=>'success',
            'html'=>view('planillas.select.partidos',compact('Partidos'))->render()
        ];
        return response()->json($response);
    }

    public function diputados(Request $request){
        if($request->DIPUTADO=='DIPUTADO FEDERAL'){
            $DF = distrito_federal::select('Distrito','Cabecera')->where('Entidad',$request->ESTADO)->get();
        }
        if ($request->DIPUTADO=='DIPUTADO LOCAL') {
            $DF = distrito_local::select('Distrito','Cabecera')->where('Entidad',$request->ESTADO)->get();
        }
        $response = [
            'status'=>'success',
            'html'=>view('planillas.select.distritos',compact('DF'))->render()
        ];
        return response()->json($response);
    }

    public function popup(Request $request){
        // dd($request->all());
        $PlanillasModel = new planillas;
        $planilla = $PlanillasModel->popup($request);
        $collect = collect($planilla);
        // dd($collect->keyBy('CARGO'));
        if ($request->TIPOELECCION=='PRESIDENTE MUNICIPAL') {
            return view('planillas.modals.planillasmodal',[
                'Planilla' => $collect,
                'unavez' => true
            ]);
        }
        if ($request->TIPOELECCION=='SENADOR') {
            return view('planillas.modals.planillasmodalsenador',[
                'Planilla' => $collect
            ]);
        }
        if ($request->TIPOELECCION=='DIPUTADO FEDERAL') {
            return view('planillas.modals.planillasmodaldiputados',[
                'Planilla' => $collect
            ]);
        }
         if ($request->TIPOELECCION=='DIPUTADO LOCAL') {
            return view('planillas.modals.planillasmodaldiputados',[
                'Planilla' => $collect
            ]);
        }
         
    }


    public function Tablainit(Request $request){
    $PlanillasModel = new planillas;
    $collect = $PlanillasModel->datatoTable($request);
    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $itemCollection = $collect;
    $perPage = 10;
    $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all(); 
    $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
    $paginatedItems->setPath($request->url());
    if($request->TIPOELECCION=='PRESIDENTE MUNICIPAL'){
        return view ('planillas.tabla.planillastabla',[
        'planillas' => $paginatedItems
        ]);
    }
    if($request->TIPOELECCION=='GOBERNADOR'){
        return view ('planillas.tabla.planillasgobernador',[
        'planillas' => $paginatedItems
        ]);
    }
    if($request->TIPOELECCION=='SENADOR'){
        return view ('planillas.tabla.planillassenador',[
        'planillas' => $paginatedItems
        ]);
    }
    if($request->TIPOELECCION=='DIPUTADO FEDERAL' || $request->TIPOELECCION=='DIPUTADO LOCAL'){
        return view ('planillas.tabla.planilladiputados',[
        'planillas' => $paginatedItems
        ]);
    }
    // defult
    return view ('planillas.tabla.planillaspresidentesrepublica',[
        'planillas' => $paginatedItems
        ]);
    }



    public function CargarPlanilla(Request $request){
        $archivo = $request->file('excelPLANILLAS');
        $nombre_original=$archivo->getClientOriginalName();
	   	$extension=$archivo->getClientOriginalExtension();
        $r1=Storage::disk('Archivos')->put($nombre_original,  \File::get($archivo) );
        $ruta  =  storage_path('Archivos') ."/". $nombre_original;
        if($r1){
            planillas::truncate();
            Excel::selectSheetsByIndex(0)->load($ruta, function($hoja) {
		        $hoja->each(function($fila) {
                        $Planillas=new planillas;
                        $Planillas->TIPOELECCION = $fila->tipoeleccion;
                        $Planillas->CARGO = $fila->cargo;
                        $Planillas->ESTADO = $fila->estado;
                        $Planillas->MUNICIPIO_DISTRITO = $fila->municipio_distrito;
                        $Planillas->NOMBRE = $fila->nombre;
                        $Planillas->PARTIDO = $fila->partido;
		                $Planillas->save();
		        });

            });
            return response()->json([
                'success' => 'Se guardo correctamente'
            ]);
    	
       }
    }
}
