<?php

namespace App\Http\Controllers;
use DB;
use App\rcs;
use App\Casillas;
use App\Partidos;
use App\Municipio;
use App\rcs_aspirantes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class rcs_aspirantesController extends Controller
{
    public function init(Request $request){
        if ($request->has('INE')){
            $query=DB::raw("SELECT distinct P.id, P.idPartido, Po.Partido, P.ID_ESTADO, P.ID_MUNICIPIO, C.NombreMunicipio, P.SECCION, P.CASILLA, P.PROPIETARIO, P.INE, P.CURP, P.Nombre, P.Paterno, P.Materno, P.Sexo, P.LADA, P.TelCasa, P.Celular, P.email, P.Cpostal, P.Calle, P.NoExterior, P.NoInterior, P.FechaNacimiento, P.OCR, P.DFederal, P.DLocal, P.facebook, P.twitter, P.Comentarios, P.Colonia, P.Promotor, P.Capacitacion FROM ( SELECT C.ESTADO, M.Region, C.MUNICIPIO, M.Municipio AS NombreMunicipio, GS.POLIGONO, C.SECCION, C.CASILLA FROM cat_municipio M JOIN casillas C ON M.Clave = C.MUNICIPIO AND M.idEntidad = C.ESTADO JOIN gto_secciones GS ON GS.SECCION = C.SECCION AND GS.CVE_MPIO = C.MUNICIPIO WHERE C.ESTADO = 11 ) C left join ( SELECT id, idPartido, ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO, INE, Nombre, Paterno, Materno, CURP, Sexo, LADA, TelCasa, Celular, email, Cpostal, Calle, NoExterior, NoInterior, FechaNacimiento, OCR, DFederal, DLocal, facebook, twitter, Comentarios, Colonia, Promotor, Capacitacion FROM rcs_aspirantes ) P on ( P.ID_ESTADO = C.ESTADO and P.ID_MUNICIPIO = C.MUNICIPIO and P.SECCION = C.SECCION ) left join ( SELECT id, Partido FROM partidos ) Po on (P.idPartido = Po.id) where P.INE like '%$request->INE%'");
            $toEdit = DB::select($query);
            $toEdit= collect($toEdit);
        }
        $mun = new Municipio;
        $obj = new rcs;
        $Entidad = 11;

        $USERLOGGED =  Auth::user();
        $Partidos = Partidos::select('id','Partido')->whereIn('id', [1, 11, 13, 16])->get();
        $Municipios = $mun->dataComboMpioByUserNivel($USERLOGGED, $Entidad);
        $Casilla = $obj->get_casillas_by_nivel($USERLOGGED, $Entidad);
        $Region = $obj->get_regiones_by_nivel($USERLOGGED, $Entidad);
        $asp =  new rcs_aspirantes;
        $aspirantes = $asp->dataTable($USERLOGGED,$Entidad,$request,1);
        if ($request->has('INE')){
           return view('rcs.rcs_replace',[
                'municipios' => $Municipios,
                'Secciones' => [],
                'Casilla' => $Casilla,
                'USRLGD' => $USERLOGGED,
                'Partidos' => $Partidos,
                "TOEDIT" => $toEdit
            ]);
        }else{
        return view('rcs.rcs_aspirantes',[
            'cat_municipios' => $Municipios,
            'Regiones' => $Region,
            'Cat_Casillas' => $Casilla,
            'ASPIRANTES' => $aspirantes,
            'USRLGD' => $USERLOGGED,
        ]);
        }
    }
    public function save(Request $request){
        $flag = rcs::where('INE', $request->INE)->exists();
	    if($flag){
    		return response()->json([
    			'success' => false,
    			'message' => 'El INE ya se encuentra registrado en RCS'
    		], 401);
        }
        if (rcs_aspirantes::where('INE', $request->INE)->exists()) {
            return response()->json([
			    'success' => false,
			    'message' => 'El INE ya se encuentra registrado como aspirante'
		    ], 401);
        }
        $RCS = new rcs_aspirantes;
        $RCS->idPartido=$request->idPartido;
        $RCS->idCandidato=1;
        $RCS->ID_ESTADO=11;
        $RCS->ID_MUNICIPIO=$request->ID_MUNICIPIO;
        $RCS->Cpostal=$request->Cpostal;
        $RCS->Calle=strtoupper($request->Calle);
        $RCS->NoExterior=$request->NoExterior;
        $RCS->NoInterior=$request->NoInterior;
        $RCS->SECCION=$request->SECCION;
        $RCS->DFederal=$request->DFederal;
        $RCS->DLocal=$request->DLocal;
        $RCS->CASILLA=$request->CASILLA;
        $RCS->PROPIETARIO=$request->PROPIETARIO;
        $RCS->INE=strtoupper($request->INE);
        $RCS->OCR= strtoupper($request->OCR);
        $RCS->CURP=strtoupper($request->CURP);
        $RCS->Nombre=strtoupper($request->Nombre);
        $RCS->Paterno=strtoupper($request->Paterno);
        $RCS->Materno=strtoupper($request->Materno);
        $RCS->Sexo=$request->Sexo;
        $RCS->FechaNacimiento=$request->FechaNacimiento;
        $RCS->LADA=$request->LADA;
        $RCS->TelCasa=$request->TelCasa;
        $RCS->Celular=$request->Celular;
        $RCS->Colonia=$request->Colonia;
        $RCS->email=$request->email;
        $RCS->facebook=$request->facebook;
        $RCS->twitter=$request->twitter;
        $RCS->Comentarios=strtoupper($request->Comentarios);
        $RCS->FechaCreate=new \DateTime();
        $RCS->UserCreate=$request->user()->id;
        $RCS->Promotor=$request->Promotor;
        $RCS->Capacitacion=$request->Capacitacion;
        if($RCS->save()){
            return response()->json([
                'success' => 'Se guardó correctamente como aspirante'
            ]);
        }
    }
    public function tablainit(Request $request){
        $Entidad = 11;
        $USERLOGGED =  Auth::user();
        $aspirantes = new rcs_aspirantes;
        $collect= $aspirantes->dataTable($USERLOGGED,$Entidad,$request,0);
        $collect=collect($collect);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();// Create a new Laravel collection from the array data
        $itemCollection = $collect;// Define how many items we want to be visible in each page
        $perPage = 10;// Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all(); // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);// set url path for generted links
        $paginatedItems->setPath($request->url());
        return view ('rcs.tabla.rcs_tablaaspirantes',[
            'Tabla' => $paginatedItems
        ]);
    }
}
