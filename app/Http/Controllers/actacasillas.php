<?php

namespace App\Http\Controllers;
use DB;
use App\Partidos;
use App\Municipio;
use App\casillas_boletas;
use App\distrito_federal;
use App\distrito_local;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;

class actacasillas extends Controller
{
    /**
     * @var Globals setup
     */
    public $Entidad = 11;
    
    public function init(Request $request){
      $UserLogged = Auth::user();
      $Municipios = new  Municipio;
      $Muni = $Municipios->dataComboMpioByUserNivel($UserLogged,$this->Entidad);
      $Partidos = DB::table('partidos_resultados')->where('Estatus', 1)->select('id','Partido','isCoalicion')->get();
      $TipoE = DB::table('cat_tipo_eleccion')->get();
      return view('actacasillas.actacasillas',[
        'Muni' => $Muni,
        'Partidos' => $Partidos,
        'TipoE' => $TipoE
      ]);
    }
    
    
    public function estadoMunicipio(Request $request){
        $Municipios = Municipio::select('Clave','Municipio')->where('idEntidad',$request->ESTADO)->get();
        $response = [
            'status'=>'success',
            'html'=>view('actacasillas.select.municipios',compact('Municipios'))->render()
        ];
        return response()->json($response);
    }

    
    public function storage(Request $request){
      $tipoEleccion = $request->TipoEleccion;
      $CasillasBoletas = new casillas_boletas;
      # Comprobación de existencia en la base para evitar duplicidad
      if ($tipoEleccion == 6 and $request->Municipio =='0') {
        $flag = casillas_boletas::where('TipoEleccion',$request->TipoEleccion)->where('Partido',$request->Partido)->exists();
      }
      else{
        switch ($tipoEleccion) {
          case 1:
          case 2:
          case 4:
            $flag = casillas_boletas::where('TipoEleccion',$request->TipoEleccion)->where('Partido',$request->Partido)->exists();
            break;
          case 3:
            $flag = $CasillasBoletas->validarDiputados($request,'DistritoF');
            break;
          case 5:
            $flag = $CasillasBoletas->validarDiputados($request,'DistritoL');
            break;
          case 6:
            $flag = casillas_boletas::where('TipoEleccion',$request->TipoEleccion)->where('Municipio', $request->Municipio)->where('Partido',$request->Partido)->exists();
            break;
        }
      }
	    if($flag){
    		return response()->json([
    			'success' => false,
    			'message' => 'El Partido ya se encuentra registrado'
    		],401);
      }
      # Almacenamiento de datos dependiendo de su tipo
      if ($request->TipoEleccion == 6 and $request->Municipio =='0') {
        $T = $CasillasBoletas->storage($request,$this->Entidad);
      }else{
        switch ($tipoEleccion) {
          case 3:
              $T = $CasillasBoletas->storageDiputados($request,'DistritoF',$this->Entidad);
            break;
          case 5:
              $T = $CasillasBoletas->storageDiputados($request,'DistritoL',$this->Entidad);
            break;
          case 6:
              $CasillasBoletas->TipoEleccion = $request->TipoEleccion;
              $CasillasBoletas->Municipio = $request->Municipio;
              $CasillasBoletas->Partido = $request->Partido;
              $CasillasBoletas->Username = $request->user()->id;
              $CasillasBoletas->FechaHora = new \DateTime();
              $CasillasBoletas->Entidad = $this->Entidad;
              if ($CasillasBoletas->save()){$T=true;}
            break;
          default:
              $T = $CasillasBoletas->storage($request,$this->Entidad);
            break;
        }
      }
      if($T) {
        return response()->json([
          'success' =>  true,
          'message' => 'Se guardó correctamente'
        ],200);
      }
    }
    
    public function delete(Request $request){
      $CB = new casillas_boletas;
      if ($request->TipoEleccion == 1 || $request->TipoEleccion == 2 || $request->TipoEleccion == 4) {
        if ($CB->supr($request)) {
          return response()->json([
           'success' => true,
           'message' => 'Se eliminó el elemento'
          ],200);
       }
      }else{
      if(DB::table('casillas_boletas')->where('TipoEleccion',$request->TipoEleccion)->where('Municipio',$request->Municipio)->where('Partido',$request->Partido)->delete()){
        return response()->json([
           'success' => true,
           'message' => 'Se eliminó el elemento'
        ],200);
      }else{
        return response()->json([
           'success' => false,
           'message' => 'Ocurrió un error al elimnar el elemento'
        ],500);
      }}
    }
    
    public function TablaInit(Request $request){
      $CasillasBoletas = new casillas_boletas;
      $tipoEleccion = $request->TipoEleccion;
      if ($request->TipoEleccion == 6 and $request->Municipio =='0') {
        $CasillasB = $CasillasBoletas->TablaFederal($request,$this->Entidad);
      }else{
      switch ($tipoEleccion) {
        case 1:          
        case 2:
        case 4:
          $CasillasB = $CasillasBoletas->TablaFederal($request,$this->Entidad);
        break;
        case 3:
          $CasillasB = $CasillasBoletas->TablaDiputados($request,'DF','distrito_federal',$this->Entidad);
          break;
        case 5:
          $CasillasB = $CasillasBoletas->TablaDiputados($request,'DL','distrito_local',$this->Entidad);
          break;
        default:
          $CasillasB = $CasillasBoletas->Tabla($request,$this->Entidad);
          break;
        }
      }
      $CasillasB->withPath('acta_casillas');

      if ($tipoEleccion == 3 || $tipoEleccion == 5) {
        return view ('actacasillas.tabla.tablaactacasillas-diputados',[
            'Tabla' => $CasillasB
        ]);
      }else{
        return view ('actacasillas.tabla.tablaactacasillas',[
            'Tabla' => $CasillasB
        ]);
      }
    }

    public function diputados(Request $request){
        if($request->DIPUTADO=='DIPUTADO FEDERAL'){
            $DF = distrito_federal::select('Distrito','Cabecera')->where('Entidad',$request->ESTADO)->get();
        }
        if ($request->DIPUTADO=='DIPUTADO LOCAL') {
            $DF = distrito_local::select('Distrito','Cabecera')->where('Entidad',$request->ESTADO)->get();
        }
        $response = [
            'status'=>'success',
            'html'=>view('actacasillas.select.distritos',compact('DF'))->render()
        ];
        return response()->json($response);
    }
}