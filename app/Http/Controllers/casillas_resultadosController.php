<?php

namespace App\Http\Controllers;

use DB;
use App\Municipio;
use App\distrito_federal;
use App\distrito_local;
use App\casillas_resultados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class casillas_resultadosController extends Controller
{
    public $Entidad=27; 
    public function init(){
        $UserLogged = Auth::user();
        $Municipios = new  Municipio;
        $TipoE = DB::table('cat_tipo_eleccion')->get();
        $Muni = $Municipios->dataComboMpioByUserNivel($UserLogged,$this->Entidad);
        return view('resultado_casillas.resultado_casillas',[
            'TipoE' => $TipoE,
            'Muni' => []
        ]);
    }
    public function getMunicipioTipoElección(Request $request){
        $Municipios = DB::table(DB::raw('casillas_boletas AS CB'))->selectRaw("CB.Municipio as Clave, M.Municipio, EMS.DistritoF");
        $Municipios->leftJoin(DB::raw('cat_municipio as M'),'M.Clave','=','CB.Municipio');
        $Municipios->rightJoin(DB::raw("( select distinct Municipio, DistritoF from entidad_dto_mpio_seccion where Entidad = $this->Entidad) EMS"),'EMS.Municipio','=','CB.Municipio');
        $Municipios->where('CB.Entidad',$this->Entidad)->where('M.Entidad',$this->Entidad);
        $Municipios->where('CB.TipoEleccion',$request->TipoEleccion);
        $Municipios->orderBy('Clave','asc');
        $Municipios = $Municipios->distinct()->get();
        return response()->json([
            'status' => 'success',
            'html' =>view('resultado_casillas.select.select_municipios',compact('Municipios'))->render()
        ]);
    }
    public function Distritos(Request $request){
        if($request->DISTRITO=='FEDERAL'){
            $DISTRITO = distrito_federal::select('Distrito','Cabecera')->where('Entidad',$request->ESTADO)->get();
        }
        if ($request->DISTRITO=='LOCAL') {
            $DISTRITO = distrito_local::select('Distrito','Cabecera')->where('Entidad',$request->ESTADO)->get();
        }
        $response = [
            'status'=>'success',
            'html'=>view('resultado_casillas.select.select_distritos',compact('DISTRITO'))->render()
        ];
        return response()->json($response);
    }

    public function casillas(Request $request){
        $Casillas = DB::table('casillas_resultados')->select('CASILLA');
        $Casillas->where('ESTADO',$this->Entidad);
        $Casillas->where('MUNICIPIO',$request->Municipio);
        $Casillas->where('TIPOELECCION',$request->TipoEleccion);
        $Casillas = $Casillas->get();
        $response = [
            'status'=>'success',
            'html'=>view('resultado_casillas.select.select_casillas',compact('Casillas'))->render()
        ];
        return response()->json($response);
    }

    public function partidos(Request $request){
        $toFront = new casillas_resultados;
        $Partidos = $toFront->partidosTabla($request,null,$this->Entidad);
        $response = [
            'status'=>'success',
            'html'=>view('resultado_casillas.select.select_partidos',compact('Partidos'))->render()
        ];
        return response()->json($response);
    }

    public function storageResults(Request $request){
        $Result = DB::table('casillas_resultados');
        $Result->where('ESTADO',$this->Entidad)->where('TIPOELECCION',$request->TipoEleccion)->where('MUNICIPIO',$request->Municipio)->where('CASILLA',$request->Casilla);
        if ($Result->update(["$request->Partido" => $request->Total])) {
            return response()->json(
                [
                    "status" => "success",
                    "message" => "Guardado"
                ],200);
        }else{
            return response()->json(
                [
                    "status" => "danger",
                    "message" => "El valor ya se encuentra registrado"
                ]);
            }
    }
}
