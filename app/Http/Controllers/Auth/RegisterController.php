<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'id'=>9611111114,
            'idMunicipio' => 25,
            'Nombre' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'PinCode' => 11111,
            'Apellidos' => 'Example Example',
            'Sexo' => 'M',
            'idCandidato'=>12,
            'idOcupacion' => 1,
            'SerFuncionario' => 0,
            'idPerfil' => 7,
            'idEstatus' => 1,
            'isTablet' => 0,
            'Parentid' => 00,
            'Nivel' => 1,
            'idEstructura' => 3,
            'isFC' => 0,
            'idSegmento' => 1,
            'idColaborar' => 1,
            'idTipoEstructura' => 1,
            'idTipoNivel' => 3,
            'uidNivel' => 'R3',
            'TomoCapacitacion' => 0,
        ]);
    }
}
