<?php

namespace App\Http\Controllers;

use DB;
use App\rcs;
use App\Partidos;
use App\Municipio;
use App\Casillas;
use App\gto_colonias;
use App\rcs_aspirantes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class rcs_nuevoController extends Controller
{
    public function form(Request $request){
        if ($request->has('INE')){
            $query=DB::raw("SELECT P.id, P.idPartido, Po.Partido, P.ID_ESTADO, P.ID_MUNICIPIO, C.NombreMunicipio, P.SECCION, P.CASILLA, P.PROPIETARIO, P.INE, P.CURP, P.Nombre, P.Paterno, P.Materno, P.Sexo, P.LADA, P.TelCasa, P.Celular, P.email, P.Cpostal, P.Calle, P.NoExterior, P.NoInterior, P.FechaNacimiento, P.OCR, P.DFederal, P.DLocal, P.facebook, P.twitter, P.Comentarios, P.Colonia, P.Promotor, P.Capacitacion FROM ( SELECT C.ESTADO, M.Region, C.MUNICIPIO, M.Municipio AS NombreMunicipio, GS.POLIGONO, C.SECCION, C.CASILLA FROM cat_municipio M JOIN casillas C ON M.Clave = C.MUNICIPIO AND M.idEntidad = C.ESTADO JOIN gto_secciones GS ON GS.SECCION = C.SECCION AND GS.CVE_MPIO = C.MUNICIPIO WHERE C.ESTADO = 11 ) C left join ( SELECT id, idPartido, ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO, INE, Nombre, Paterno, Materno, CURP, Sexo, LADA, TelCasa, Celular, email, Cpostal, Calle, NoExterior, NoInterior, FechaNacimiento, OCR, DFederal, DLocal, facebook, twitter, Comentarios, Colonia, Promotor, Capacitacion FROM rcs ) P on ( P.ID_ESTADO = C.ESTADO and P.ID_MUNICIPIO = C.MUNICIPIO and P.SECCION = C.SECCION and P.CASILLA = C.CASILLA ) left join ( SELECT id, Partido FROM partidos ) Po on (P.idPartido = Po.id) where P.INE like '%$request->INE%'");
            $toEdit = DB::select($query);
            $toEdit= collect($toEdit);
        }
        $USERLOGGED =  Auth::user();
        if($USERLOGGED->idTipoNivel==1){
            abort(403,'Forbidden');
        }
        $mun = new Municipio;
        $Entidad = 11;

        $Municipios= $mun->dataComboMpioByUserNivel($USERLOGGED, $Entidad);
        $Partidos = Partidos::select('id','Partido')->whereIn('id', [1, 11, 13, 16])->get();
        if ($request->has('INE')){
           return view('rcs.rcs_replace',[
                'municipios' => $Municipios,
                'Secciones' => [],
                'Casilla' => [],
                'USRLGD' => $USERLOGGED,
                'Partidos' => $Partidos,
                "TOEDIT" => $toEdit
        ]);
        }else{
             return view('rcs.rcs_nuevoregistro',[
                'municipios' => $Municipios,
                'Secciones' => [],
                'Casilla' => [],
                'USRLGD' => $USERLOGGED,
                'Partidos' => $Partidos,
            ]);
        }
    }
    public function store(Request $request){
        // dd($request->all());
        $flag = rcs::where('INE', $request->INE)->exists();
	    if($flag){
    		return response()->json([
    			'success' => false,
    			'message' => 'El INE ya se encuentra registrado'
    		], 401);
	    }
        $RCS = new rcs;

        $token = '11-'.$request->ID_MUNICIPIO.'-'.$request->SECCION.'-'.$request->CASILLA.'-'.$request->PROPIETARIO;
        if($RCS->dataCheckIfExists($token, null)){
            return response()->json([
                'success' => false,
                'message' => 'El puesto en la casilla está ocupado'
            ], 401);
        }

        $RCS->idPartido=$request->idPartido;
        $RCS->idCandidato=1;
        $RCS->ID_ESTADO=11;
        $RCS->ID_MUNICIPIO=$request->ID_MUNICIPIO;
        $RCS->Cpostal=$request->Cpostal;
        $RCS->Calle=strtoupper($request->Calle);
        $RCS->NoExterior=$request->NoExterior;
        $RCS->NoInterior=$request->NoInterior;
        $RCS->SECCION=$request->SECCION;
        $RCS->DFederal=$request->DFederal;
        $RCS->DLocal=$request->DLocal;
        $RCS->CASILLA=$request->CASILLA;
        $RCS->PROPIETARIO=$request->PROPIETARIO;
        $RCS->INE=strtoupper($request->INE);
        $RCS->OCR=strtoupper($request->OCR);
        $RCS->CURP=strtoupper($request->CURP);
        $RCS->Nombre=strtoupper($request->Nombre);
        $RCS->Paterno=strtoupper($request->Paterno);
        $RCS->Materno=strtoupper($request->Materno);
        $RCS->Sexo=$request->Sexo;
        $RCS->FechaNacimiento=$request->FechaNacimiento;
        $RCS->LADA=$request->LADA;
        $RCS->TelCasa=$request->TelCasa;
        $RCS->Celular=$request->Celular;
        $RCS->Colonia=strtoupper($request->Colonia);
        $RCS->email=$request->email;
        $RCS->facebook=$request->facebook;
        $RCS->twitter=$request->twitter;
        $RCS->Comentarios=strtoupper($request->Comentarios);
        $RCS->FechaCreate=new \DateTime();
        $RCS->UserCreate=$request->user()->id;
        $RCS->Promotor=strtoupper($request->Promotor);
        $RCS->Capacitacion=$request->Capacitacion;
        if($request->ASPIRANTE==1){
             $RCS_aspirante =rcs_aspirantes::find($request->id);
             if ($RCS_aspirante->delete()){}
        }
        if($RCS->save()){
            return response()->json([
                'success' => 'Se guardó correctamente'
            ]);
        }
    }
    public function delete(Request $request){
        $RCS = rcs::find($request->ID);
        if ($RCS->delete()){
            $saveMotivo=[
                'Motivo' => $request->Motivo
            ];
            if(DB::table('rcs')->where('id',$request->ID)->update($saveMotivo)){
               return [
                    'success' => 'Se eliminó'
                ];
            }
        }
    }
    public function restore(Request $request){
        $RCS = rcs::withTrashed()->where('id',$request->ID);
        if ($RCS->restore()){
            return [
                'success' => 'Restaurado'
            ];
        }
    }
    public function savereplace(Request $request){
        
        $toUPDATE=[
            'idPartido' => $request->idPartido,
            'ID_MUNICIPIO' => $request->ID_MUNICIPIO,
            'SECCION' => $request->SECCION,
            'Cpostal' => $request->Cpostal,
            'Calle' => strtoupper($request->Calle),
            'NoExterior' => $request->NoExterior,
            'NoInterior' => $request->NoInterior,
            'FechaNacimiento'=> $request->FechaNacimiento,
            'OCR' => strtoupper($request->OCR),
            'DFederal' => $request->DFederal,
            'DLocal' => $request->DLocal,
            'LADA' => $request->LADA,
            'email' => $request->email,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'CASILLA' => $request->CASILLA,
            'PROPIETARIO' => $request->PROPIETARIO,
            'INE' => strtoupper($request->INE),
            'CURP' => strtoupper($request->CURP),
            'Nombre' => strtoupper($request->Nombre),
            'Paterno' => strtoupper($request->Paterno),
            'Materno' => strtoupper($request->Materno),
            'Sexo' => $request->Sexo,
            'TelCasa' => $request->TelCasa,
            'Celular' => $request->Celular,
            'Colonia' => strtoupper($request->Colonia),
            'Comentarios' => strtoupper($request->Comentarios),
            'Promotor' => strtoupper($request->Promotor),
            'Capacitacion' => $request->Capacitacion
            
        ];
        $RCS = new rcs;
        $token = '11-'.$request->ID_MUNICIPIO.'-'.$request->SECCION.'-'.$request->CASILLA.'-'.$request->PROPIETARIO;
        if($RCS->dataCheckIfExists($token, $request->INEANTERIOR)){
            return response()->json([
                'success' => 'El puesto en la casilla está ocupado'
            ]);
        }

        if(DB::table('rcs')->where('INE',$request->INEANTERIOR)->update($toUPDATE)){
            return response()->json([
                'success' => 'Se guardó correctamente'
            ]);
        }else{
            return response()->json([
                'success' => 'No hay nada que actualizar'
            ]);
        }
    }
    public function addcolonia(Request $request){
        $mun = new Municipio;
        $USERLOGGED =  Auth::user();
        $Entidad =11;
        $Municipios= $mun->dataComboMpioByUserNivel($USERLOGGED, $Entidad);
        return view('rcs.modals.rcs_addColonia',[
            'municipios' => $Municipios
        ]);
    }
    public function savecolonia(Request $request){
        $gto_colonias = new gto_colonias;
        $gto_colonias->Colonia = $request->Colonia;
        $gto_colonias->CVE_MPIO = $request->CVE_MPIO;
        $gto_colonias->SECCION = $request->SECCION;
        if($gto_colonias->save()){
            return response()->json([
                'success' => 'Se guardó correctamente'
            ]);
        }else{
            return response()->json([
                'success' => 'Lo sentimos ocurrió un error inesperado'
            ]);
        }
    }
}
