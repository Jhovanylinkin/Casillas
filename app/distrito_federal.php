<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class distrito_federal extends Model
{
    protected $table ='distrito_federal';
    public $timestamps = false;
}
