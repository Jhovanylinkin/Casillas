<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
            'idMunicipio',
            'Nombre',
            'email',
            'password',
            'PinCode',
            'Apellidos',
            'Sexo',
            'idCandidato',
            'idOcupacion',
            'SerFuncionario',
            'idPerfil',
            'idEstatus',
            'isTablet',
            'Parentid',
            'Nivel',
            'idEstructura',
            'isFC',
            'idSegmento',
            'idColaborar',
            'idTipoEstructura',
            'idTipoNivel',
            'uidNivel',
            'TomoCapacitacion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
