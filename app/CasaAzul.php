<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CasaAzul extends Model
{
    protected $table ='casa_amiga';
    public $timestamps = false;
}
