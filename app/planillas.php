<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class planillas extends Model
{
    protected $table ='planillas';
    public $timestamps = false;
    public function popup($request){
        $selectRaw = "TIPOELECCION, CARGO, ESTADO, MUNICIPIO_DISTRITO, NOMBRE, PARTIDO";
        $queryBody= DB::table(DB::raw('planillas'))->selectRaw($selectRaw);
        $queryBody->where('TIPOELECCION','like','%'.$request->TIPOELECCION.'%');
        $queryBody->where('PARTIDO',$request->PARTIDO);

        if ($request->TIPOELECCION=='PRESIDENTE MUNICIPAL' || $request->TIPOELECCION=='DIPUTADO FEDERAL'|| $request->TIPOELECCION=='DIPUTADO LOCAL') {
            $queryBody->where('ESTADO',$request->ESTADO)->where('MUNICIPIO_DISTRITO',$request->MUNICIPIO_DISTRITO);
        }
        return $queryBody->get();  
    }
    public function datatoTable($request) {
        # query Presidentes de larepublica -> query inicial de la tabla
        $selectRaw = "TIPOELECCION, CARGO, NOMBRE, PARTIDO";
        $queryBody = DB::table(DB::raw("planillas"))->selectRaw($selectRaw);
        $queryBody->where('CARGO','PRESIDENTE REPUBLICA');
        # PRESIDENTE MUNICIPAL
        if($request->TIPOELECCION=='PRESIDENTE MUNICIPAL'){
            $selectRaw = "P.ID, P.TIPOELECCION, P.CARGO, P.ESTADO, E.Entidad, P.MUNICIPIO_DISTRITO, M.Municipio, P.NOMBRE, P.PARTIDO";
            $queryBody = DB::table(DB::raw("(select id, Entidad from cat_entidad ) E"))->selectRaw($selectRaw);
            # left join cat_municipio
            $queryBody->leftJoin(DB::raw("( select idEntidad, Clave, Municipio from cat_municipio ) M"), function($join){
                $join->on('E.id','=','M.idEntidad');
            });
            # left join planillas
            $queryBody->leftJoin(DB::raw("( select ID, TIPOELECCION, CARGO, ESTADO, MUNICIPIO_DISTRITO, NOMBRE, PARTIDO from planillas ) P"), function($join){
                $join->on('E.id','=','P.ESTADO')
                     ->on('P.MUNICIPIO_DISTRITO','=','M.Clave');
            });
        }
        # DIPUTADO FEDERAL
        if($request->TIPOELECCION=='DIPUTADO FEDERAL'){
            $selectRaw = "P.ID, P.TIPOELECCION, P.CARGO, P.ESTADO, E.Entidad, P.MUNICIPIO_DISTRITO, DF.Cabecera, P.NOMBRE, P.PARTIDO";
            $queryBody = DB::table(DB::raw("(select id, Entidad from cat_entidad ) E"))->selectRaw($selectRaw);
            # left join distrito_federal
            $queryBody->leftJoin(DB::raw("(select Entidad, Distrito, Cabecera from distrito_federal ) DF"), function($join){
                $join->on('E.id','=','DF.Entidad');
            });
            # left join planillas
            $queryBody->leftJoin(DB::raw("( select ID, TIPOELECCION, CARGO, ESTADO, MUNICIPIO_DISTRITO, NOMBRE, PARTIDO from planillas ) P"), function($join){
                $join->on('E.id','=','P.ESTADO')
                     ->on('P.MUNICIPIO_DISTRITO','=','DF.Distrito');
            });
        }
        # DIPUTADO LOCAL
        if($request->TIPOELECCION=='DIPUTADO LOCAL'){
            $selectRaw = "P.ID, P.TIPOELECCION, P.CARGO, P.ESTADO, E.Entidad, P.MUNICIPIO_DISTRITO, DL.Cabecera, P.NOMBRE, P.PARTIDO";
            $queryBody = DB::table(DB::raw("(select id, Entidad from cat_entidad ) E"))->selectRaw($selectRaw);
            # left join distrito_federal
            $queryBody->leftJoin(DB::raw("( select Entidad, Distrito, Cabecera from distrito_local ) DL"), function($join){
                $join->on('E.id','=','DL.Entidad');
            });
            # left join planillas
            $queryBody->leftJoin(DB::raw("( select ID, TIPOELECCION, CARGO, ESTADO, MUNICIPIO_DISTRITO, NOMBRE, PARTIDO from planillas ) P"), function($join){
                $join->on('E.id','=','P.ESTADO')
                     ->on('P.MUNICIPIO_DISTRITO','=','DL.Distrito');
            });
        }
        # GOBERNADOR 
        if($request->TIPOELECCION=='GOBERNADOR'){
            $selectRaw = "P.TIPOELECCION, P.CARGO, E.id ESTADO, E.Entidad, P.NOMBRE, P.PARTIDO";
            $queryBody = DB::table(DB::raw("(select id, Entidad from cat_entidad ) E"))->selectRaw($selectRaw);
            # left join planillas
            $queryBody->leftJoin(DB::raw("(SELECT TIPOELECCION, CARGO, ESTADO, NOMBRE, PARTIDO FROM planillas ) P"), function($join){
                $join->on('E.id','=','P.ESTADO');
            });
        }
        # SENADOR
        if($request->TIPOELECCION=='SENADOR'){
            $queryBody=DB::table(DB::raw('planillas'))->selectRaw("TIPOELECCION,CARGO,NOMBRE,PARTIDO");
            $queryBody->where('CARGO','SENADOR FORMULA 1');
        }

        if($request->TIPOELECCION=='DIPUTADO FEDERAL' || $request->TIPOELECCION=='PRESIDENTE MUNICIPAL' || $request->TIPOELECCION=='DIPUTADO LOCAL' || $request->TIPOELECCION=='GOBERNADOR'){
            $queryBody->whereNotNull('P.TIPOELECCION');
            $queryBody->where('P.TIPOELECCION','like','%'.$request->TIPOELECCION.'%');
            $queryBody->where('P.CARGO',$request->TIPOELECCION);
        }


        if($request->has('PARTIDO')){
            $queryBody->where('PARTIDO',$request->PARTIDO);
        }
        if($request->has('ESTADO')){
            $queryBody->where('ESTADO',$request->ESTADO);
        }
        if($request->has('MUNICIPIO_DISTRITO')){
            $queryBody->where('MUNICIPIO_DISTRITO',$request->MUNICIPIO_DISTRITO);
        }
        if($request->has('NombreC')) {
              $queryBody->where('NOMBRE','like','%'.$request->NombreC.'%');
        }

        return $queryBody->get();
    }
}
