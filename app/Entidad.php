<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entidad extends Model
{
    protected $table= 'cat_entidad';
    public $timestamp = false;

    public function municipios(){
    	return $this->hasMany('App\Municipio', 'idEntidad');
    }
}
