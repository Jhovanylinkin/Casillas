<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class casillas_boletas extends Model
{
  protected $table ='casillas_boletas';
  public $timestamps = false;

  public function storage($request,$Entidad){
    $user = $request->user()->id;
    $Municipios = DB::table('cat_municipio')->selectRaw("Entidad,'$request->TipoEleccion' as TipoEleccion,Clave as Municipio,'$request->Partido' as Partido,'2' as Orden,$user as Username, now() as FechaHora")
    ->where('Entidad',$Entidad)->get();
    foreach ($Municipios as $M) {
      DB::table('casillas_boletas')->insert([
        'Entidad' => $M->Entidad,
        'TipoEleccion' => $M->TipoEleccion,
        'Municipio' => $M->Municipio,
        'Partido' => $M->Partido,
        'Username' => $M->Username,
        'FechaHora' => $M->FechaHora
      ]);
    }
    return true;
  }
  
  public function storageDiputados($request,$DFDL,$Entidad){
    $user = $request->user()->id;
    if($request->TipoEleccion == 3){
      $DT = "DistritoF DF, '1' DL";
    }elseif($request->TipoEleccion == 5){
      $DT = "DistritoL DL, '1' DF";
    }
    $selectRaw = "DISTINCT Entidad,'$request->TipoEleccion' as TipoEleccion, $DT, Municipio,'$request->Partido' as Partido,$user as Username, now() as FechaHora";
    $Municipios = DB::table('entidad_dto_mpio_seccion')->selectRaw($selectRaw)
    ->where('Entidad',$Entidad)
    ->where($DFDL,$request->Municipio)->get();
    foreach ($Municipios as $M) {
      $data[] = [
        'Entidad' => $M->Entidad,
        'TipoEleccion' => $M->TipoEleccion,
        'DF' => $M->DF,
        'DL' => $M->DL,
        'Municipio' => $M->Municipio,
        'Partido' => $M->Partido,
        'Username' => $M->Username,
        'FechaHora' => $M->FechaHora
      ];
    }
    try{
      DB::table('casillas_boletas')->insert($data);
      return true;
    }catch(\Exception $e){
      return $e->getMessage();
    }    
  }

  public function supr($request,$Entidad){
    $user = $request->user()->id;
    $Municipios = DB::table('cat_municipio')->selectRaw("Entidad,'$request->TipoEleccion' as TipoEleccion,Clave as Municipio,'$request->Partido' as Partido,'2' as Orden,$user as Username, now() as FechaHora")
    ->where('Entidad',$Entidad)->get();
    foreach ($Municipios as $M) {
      DB::table('casillas_boletas')
              ->where('Entidad',$M->Entidad)
              ->where('TipoEleccion',$M->TipoEleccion)
              ->where('Municipio',$M->Municipio)
              ->where('Partido',$M->Partido)
              ->delete();
    }
    return true;
  }
  public function validarDiputados($request,$DFDL){
    if($request->TipoEleccion == 3){
      $DT = "DF";
    }elseif($request->TipoEleccion == 5){
      $DT = "DL";
    }
    return DB::table('casillas_boletas')->where('TipoEleccion',$request->TipoEleccion)->where($DT,$request->Municipio)->where('Partido',$request->Partido)->count('Municipio');
  }
  public function TablaDiputados($request,$TipoD,$tablaD,$Entidad){
    $selectRaw="CB.Entidad as idE, CB.TipoEleccion as idTE, CB.Municipio as idM, CB.Partido as idPartido, E.Entidad, TE.TipoEleccion,DT.Distrito,DT.Cabecera,M.Municipio,P.Partido";
    $query = DB::table("casillas_boletas AS CB")
               ->leftJoin(DB::raw("( SELECT id, Entidad FROM cat_entidad ) E"), "E.id", "=", "CB.Entidad")
               ->leftJoin(DB::raw("( SELECT id, TipoEleccion FROM cat_tipo_eleccion ) TE"), "TE.id", "=", "CB.TipoEleccion")
               ->leftJoin(DB::raw("( SELECT Distrito, Cabecera FROM $tablaD WHERE Entidad = $Entidad ) DT"), "DT.Distrito", "=", "CB.$TipoD")
               ->leftJoin(DB::raw(" (SELECT * FROM cat_municipio WHERE Entidad=$Entidad) M"), "M.Clave", "=", "CB.Municipio")
               ->leftJoin(DB::raw(" ( SELECT id, Partido FROM partidos_resultados )P "), "P.id", "=", "CB.Partido")
               ->where("CB.TipoEleccion", $request->TipoEleccion)
               ->selectRaw($selectRaw)
               ->orderByRaw("CB.TipoEleccion, CB.Orden DESC");
    if ($request->has('Municipio')) {
      $query->where("CB.$TipoD",$request->Municipio);
    }
    return $query->paginate(10);
  }

  public function TablaFederal($request,$Entidad){
    $TotalMunicipios = DB::table('cat_municipio')->where('Entidad',$Entidad)->count('Clave');
    $SelectRaw="CB.Entidad as idE, CB.TipoEleccion as idTE,'0' as idM, CB.Partido as idPartido, E.Entidad, TE.TipoEleccion,'Todos' as Municipio, P.Partido";
    $queryBody = DB::table(DB::raw("(SELECT DISTINCT Partido,TipoEleccion,Entidad, Orden FROM casillas_boletas )CB"))
    ->selectRaw($SelectRaw);
    //leftJoin a casillas_boletas
    $queryBody->leftJoin(DB::raw("(SELECT TipoEleccion, count(Municipio) TotalMunicipios, Partido FROM casillas_boletas where TipoEleccion=$request->TipoEleccion group by Partido)CBT"), function($join){
      $join->on('CBT.TipoEleccion','=','CB.TipoEleccion')
           ->on('CBT.Partido','=','CB.Partido');
    });
    //leftJoin a cat_entidad
    $queryBody->leftJoin(DB::raw("(SELECT id, Entidad FROM cat_entidad )E"), 'E.id','=','CB.Entidad');
    //leftJoin a cat_tipo_eleccion
    $queryBody->leftJoin(DB::raw("(SELECT id, TipoEleccion FROM cat_tipo_eleccion )TE"),'CB.TipoEleccion','=','TE.id');
    //leftJoin a partidos
    $queryBody->leftJoin(DB::raw("(SELECT id, Partido FROM partidos_resultados )P"), 'P.id','=','CB.Partido');
    $queryBody->where('CBT.TotalMunicipios',$TotalMunicipios);
    $queryBody->groupBy('CB.TipoEleccion')->groupBy('P.Partido');
    $queryBody->orderByRaw('CB.TipoEleccion, CB.Orden DESC');
    return $queryBody->paginate(10);
  }

  
  public function Tabla($request,$Entidad){
    $SelectRaw="CB.Entidad as idE, CB.TipoEleccion as idTE, CB.Municipio as idM, CB.Partido as idPartido, CB.Orden, E.Entidad, TE.TipoEleccion, IF((CB.TipoEleccion=1 OR CB.TipoEleccion =2 OR CB.TipoEleccion=4),'Todos',M.Municipio) Municipio, P.Partido";
    $queryBody = DB::table(DB::raw("( SELECT Entidad, TipoEleccion, Municipio, Partido, Orden FROM casillas_boletas )CB"))
    ->selectRaw($SelectRaw);
    //leftJoin a cat_entidad
    $queryBody->leftJoin(DB::raw("( SELECT id, Entidad FROM cat_entidad )E"), 'E.id','=','CB.Entidad');
    //leftJoin a cat_municipio
    $queryBody->leftJoin(DB::raw("( SELECT id, idEntidad, Municipio, Clave FROM cat_municipio WHERE idEntidad = ".$Entidad." )M"), 'CB.Municipio','=','M.Clave');
    //leftJoin a cat_tipo_eleccion
    $queryBody->leftJoin(DB::raw("( SELECT id, TipoEleccion FROM cat_tipo_eleccion )TE"),'CB.TipoEleccion','=','TE.id');
    //leftJoin a partidos
    $queryBody->leftJoin(DB::raw("( SELECT id, Partido FROM partidos_resultados )P"), 'P.id','=','CB.Partido');
    if ($request->has('TipoEleccion')) {
      $queryBody->where('CB.TipoEleccion',$request->TipoEleccion);
    }
    if ($request->has('Municipio')) {
      if($request->Municipio == '0') {
      }else{$queryBody->where('CB.Municipio',$request->Municipio);}
    }
    $queryBody->groupBy('CB.TipoEleccion')->groupBy('P.Partido');
    $queryBody->orderByRaw('CB.TipoEleccion, CB.Municipio, CB.Orden DESC');
    return $queryBody->paginate(10);
  }
}
