<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class casillas_resultados extends Model
{
    protected $table ='casillas_resultados';
    public $timestamps = false;
    
    public function casillasBoletasR($request){
        $SelectRaw = "concat('Dato',P.id) CampoSave,P.Partido,P.Logo,P.Siglas,P.Color,P.id";
        $casillasBoletas = DB::table(DB::raw("casillas_boletas as CB"))->selectRaw($SelectRaw);
        $casillasBoletas->join(DB::raw("partidos as P"),'CB.Partido','=','P.id');
        $casillasBoletas->where('CB.TipoEleccion',$request->TipoEleccion);
        switch ($request->TipoEleccion) {
            case 1: # Presidente de la republica
            case 2: # Senador
            case 4: # Gobernadores 
                $casillasBoletas->distinct();
                break;
            case 3:
                $casillasBoletas->leftJoin(DB::raw('entidad_dto_mpio_seccion as EMS'),'EMS.Municipio','=','CB.Municipio');
                $casillasBoletas->addSelect('EMS.DistritoF');
                $casillasBoletas->where('EMS.Entidad',27);
                $casillasBoletas->where('EMS.DistritoF',$request->Municipio);
                $casillasBoletas->distinct();
                break;
            case 5:
                $casillasBoletas->leftJoin(DB::raw('entidad_dto_mpio_seccion as EMS'),'EMS.Municipio','=','CB.Municipio');
                $casillasBoletas->addSelect('EMS.DistritoL');
                $casillasBoletas->where('EMS.Entidad',27);
                $casillasBoletas->where('EMS.DistritoL',$request->Municipio);
                $casillasBoletas->distinct();
                break;
            case 6:
                $casillasBoletas->where('CB.Municipio',$request->Municipio);
                break;
        }
        return $casillasBoletas->get();
    }

    public function partidosTabla($request,$TipoRequest,$Estado){
        $casillasBoletasR = $this->casillasBoletasR($request);
        $casillasResultados = DB::table('casillas_resultados');
        foreach ($casillasBoletasR as $key) {
            $casillasResultados->addSelect(DB::raw("SUM($key->CampoSave) AS $key->CampoSave"));
        }
        $casillasResultados->where('ESTADO',$Estado)->where('TIPOELECCION',$request->TipoEleccion);

        switch ($request->TipoEleccion) {
            case 1: # Presidente de la republica
            case 2: # Senador
            case 4: # Gobernadores
            if($request->has('Municipio')){$casillasResultados->where('MUNICIPIO',$request->Municipio);}else{$casillasResultados->distinct();}
                break;
            case 3:
                $casillasResultados->where('DF',$request->Municipio);
                break;
            case 5:
                $casillasResultados->where('DL',$request->Municipio);
                break;
            case 6:
            # Presidentes Municipales
                $casillasResultados->where('MUNICIPIO',$request->Municipio);
                break;
        }

        if ($TipoRequest == 1) {
            return $casillasResultados->get();
        }else{
        return $casillasBoletasR;
        }
    }

    public function Tabla($request,$Estado){
        $casillasBoletasR = $this->casillasBoletasR($request);
        $casillasResultados = DB::table(DB::raw('casillas_resultados as CR'))->select('CR.MUNICIPIO','CR.CASILLA','CR.SECCION','M.Municipio as Mun');
        foreach ($casillasBoletasR as $key) {
            $casillasResultados->addSelect($key->CampoSave);
        }
        $casillasResultados->where('CR.ESTADO',$Estado)->where('CR.TIPOELECCION',$request->TipoEleccion);
        switch ($request->TipoEleccion) {
            case 1: # Presidente de la republica
            case 2: # Senador
            case 4: # Gobernadores
                if($request->has('Municipio')){$casillasResultados->where('CR.MUNICIPIO',$request->Municipio);}else{$casillasResultados->distinct();}
                break;
            case 3:
                $casillasResultados->where('CR.DF',$request->Municipio);
                break;
            case 5:
                $casillasResultados->where('CR.DL',$request->Municipio);
                break;
            case 6:
                $casillasResultados->where('CR.MUNICIPIO',$request->Municipio);
                break;
        }
        $casillasResultados->leftJoin(DB::raw("cat_municipio as M"),"CR.MUNICIPIO","=","M.Clave");
        $casillasResultados->where('M.Entidad',$Estado);
        return $casillasResultados->paginate(10);
    }
}
