<?php

namespace App;

use App\LogsActivities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Partidos extends Model
{
    //
    public $timestamps = false;
    protected $table ='partidos';

    public  function dataForTable(){
    	return DB::table('partidos AS P')
    	->selectRaw("P.id ,P.Partido ,P.Logo, P.Dato , P.LogoOficial,P.isCoalicion,P.isIndependiente , P.UserUpdate , DATE_FORMAT(P.FechaLastUpdate,'%d-%m-%Y') AS FechaLastUpdate")
        ->where('P.Estatus', 1)
        ->orderBy('P.id')
    	->paginate(15);
    }


    public  function dataFormEdit($partidoID){
        return DB::table('partidos AS P')
        ->selectRaw('P.id ,P.Partido ,P.Siglas ,P.Color , P.Dato , P.LogoOficial,P.isCoalicion,P.isIndependiente')
        ->where('P.id', $partidoID)
        ->first();
    }

    public function insertar_partido($data,$dataActivity){
        try {

        LogsActivities::create($dataActivity);

        DB::table('partidos')->insert($data);

        return DB::getPdo()->lastInsertId();

        } catch(\Exception $e){
           // do task when error
           //return $e->getMessage();   // insert query
            return "Error al Guardar los datos";
        }
    }

    public function update_partido(array $data,$dataActivity,$partidoID){
        try {
        DB::table('partidos')->where('id', $partidoID)->update($data);
        LogsActivities::create($dataActivity);
        return "Ok";
        } catch(\Exception $e){
           // do task when error
           //return $e->getMessage();   // insert query
            return "Error al Guardar los datos";
        }
    }

    public function destroy_partido(array $data,$dataActivity,$partidoID){
        try {
        DB::table('partidos')->where('id', $partidoID)->update($data);
        LogsActivities::create($dataActivity);
        return "Ok";
        } catch(\Exception $e){
           // do task when error
           //return $e->getMessage();   // insert query
            return "Error al Guardar los datos";
        }
    }
}
