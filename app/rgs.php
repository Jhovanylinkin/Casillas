<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class rgs extends Model
{
    protected $table ='rgs';
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    public function dataTable($User, $Entidad, $request){
        $TipoNivel = $User->idTipoNivel;
        $uidNivel = $User->uidNivel;
        $Region = $User->RRegion;
        $selectRaw = "R.id, M.Region, M.Municipio, S.POLIGONO, S.SECCION, R.INE, R.TotalCasillas, concat_ws(' ', R.Nombre, R.Paterno, R.Materno) Nombre";
        $queryBody = DB::table(DB::raw("( Select Entidad, Municipio, Clave, Region from cat_municipio where Entidad = 11 ) M"))
        ->selectRaw($selectRaw);

        # left join gto_secciones
        $queryBody->leftJoin(DB::raw("( Select SECCION, POLIGONO, CVE_MPIO from gto_secciones ) S"), function($join){
            $join->on('M.Clave','=','S.CVE_MPIO');
        });
        # left join rgs
        $queryBody->leftJoin(DB::raw("( select id, ID_ESTADO, ID_MUNICIPIO, Nombre, Paterno, Materno, INE, SECCION, TotalCasillas from rgs ) R"), function($join){
            $join->on('R.ID_ESTADO','=','M.Entidad')
                 ->on('R.ID_MUNICIPIO','=','S.CVE_MPIO')
                 ->on('R.SECCION','=','S.SECCION');
        });
        $queryBody->whereNotNull('INE');

        switch ($TipoNivel) {
            case 1:

                break;
            case 2:

                break;
            case 3:
                $queryBody->where('M.Region',"$uidNivel");
                break;
            case 4:
                if($User->RRegion == 'R7'){
                    $queryBody->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
                }else{
                    $queryBody->where('M.Clave','=',"$uidNivel");
                }
                break;
            case 5:
                $queryBody->where('S.POLIGONO','=',"$uidNivel");
                break;
            case 6:
                $queryBody->where('S.SECCION','=',"$uidNivel");
                break;
            case 8:
                $queryBody->whereRaw("S.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DISTRITO =".$uidNivel.")");
                break;
            case 9:
                $queryBody->whereRaw("S.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
                break;
            default:
                break;
        }
        if($request->has('INE')){
            $queryBody->where('R.INE', 'like', '%'.$request->INE.'%');
        }
        if($request->has('NOMBRE')){
            $queryBody->where('Nombre', 'like', '%'.$request->NOMBRE.'%');
        }
        if($request->has('SECCION')){
            $queryBody->where('S.SECCION',$request->SECCION);
        }
        if($request->has('REGION')){
            $queryBody->where('Region',$request->REGION);
        }
        if($request->has('MUNICIPIO')){
            $queryBody->where('M.Clave',$request->MUNICIPIO);
        }

        return $queryBody->get();
    }

    // funciones para filtrar las graficas
    public function chartRecruited($dateToChart,$User,$Entidad){
        $TipoNivel = $User->idTipoNivel;
        $uidNivel = $User->uidNivel;
        $Region = $User->RRegion;
        $selectRaw = "R.id, M.Region,R.Capacitacion, M.Municipio, S.POLIGONO, S.SECCION, R.INE, R.TotalCasillas, concat_ws(' ', R.Nombre, R.Paterno, R.Materno) Nombre";
        $queryBody = DB::table(DB::raw("( Select Entidad, Municipio, Clave, Region from cat_municipio where Entidad = 11 ) M"))
        ->selectRaw($selectRaw);
        # left join gto_secciones
        $queryBody->leftJoin(DB::raw("( Select SECCION, POLIGONO, CVE_MPIO from gto_secciones ) S"), function($join){
            $join->on('M.Clave','=','S.CVE_MPIO');
        });
        # left join rgs
        $queryBody->leftJoin(DB::raw("( select id, ID_ESTADO, ID_MUNICIPIO, Nombre, Paterno, Materno, INE, SECCION, TotalCasillas,Capacitacion from rgs ) R"), function($join){
            $join->on('R.ID_ESTADO','=','M.Entidad')
                 ->on('R.ID_MUNICIPIO','=','S.CVE_MPIO')
                 ->on('R.SECCION','=','S.SECCION');
        });
        $queryBody->whereNotNull('INE');
        switch ($TipoNivel) {
            case 1:

                break;
            case 2:

                break;
            case 3:
                $queryBody->where('M.Region',"$uidNivel");
                break;
            case 4:
                if($User->RRegion == 'R7'){
                    $queryBody->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
                }else{
                    $queryBody->where('M.Clave','=',"$uidNivel");
                }
                break;
            case 5:
                $queryBody->where('S.POLIGONO','=',"$uidNivel");
                break;
            case 6:
                $queryBody->where('S.SECCION','=',"$uidNivel");
                break;
            case 8:
                $queryBody->whereRaw("S.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DISTRITO =".$uidNivel.")");
                break;
            case 9:
                $queryBody->whereRaw("S.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
                break;
            default:
                break;
        }
        switch ($dateToChart) {
            # Reclutados
            case 1:
                break;
            # Capacitados
            case 2:
                $queryBody->where('Capacitacion','=',1);
                break;
            # Encuadrados
            case 3:
                $queryBody->whereNotNull('TotalCasillas');
                break;
        }
        return $queryBody->count();
    }

    public function totalRGS($User,$Entidad){
        $TipoNivel = $User->idTipoNivel;
        $uidNivel = $User->uidNivel;
        $MetaTotal = DB::table('avance_electoral_municipio');

        if ($TipoNivel == 5 || $TipoNivel == 6) {
          $selectRaw = "MR.MetaRGs as MetaRG";
          $MetaTotal = DB::table(DB::raw("(select MetaRGs, Clave from avance_electoral_municipio) MR"))->selectRaw($selectRaw);
          # leftJoin gto_secciones
          $MetaTotal->join('gto_secciones as GS',function($join)
          {
            $join->on('GS.CVE_MPIO','=','MR.Clave');
          });
        }

        switch ($TipoNivel) {
          case 3:
            $MetaTotal->where('Region',"$uidNivel");
            break;
          case 4:
            $MetaTotal->where('Clave',$uidNivel);
            break;
          case 5:
            $MetaTotal->where('GS.POLIGONO',"$uidNivel");
            break;
          case 6:
            $MetaTotal->where('GS.SECCION',$uidNivel);
            break;
          case 8:
              $MetaTotal->whereRaw("Clave IN (SELECT MUNICIPIO FROM numeralia WHERE ID_ESTADO = $Entidad AND DISTRITO=$uidNivel)");
            break;
          case 9;
              $MetaTotal->whereRaw("Clave IN (SELECT MUNICIPIO FROM numeralia WHERE ID_ESTADO = $Entidad AND DL=$uidNivel)");
            break;
        }
        return $MetaTotal->sum('MetaRGs');
    }
}
