<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gto_colonias extends Model
{
    protected $table ='gto_colonias';
    public $timestamps = false;
}
