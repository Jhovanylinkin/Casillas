<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class rcs_aspirantes extends Model
{
    protected $table ='rcs_aspirantes';
    public $timestamps = false;

    public function dataTable($User, $Entidad, $request, $dateToChart){
        $TipoNivel = $User->idTipoNivel;
        $uidNivel = $User->uidNivel;
        $Region = $User->RRegion;

        $selectRaw = "C.Entidad, C.NombreEntidad, C.Region, C.SECCION, C.Clave, C.NombreMunicipio, C.POLIGONO, P1.INE INE, concat_ws(' ', P1.Nombre, P1.Paterno, P1.Materno) Nombre";
        $queryBody = DB::table(DB::raw("( SELECT M.Region, M.Municipio AS NombreMunicipio, GS.POLIGONO, GS.SECCION, E.Entidad as NombreEntidad, M.Entidad, M.Clave FROM cat_municipio M JOIN gto_secciones GS ON GS.CVE_MPIO = M.Clave JOIN cat_entidad E ON E.id = M.Entidad WHERE M.Entidad = 11 ) C"))
        ->selectRaw($selectRaw);
        $queryBody->leftJoin(DB::raw("( SELECT ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO, INE, Nombre, Paterno, Materno, deleted_at FROM rcs_aspirantes where deleted_at is null ) P1"), function($join){
            $join->on('P1.ID_ESTADO','=','C.Entidad')
                 ->on('P1.ID_MUNICIPIO','=','C.Clave')
                 ->on('P1.SECCION','=','C.SECCION');
        });
        $queryBody->whereNotNull('INE');

        switch ($TipoNivel) {
            case 1:

                break;
            case 2:

                break;
            case 3:
                $queryBody->where('C.Region',"$uidNivel");
                break;
            case 4:
                if($User->RRegion == 'R7'){
                    $queryBody->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
                }else{
                    $queryBody->where('C.Clave','=',"$uidNivel");
                }
                break;
            case 5:
                $queryBody->where('C.POLIGONO','=',"$uidNivel");
                break;
            case 6:
                $queryBody->where('C.SECCION','=',"$uidNivel");
                break;
            case 8:
                $queryBody->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DISTRITO =".$uidNivel.")");
                break;
            case 9:
                $queryBody->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
                break;
            default:
                break;
        }
        switch ($dateToChart) {
          case 1:
            return $queryBody->count('P1.INE');
            break;
        }
        if($request->has('INE')){
            $queryBody->where('P1.INE', 'like', '%'.$request->INE.'%');
        }
        if($request->has('NOMBRE')){
            $queryBody->where('P1.Nombre', 'like', '%'.$request->NOMBRE.'%');
        }
        if($request->has('SECCION')){
            $queryBody->where('C.SECCION',$request->SECCION);
        }
        if($request->has('REGION')){
            $queryBody->where('Region',$request->REGION);
        }
        if($request->has('MUNICIPIO')){
            $queryBody->where('C.Clave',$request->MUNICIPIO);
        }

        return $queryBody->get();
    }
}
