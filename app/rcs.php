<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class rcs extends Model
{
    use SoftDeletes;
    protected $table ='rcs';
    public $timestamps = false;
    protected $dates = ['deleted_at'];

    public function dataForTable($user, $Entidad, $request){
    	$TipoNivel = $user->idTipoNivel;
    	$uidNivel = $user->uidNivel;
    	$Region = $user->RRegion;

    	$selectRaw = "C.ESTADO, C.Region, C.MUNICIPIO, C.NombreMunicipio, C.POLIGONO, C.SECCION, C.CASILLA, P1.INE INEP1, concat_ws(' ',P1.Nombre, P1.Paterno, P1.Materno) NombreP1, P2.INE INEP2, concat_ws(' ',P2.Nombre, P2.Paterno, P2.Materno) NombreP2, S1.INE INES1, concat_ws(' ',S1.Nombre, S1.Paterno, S1.Materno) NombreS1, S2.INE INES2, concat_ws(' ',S2.Nombre, S2.Paterno, S2.Materno) NombreS2";

    	$queryFull = DB::table(DB::raw("(SELECT C.ESTADO, M.Region, C.MUNICIPIO, M.Municipio AS NombreMunicipio, GS.POLIGONO, C.SECCION, C.CASILLA FROM cat_municipio M JOIN casillas C ON M.Clave = C.MUNICIPIO AND M.idEntidad = C.ESTADO JOIN gto_secciones GS ON GS.SECCION = C.SECCION AND GS.CVE_MPIO = C.MUNICIPIO WHERE C.ESTADO=".$Entidad.") AS C"))
    		->selectRaw($selectRaw);
    	//leftJoin a Propietario1
    	$queryFull->leftJoin(DB::raw("(SELECT ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO, INE, Nombre, Paterno, Materno, deleted_at FROM rcs where PROPIETARIO=1 and deleted_at is null) AS P1"), function($join){
    		$join->on('P1.ID_ESTADO', '=', 'C.ESTADO')
    			 ->on('P1.ID_MUNICIPIO', '=', 'C.MUNICIPIO')
    			 ->on('P1.SECCION', '=', 'C.SECCION')
    			 ->on('P1.CASILLA', '=', 'C.CASILLA');
    	});
    	//leftJoin a Propietario2
    	$queryFull->leftJoin(DB::raw("(SELECT ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO, INE, Nombre, Paterno, Materno,deleted_at FROM rcs where PROPIETARIO=2 and deleted_at is null) AS P2"), function($join){
    		$join->on('P2.ID_ESTADO', '=', 'C.ESTADO')
    			 ->on('P2.ID_MUNICIPIO', '=', 'C.MUNICIPIO')
    			 ->on('P2.SECCION', '=', 'C.SECCION')
    			 ->on('P2.CASILLA', '=', 'C.CASILLA');
    	});
    	//leftJoin a Suplente 1
    	$queryFull->leftJoin(DB::raw("(SELECT ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO, INE, Nombre, Paterno, Materno, deleted_at FROM rcs where PROPIETARIO=3 and deleted_at is null) AS S1"), function($join) {
    		$join->on('S1.ID_ESTADO', '=', 'C.ESTADO')
    			 ->on('S1.ID_MUNICIPIO', '=', 'C.MUNICIPIO')
    			 ->on('S1.SECCION', '=', 'C.SECCION')
    			 ->on('S1.CASILLA', '=', 'C.CASILLA');
    	});
    	//leftJoin a Suplente 2
    	$queryFull->leftJoin(DB::raw("(SELECT ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO, INE, Nombre, Paterno, Materno, deleted_at FROM rcs where PROPIETARIO=4 and deleted_at is null) AS S2"), function($join){
    		$join->on('S2.ID_ESTADO', '=', 'C.ESTADO')
    			 ->on('S2.ID_MUNICIPIO', '=', 'C.MUNICIPIO')
    			 ->on('S2.SECCION', '=', 'C.SECCION')
    			 ->on('S2.CASILLA', '=', 'C.CASILLA');
    	});

        switch ($TipoNivel) {
            case 1:

                break;
            case 2:

                break;
            case 3:
                $queryFull->where('C.Region',"$uidNivel");
                break;
            case 4:
                if($user->RRegion == 'R7'){
                    $queryFull->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
                }else{
                    $queryFull->where('C.MUNICIPIO','=',"$uidNivel");
                }
                break;
            case 5:
                $queryFull->where('C.POLIGONO','=',"$uidNivel");
                break;
            case 6:
                $queryFull->where('C.SECCION','=',"$uidNivel");
                break;
            case 8:
                $queryFull->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DISTRITO =".$uidNivel.")");
                break;
            case 9:
                $queryFull->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
                break;
            default:
                break;
        }

        if($request->has('NOMBRE')){
            $queryFull->where('P1.Nombre', 'like', '%'.$request->NOMBRE.'%')
                      ->orWhere('P2.Nombre', 'like', '%'.$request->NOMBRE.'%')
                      ->orWhere('S1.Nombre', 'like', '%'.$request->NOMBRE.'%')
                      ->orWhere('S2.Nombre', 'like', '%'.$request->NOMBRE.'%');
        }
        if($request->has('INE')){
            $queryFull->where('P1.INE', 'like', '%'.$request->INE.'%')
                      ->orWhere('P2.INE', 'like', '%'.$request->INE.'%')
                      ->orWhere('S1.INE', 'like', '%'.$request->INE.'%')
                      ->orWhere('S2.INE', 'like', '%'.$request->INE.'%');
        }
        if($request->has('SECCION')){
            $queryFull->where('C.SECCION',$request->SECCION);
        }
        if($request->has('REGION')){
            $queryFull->where('Region',$request->REGION);
        }
        if($request->has('MUNICIPIO')){
            $queryFull->where('MUNICIPIO',$request->MUNICIPIO);
        }
        if($request->has('CASILLA')){
            $queryFull->where('C.CASILLA',$request->CASILLA);
        }

    	return $queryFull->get();
    }



    public function tablaDeleted($user){
        $TipoNivel = $user->idTipoNivel;
        $uidNivel = $user->uidNivel;
        $selectRaw = "C.ESTADO, C.Region, C.MUNICIPIO, C.NombreMunicipio, C.POLIGONO, C.SECCION, C.CASILLA, P1.id, P1.Motivo, P1.INE INE, concat_ws(' ', P1.Nombre, P1.Paterno, P1.Materno) Nombre";
        $queryFull = DB::table(DB::raw("( SELECT C.ESTADO, M.Region, C.MUNICIPIO, M.Municipio AS NombreMunicipio, GS.POLIGONO, C.SECCION, C.CASILLA FROM cat_municipio M JOIN casillas C ON M.Clave = C.MUNICIPIO AND M.idEntidad = C.ESTADO JOIN gto_secciones GS ON GS.SECCION = C.SECCION AND GS.CVE_MPIO = C.MUNICIPIO WHERE C.ESTADO = 11 ) C"))
            ->selectRaw($selectRaw);
        $queryFull->leftJoin(DB::raw("( SELECT id, ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO, INE, Nombre, Paterno, Materno, deleted_at, Motivo FROM rcs where PROPIETARIO = 1 and deleted_at is not null or PROPIETARIO = 2 and deleted_at is not null or PROPIETARIO = 3 and deleted_at is not null or PROPIETARIO = 4 and deleted_at is not null ) P1"), function($join){
    		$join->on('P1.ID_ESTADO', '=', 'C.ESTADO')
    			 ->on('P1.ID_MUNICIPIO', '=', 'C.MUNICIPIO')
    			 ->on('P1.SECCION', '=', 'C.SECCION')
    			 ->on('P1.CASILLA', '=', 'C.CASILLA');
        });
        $queryFull->whereNotNull('INE');
        if ($TipoNivel == 3) {
            $queryFull->where('Region',"$uidNivel");
        }
        return $queryFull->get();
    }

    public function get_casillas_by_nivel($user, $Entidad){
        $TipoNivel = $user->idTipoNivel;
        $uidNivel = $user->uidNivel;

        if($TipoNivel == 1 || $TipoNivel ==7){
            return [];
        }

        $queryFull = DB::table('casillas AS C')->where('ESTADO', $Entidad)->distinct()->select('casilla');

        switch ($TipoNivel) {
            case 2:
                break;
            case 3:
                $queryFull->join('cat_municipio AS M', function($join){
                    $join->on('M.Clave', '=', 'C.MUNICIPIO')
                         ->on('M.Entidad', '=', 'C.ESTADO');
                })->where('M.Region', $uidNivel);
                break;
            case 4:
                if($user->RRegion == 'R7'){
                    $queryFull->where('MUNICIPIO', $user->RCveMpio);
                }else{
                    $queryFull->where('MUNICIPIO', $user->uidNivel);
                }
                break;
            case 5:
                $queryFull->join('gto_secciones AS SE', function($join){
                    $join->on('SE.CVE_MPIO', '=', 'C.MUNICIPIO')
                         ->on('SE.SECCION', '=', 'C.SECCION');
                })->where('SE.POLIGONO', $uidNivel);
                break;
            case 6:
                $queryFull->where('SECCION', $uidNivel);
                break;
            case 8:
                $queryFull->whereRaw("SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DISTRITO =".$uidNivel.")");
                break;
            case 9:
                $queryFull->whereRaw("SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
                break;
            default:

                break;
        }

        return $queryFull->get();
    }
    public function chartToUserLevel($dateToChart, $User, $Entidad){
      $TipoNivel = $User->idTipoNivel;
    	$uidNivel = $User->uidNivel;
    	$Region = $User->RRegion;
      $selectRaw = "C.ESTADO, C.Region, C.MUNICIPIO, C.NombreMunicipio, C.POLIGONO, C.SECCION, C.CASILLA, RCS.INE, concat_ws(' ',RCS.Nombre, RCS.Paterno, RCS.Materno) NombreRCS";

    	$queryFull = DB::table(DB::raw("(SELECT C.ESTADO, M.Region, C.MUNICIPIO, M.Municipio AS NombreMunicipio, GS.POLIGONO, C.SECCION, C.CASILLA FROM cat_municipio M JOIN casillas C ON M.Clave = C.MUNICIPIO AND M.idEntidad = C.ESTADO JOIN gto_secciones GS ON GS.SECCION = C.SECCION AND GS.CVE_MPIO = C.MUNICIPIO WHERE C.ESTADO=".$Entidad.") AS C"))
    		->selectRaw($selectRaw);
    	//leftJoin  RCS
    	$queryFull->leftJoin(DB::raw("(SELECT idPartido, ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO, INE, Nombre, Paterno, Materno, deleted_at FROM rcs where deleted_at is null) AS RCS"), function($join){
    		$join->on('RCS.ID_ESTADO', '=', 'C.ESTADO')
    			 ->on('RCS.ID_MUNICIPIO', '=', 'C.MUNICIPIO')
    			 ->on('RCS.SECCION', '=', 'C.SECCION')
    			 ->on('RCS.CASILLA', '=', 'C.CASILLA');
    	});
      $queryFull->whereNotNull('RCS.INE');
      switch ($TipoNivel) {
          case 1:

              break;
          case 2:

              break;
          case 3:
              $queryFull->where('C.Region',"$uidNivel");
              break;
          case 4:
              if($User->RRegion == 'R7'){
                  $queryFull->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
              }else{
                  $queryFull->where('C.MUNICIPIO','=',"$uidNivel");
              }
              break;
          case 5:
              $queryFull->where('C.POLIGONO','=',"$uidNivel");
              break;
          case 6:
              $queryFull->where('C.SECCION','=',"$uidNivel");
              break;
          case 8:
              $queryFull->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DISTRITO =".$uidNivel.")");
              break;
          case 9:
              $queryFull->whereRaw("C.SECCION IN (SELECT SECCION FROM numeralia WHERE ID_ESTADO = ".$Entidad." AND DL =".$uidNivel.")");
              break;
      }
      switch ($dateToChart) {
        case 1:
            $queryFull->where('RCS.PROPIETARIO',1);
            return $queryFull->count('RCS.INE');
          break;
        case 2:
            $queryFull->where('RCS.PROPIETARIO',2);
            return $queryFull->count('RCS.INE');
          break;
        case 3:
            $queryFull->where('RCS.PROPIETARIO',3);
            return $queryFull->count('RCS.INE');
          break;
        case 4:
            $queryFull->where('RCS.PROPIETARIO',4);
            return $queryFull->count('RCS.INE');
          break;
        case 5:
            $queryFull->where('RCS.idPartido',11);
            return $queryFull->count('RCS.INE');
          break;
        case 6:
            $queryFull->where('RCS.idPartido',13);
            return $queryFull->count('RCS.INE');
          break;
        case 7:
            $queryFull->where('RCS.idPartido',16);
            return $queryFull->count('RCS.INE');
          break;
      }
    }

    public function totalRCS($User,$Entidad){
      $TipoNivel = $User->idTipoNivel;
      $uidNivel = $User->uidNivel;
      $MetaTotal = DB::table('avance_electoral_municipio');

      if ($TipoNivel == 5 || $TipoNivel == 6) {
        $selectRaw = "MR.MetaRCs as MetaRCs";
        $MetaTotal = DB::table(DB::raw("(select MetaRCs, Clave from avance_electoral_municipio) MR"))->selectRaw($selectRaw);
        # leftJoin gto_secciones
        $MetaTotal->join('gto_secciones as GS',function($join)
        {
          $join->on('GS.CVE_MPIO','=','MR.Clave');
        });
      }
      switch ($TipoNivel) {
        case 3:
          $MetaTotal->where('Region',"$uidNivel");
          break;
        case 4:
          $MetaTotal->where('Clave',$uidNivel);
          break;
        case 5:
          $MetaTotal->where('GS.POLIGONO',"$uidNivel");
          break;
        case 6:
          $MetaTotal->where('GS.SECCION',$uidNivel);
          break;
        case 8:
            $MetaTotal->whereRaw("Clave IN (SELECT MUNICIPIO FROM numeralia WHERE ID_ESTADO = $Entidad AND DISTRITO=$uidNivel)");
          break;
        case 9;
            $MetaTotal->whereRaw("Clave IN (SELECT MUNICIPIO FROM numeralia WHERE ID_ESTADO = $Entidad AND DL=$uidNivel)");
          break;
      }
      return $MetaTotal->sum('MetaRCs');
    }
    public function get_regiones_by_nivel($user, $Entidad){
        $TipoNivel = $user->idTipoNivel;
        $uidNivel = $user->uidNivel;

        if($TipoNivel == 1 || $TipoNivel ==7 || $TipoNivel == 8 || $TipoNivel == 9){
            return [];
        }

        $queryFull = DB::table('cat_municipio AS M')->where('M.Entidad', $Entidad)->distinct()->select('M.Region');

        if($TipoNivel == 4 || $TipoNivel == 5 || $TipoNivel == 6 ){
            $queryFull->where('M.Region', $user->RRegion);
        }
        else if ($TipoNivel == 3) {
            $queryFull->where('M.Region', $uidNivel);
        }

        return $queryFull->get();
    }

    public function dataCheckIfExists($token, $INE){
        if(is_null($INE)){
            return DB::table('rcs')
            ->select('id')
            ->whereRaw("CONCAT_WS('-', ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO) LIKE '%".$token."%'")
            ->first();
        }else{
            return DB::table('rcs')
            ->select('id')
            ->whereRaw("CONCAT_WS('-', ID_ESTADO, ID_MUNICIPIO, SECCION, CASILLA, PROPIETARIO) LIKE '%".$token."%'")
            ->where('INE', '<>', $INE)
            ->first();
        }
    }
}
