<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class distrito_local extends Model
{
    protected $table ='distrito_local';
    public $timestamps = false;
}
