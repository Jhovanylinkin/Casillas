<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abogados extends Model
{
    protected $table ='abogados';
    public $timestamps = false;
}
