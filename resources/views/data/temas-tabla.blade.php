@unless($datosTabla->count()>0)
    <h4>Sin Informacion</h4>

@else
                                        <p> Mostrando registros del {{ $datosTabla->firstItem() }} al {{ $datosTabla->lastItem() }} de un total de {{ $datosTabla->total() }} registros </p>
                                        <table class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="all"><i class="fa fa-bookmark"></i>Cobertura</th>
                                                    <th class="all">Temas</th>
                                                    <th class="all col-sm-6">Descripcion</th>
                                                    <th class="all">Fecha Captura</th>
                                                    <th class="all">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                 @foreach($datosTabla as $datoActual)
                                                    <tr>
                                                        <td> {{$datoActual->Cobertura}} </td>
                                                        <td> 
                                                        @if($datoActual->isImportado==1)
                                                        <a href="javascript:;" onclick="f_popup_info('@php echo base64_encode($datoActual->id) @endphp','/temas-lista-excel','Documentos importados');"> <i class="fa fa-eye"></i> {{$datoActual->Nombre}} </a> 
                                                        @else
                                                        {{$datoActual->Nombre}}
                                                        @endif
                                                        </td>
                                                        <td> {{$datoActual->Descripcion}} </td>
                                                        <td> {{ \Carbon\Carbon::parse($datoActual->FechaCaptura)->format('d-m-Y')}} </td>
                                                        <td>
                                                            <div class="btn-group pull-right">
                                                                <button class="btn green-haze btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Accion
                                                                    <i class="fa fa-angle-down"></i>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right">
                                                                    
                                                                    @if($mprivilegio->Editar==1)
                                                                    <li>
                                                                        <a href="javascript:;" onclick="f_popup_info('@php echo base64_encode($datoActual->id) @endphp','/temas-edit','Editar Tema');">
                                                                        <i class="icon-pencil"></i> Editar </a>
                                                                    </li>
                                                                    @else
                                                                    <li>
                                                                        <a href="javascript:;" onclick="f_mensaje_sistema('Usted no tiene privilegios');">
                                                                        <i class="icon-pencil"></i> Editar </a>
                                                                    </li>
                                                                    @endif
                                                                    @if($mprivilegio->Eliminar==1)
                                                                    <li>
                                                                        <a href="javascript:;" onclick="f_delete_row('@php echo base64_encode($datoActual->id) @endphp','/temas/delete','{{$datoActual->Nombre}}');">
                                                                        <i class="icon-trash"></i> Eliminar </a>
                                                                    </li>
                                                                    @else
                                                                    <li>
                                                                        <a href="javascript:;" onclick="f_mensaje_sistema('Usted no tiene privilegios');">
                                                                        <i class="icon-trash"></i> Eliminar </a>
                                                                    </li>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                            </tbody>
                                            
                                        </table>
                                         {{$datosTabla->links()}}
@endunless