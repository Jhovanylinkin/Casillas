@extends('layouts.metronic') @section('Activo')
<a href="/pantalla3">
    <span class="selected"></span>
</a>
@endsection @section('content')

<div class="col-table">
    <div class="innerLR">
<div class="innerAll">

<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Distrito" data-placement="top">
<span class="input-group-addon inputLabel loadingDist"><i class="fa fa-sitemap text-primary"></i></span>
<select name="Distrito" class="form-control">
<option value="">Selecciona Distrito...</option>
        <option value="1">TUXTLA GUTIÉRREZ ORIENTE</option>
        <option value="2">TUXTLA GUTIÉRREZ PONIENTE</option>
        <option value="3">CHIAPA DE CORZO</option>
        <option value="4">VENUSTIANO CARRANZA</option>
        <option value="5">SAN CRISTÓBAL DE LAS CASAS</option>
        <option value="6">COMITÁN</option>
        <option value="7">OCOSINGO</option>
        <option value="8">YAJALÓN</option>
        <option value="9">PALENQUE</option>
        <option value="10">BOCHIL</option>
        <option value="11">PUEBLO NVO. SOLISTAHUACÁN</option>
        <option value="12">PICHUCALCO</option>
        <option value="13">COPAINALÁ</option>
        <option value="14">CINTALAPA</option>
        <option value="15">TONALÁ</option>
        <option value="16">HUIXTLA</option>
        <option value="17">MOTOZINTLA</option>
        <option value="18">TAPACHULA NORTE</option>
        <option value="19">TAPACHULA SUR</option>
        <option value="20">LAS MARGARITAS</option>
        <option value="21">TENEJAPA</option>
        <option value="22">CHAMULA</option>
        <option value="23">VILLAFLORES</option>
        <option value="24">CACAHOATÁN</option>
</select>
</div>
</div>
<div class="col-md-4"> 
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Municipio" data-placement="top">
        <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-globe text-primary"></i></span>
    <select name="Municipio" class="form-control">	<option value="">Selecciona Municipio...</option>
<option value="102">TUXTLA GUTIÉRREZ</option>
</select>
</div>
</div>
<div class="col-md-4">
<a id="btnXLS" href="#" target="_blank" class="btn btn-default no-ajaxify"><i class="fa fa-table"></i> Exportar Excel</a>
<a id="btnPDF" href="#" target="_blank" class="btn btn-default no-ajaxify"><i class="fa fa-file"></i> Exportar PDF</a>
</div>
<div class="clearfix"></div>
</div>
<div class="col-separator-h"></div>
<div class="col-table-row">
<div class="col-app col-unscrollable">
<div id="tablaheader" class="tablaheader strong"></div>
<div class="col-app col-unscrollable">
    <div id="divReporte" class="innerAll">	<div id="tablaDatos---">
            <br>
<table id="tablaDatos" class="table table-striped table-bordered margin-bottom-none">
<thead class="bg-gray">
<tr>
<th class="center" colspan="10"><center>DISTRITO: TUXTLA GUTIÉRREZ ORIENTE &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; MUNICIPIO: TUXTLA GUTIÉRREZ </center></th>
</tr>
<tr>
<th class="center" colspan="10">Fecha y hora de impresión: 19/abril/2018 15:52 hrs.</th>
</tr>
<tr>
<th class="center" colspan="5">Casillas capturadas: <span class="txtdatos">347 (99.14%)</span></th>
<th class="center" colspan="5">Casillas sin capturar: <span class="txtdatos">3 (0.86%)</span></th>
</tr>
<tr>
<th class="center" colspan="2">Casillas aprobadas: Identificación</th>
<th class="center" rowspan="3">Número de ARE</th>
<th class="center" colspan="3">Casillas reportadas</th>
<th class="center" colspan="3">Integración de las mesas directivas de casilla</th>
<th class="center" rowspan="3">Núm. de incidentes reportados</th>
</tr>
<tr>
<th class="center" rowspan="2">Núm. Sección</th>
<th class="center" rowspan="2">Tipo casilla</th>
<th class="center" colspan="2">Condición de instalación</th>
<th class="center" rowspan="2">Hora de instalación</th>
<th class="center" rowspan="2">Núm. de funcionarios presentes</th>
<th class="center" rowspan="2">Casillas sin funcionarios tomados de la fila</th>
<th class="center" rowspan="2">Núm. de funcionarios tomados de la fila</th>
</tr>
<tr>
<th class="center">Instalada</th>
<th class="center">No Instalada</th>
</tr>
<tr>
<th class="center">Total</th>
<th class="center"> 350</th>
<th class="center"></th>
<th class="center"> <span class="txtdatos">347</span></th>
<th class="center"> <span class="txtdatos">0</span></th>
<th class="center"></th>
<th class="center"> <span class="txtdatos">1004</span></th>
<th class="center"> <span class="txtdatos">0</span></th>
<th class="center"> <span class="txtdatos">145</span></th>
<th class="center"> <span class="txtdatos">10</span></th>
</tr>
<tr>
<th class="center">%</th>
<th class="center"></th>
<th class="center"></th>
<th class="center"> <span class="txtdatos">99.14%</span></th>
<th class="center"> <span class="txtdatos">0.00%</span></th>
<th class="center"></th>
<th class="center"> <span class="txtdatos">72.33%</span></th>
<th class="center"> <span class="txtdatos">0.00%</span></th>
<th class="center"> <span class="txtdatos">14.44%</span></th>
<th class="center"></th>
</tr>
</thead>
<tbody>
<tr>
<td class="center" width="100">1604</td>
<td class="center" width="100">B</td>
<td class="center" width="100">10</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1604</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">10</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1604</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">10</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1604</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">10</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1604</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">10</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1605</td>
<td class="center" width="100">B</td>
<td class="center" width="100">13</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1605</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">13</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1605</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">13</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:40</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1605</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">13</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1607</td>
<td class="center" width="100">B</td>
<td class="center" width="100">10</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1607</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">13</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:40</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1607</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">13</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1608</td>
<td class="center" width="100">B</td>
<td class="center" width="100">12</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1608</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">12</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1608</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">12</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1608</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">12</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1608</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">12</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1608</td>
<td class="center" width="100">C5</td>
<td class="center" width="100">12</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1610</td>
<td class="center" width="100">B</td>
<td class="center" width="100">54</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1610</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">54</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1610</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">54</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1610</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">54</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1610</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">54</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1610</td>
<td class="center" width="100">C5</td>
<td class="center" width="100">54</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1610</td>
<td class="center" width="100">E1</td>
<td class="center" width="100">53</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">1</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1610</td>
<td class="center" width="100">E1C1</td>
<td class="center" width="100">53</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1610</td>
<td class="center" width="100">E1C2</td>
<td class="center" width="100">53</td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1611</td>
<td class="center" width="100">B</td>
<td class="center" width="100">18</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1611</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">18</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1611</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">18</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1611</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">18</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1611</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">18</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1612</td>
<td class="center" width="100">B</td>
<td class="center" width="100">21</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:16</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1612</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">21</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:18</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1612</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">50</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1612</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">50</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1618</td>
<td class="center" width="100">B</td>
<td class="center" width="100">21</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:21</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">1</td>
</tr>
<tr>
<td class="center" width="100">1618</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">21</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:22</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1618</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">21</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:29</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1619</td>
<td class="center" width="100">B</td>
<td class="center" width="100">53</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1619</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">53</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1619</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">53</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1620</td>
<td class="center" width="100">B</td>
<td class="center" width="100">51</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:13</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1620</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">51</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:40</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1620</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">51</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1621</td>
<td class="center" width="100">B</td>
<td class="center" width="100">65</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:40</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1621</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">65</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:42</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1622</td>
<td class="center" width="100">B</td>
<td class="center" width="100">57</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1622</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">57</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:22</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1622</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">57</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:23</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1622</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">57</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:27</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1622</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">57</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1622</td>
<td class="center" width="100">E1</td>
<td class="center" width="100">56</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1622</td>
<td class="center" width="100">E1C1</td>
<td class="center" width="100">56</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1622</td>
<td class="center" width="100">E1C2</td>
<td class="center" width="100">56</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1623</td>
<td class="center" width="100">B</td>
<td class="center" width="100">59</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1623</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">59</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1623</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">59</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1624</td>
<td class="center" width="100">B</td>
<td class="center" width="100">59</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1624</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">59</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1624</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">59</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1625</td>
<td class="center" width="100">B</td>
<td class="center" width="100">60</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1625</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">60</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1625</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">60</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1625</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">60</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1626</td>
<td class="center" width="100">B</td>
<td class="center" width="100">65</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1626</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">65</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:37</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1626</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">65</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:45</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1627</td>
<td class="center" width="100">B</td>
<td class="center" width="100">64</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:45</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1627</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">64</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:45</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1628</td>
<td class="center" width="100">B</td>
<td class="center" width="100">51</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:44</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1628</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">51</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:37</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1628</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">51</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1629</td>
<td class="center" width="100">B</td>
<td class="center" width="100">52</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:50</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1629</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">52</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1629</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">52</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1629</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">52</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:50</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1629</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">52</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:08</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1630</td>
<td class="center" width="100">B</td>
<td class="center" width="100">50</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1630</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">50</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1630</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">50</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1631</td>
<td class="center" width="100">B</td>
<td class="center" width="100">49</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:14</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1631</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">49</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1631</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">49</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1632</td>
<td class="center" width="100">B</td>
<td class="center" width="100">48</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1632</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">48</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1632</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">48</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:26</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1653</td>
<td class="center" width="100">B</td>
<td class="center" width="100">49</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:12</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1653</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">49</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:18</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1653</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">49</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:14</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1654</td>
<td class="center" width="100">B</td>
<td class="center" width="100">47</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1654</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">47</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1655</td>
<td class="center" width="100">B</td>
<td class="center" width="100">64</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:45</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1655</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">64</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:45</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1655</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">64</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:40</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1656</td>
<td class="center" width="100">B</td>
<td class="center" width="100">64</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:26</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1656</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">64</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:40</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1657</td>
<td class="center" width="100">B</td>
<td class="center" width="100">66</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1657</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">66</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1657</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">66</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1657</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">66</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1657</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">66</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1658</td>
<td class="center" width="100">B</td>
<td class="center" width="100">68</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1658</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">68</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1658</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">68</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1658</td>
<td class="center" width="100">E1</td>
<td class="center" width="100">67</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:40</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1658</td>
<td class="center" width="100">E1C1</td>
<td class="center" width="100">67</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:40</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1659</td>
<td class="center" width="100">B</td>
<td class="center" width="100">85</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:48</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">1</td>
</tr>
<tr>
<td class="center" width="100">1659</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">85</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">1</td>
</tr>
<tr>
<td class="center" width="100">1659</td>
<td class="center" width="100">E1</td>
<td class="center" width="100">67</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1659</td>
<td class="center" width="100">E1C1</td>
<td class="center" width="100">67</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1659</td>
<td class="center" width="100">S1</td>
<td class="center" width="100">85</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1660</td>
<td class="center" width="100">B</td>
<td class="center" width="100">87</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1660</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">87</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1660</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">87</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1660</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">87</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1661</td>
<td class="center" width="100">B</td>
<td class="center" width="100">86</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:45</td>
<td class="center" width="100">4</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1661</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">86</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:30</td>
<td class="center" width="100">4</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1661</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">86</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:48</td>
<td class="center" width="100">4</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1661</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">86</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:55</td>
<td class="center" width="100">4</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1661</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">86</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">4</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1662</td>
<td class="center" width="100">B</td>
<td class="center" width="100">71</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1662</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">71</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:50</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1662</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">71</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:02</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1662</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">72</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1663</td>
<td class="center" width="100">B</td>
<td class="center" width="100">72</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1663</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">72</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:55</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1663</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">72</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1663</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">72</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1663</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">72</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1664</td>
<td class="center" width="100">B</td>
<td class="center" width="100">47</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1664</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">47</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1664</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">47</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1665</td>
<td class="center" width="100">B</td>
<td class="center" width="100">46</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1665</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">46</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1666</td>
<td class="center" width="100">B</td>
<td class="center" width="100">73</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1666</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">73</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:33</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1666</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">73</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1667</td>
<td class="center" width="100">B</td>
<td class="center" width="100">46</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1667</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">46</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1681</td>
<td class="center" width="100">B</td>
<td class="center" width="100">44</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1681</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">44</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">1</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1682</td>
<td class="center" width="100">B</td>
<td class="center" width="100">73</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1682</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">73</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1683</td>
<td class="center" width="100">B</td>
<td class="center" width="100">74</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1683</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">74</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:04</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1683</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">74</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1684</td>
<td class="center" width="100">B</td>
<td class="center" width="100">71</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1684</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">71</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1684</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">71</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1685</td>
<td class="center" width="100">B</td>
<td class="center" width="100">68</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1685</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">68</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1685</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">68</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:58</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1685</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">68</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1686</td>
<td class="center" width="100">B</td>
<td class="center" width="100">70</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:25</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1686</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">70</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:32</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1686</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">70</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:45</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1687</td>
<td class="center" width="100">B</td>
<td class="center" width="100">74</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1687</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">74</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:09</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1688</td>
<td class="center" width="100">B</td>
<td class="center" width="100">75</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1688</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">75</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1689</td>
<td class="center" width="100">B</td>
<td class="center" width="100">43</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1689</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">43</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1689</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">43</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1690</td>
<td class="center" width="100">B</td>
<td class="center" width="100">43</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1690</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">43</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1690</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">43</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1691</td>
<td class="center" width="100">B</td>
<td class="center" width="100">44</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1691</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">44</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1692</td>
<td class="center" width="100">B</td>
<td class="center" width="100">41</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1692</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">41</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1693</td>
<td class="center" width="100">B</td>
<td class="center" width="100">44</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1693</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">44</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:29</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1712</td>
<td class="center" width="100">B</td>
<td class="center" width="100">76</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:25</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1712</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">76</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1712</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">76</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:35</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1712</td>
<td class="center" width="100">S1</td>
<td class="center" width="100">76</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:27</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1713</td>
<td class="center" width="100">B</td>
<td class="center" width="100">76</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:35</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">1</td>
</tr>
<tr>
<td class="center" width="100">1713</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">76</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:31</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1713</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">76</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:25</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1714</td>
<td class="center" width="100">B</td>
<td class="center" width="100">75</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:03</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1714</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">75</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1714</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">75</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:45</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1715</td>
<td class="center" width="100">B</td>
<td class="center" width="100">77</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1715</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">77</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1715</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">77</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1716</td>
<td class="center" width="100">B</td>
<td class="center" width="100">70</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1716</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">70</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:35</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1716</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">70</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:35</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1717</td>
<td class="center" width="100">B</td>
<td class="center" width="100">78</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1717</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">78</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1717</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">78</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1717</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">78</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1718</td>
<td class="center" width="100">B</td>
<td class="center" width="100">78</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1718</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">78</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1719</td>
<td class="center" width="100">B</td>
<td class="center" width="100">77</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">1</td>
</tr>
<tr>
<td class="center" width="100">1719</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">77</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">1</td>
</tr>
<tr>
<td class="center" width="100">1720</td>
<td class="center" width="100">B</td>
<td class="center" width="100">40</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:35</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1720</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">40</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:55</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1720</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">40</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:20</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1721</td>
<td class="center" width="100">B</td>
<td class="center" width="100">41</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1721</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">41</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1721</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">41</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:05</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1727</td>
<td class="center" width="100">B</td>
<td class="center" width="100">35</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">10:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1727</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">35</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:40</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1727</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">35</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1728</td>
<td class="center" width="100">B</td>
<td class="center" width="100">40</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">1</td>
</tr>
<tr>
<td class="center" width="100">1728</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">40</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:15</td>
<td class="center" width="100">1</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1729</td>
<td class="center" width="100">B</td>
<td class="center" width="100">79</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1729</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">79</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1729</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">79</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1729</td>
<td class="center" width="100">E1</td>
<td class="center" width="100">79</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1729</td>
<td class="center" width="100">E1C1</td>
<td class="center" width="100">79</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1729</td>
<td class="center" width="100">E1C2</td>
<td class="center" width="100">79</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1729</td>
<td class="center" width="100">E1C3</td>
<td class="center" width="100">79</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1730</td>
<td class="center" width="100">B</td>
<td class="center" width="100">38</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1730</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">38</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1730</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">38</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1730</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">38</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1730</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">38</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1731</td>
<td class="center" width="100">B</td>
<td class="center" width="100">39</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:12</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1731</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">39</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1731</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">39</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1731</td>
<td class="center" width="100">E1</td>
<td class="center" width="100">39</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1731</td>
<td class="center" width="100">E1C1</td>
<td class="center" width="100">39</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1732</td>
<td class="center" width="100">B</td>
<td class="center" width="100">36</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1732</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">36</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">07:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1732</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">36</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1732</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">36</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1742</td>
<td class="center" width="100">B</td>
<td class="center" width="100">106</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1742</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">106</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1742</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">106</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1742</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">106</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1742</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">106</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1742</td>
<td class="center" width="100">E1</td>
<td class="center" width="100">105</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">10:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1742</td>
<td class="center" width="100">E1C1</td>
<td class="center" width="100">105</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:40</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">B</td>
<td class="center" width="100">80</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">80</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:35</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">81</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">81</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">E1</td>
<td class="center" width="100">81</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">E1C1</td>
<td class="center" width="100">81</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">E1C2</td>
<td class="center" width="100">81</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">E1C3</td>
<td class="center" width="100">81</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">E1C4</td>
<td class="center" width="100">81</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:16</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">E2</td>
<td class="center" width="100">80</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:40</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">E2C1</td>
<td class="center" width="100">80</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:47</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">E2C2</td>
<td class="center" width="100">80</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:42</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1743</td>
<td class="center" width="100">E2C3</td>
<td class="center" width="100">80</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1750</td>
<td class="center" width="100">B</td>
<td class="center" width="100">61</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1750</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">61</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1750</td>
<td class="center" width="100">C2</td>
<td class="center" width="100">61</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1750</td>
<td class="center" width="100">C3</td>
<td class="center" width="100">61</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1750</td>
<td class="center" width="100">C4</td>
<td class="center" width="100">61</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:25</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1750</td>
<td class="center" width="100">C5</td>
<td class="center" width="100">61</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:12</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1750</td>
<td class="center" width="100">C6</td>
<td class="center" width="100">62</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1750</td>
<td class="center" width="100">C7</td>
<td class="center" width="100">62</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1751</td>
<td class="center" width="100">B</td>
<td class="center" width="100">69</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:32</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1751</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">69</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1751</td>
<td class="center" width="100">E1</td>
<td class="center" width="100">69</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1751</td>
<td class="center" width="100">E1C1</td>
<td class="center" width="100">69</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1752</td>
<td class="center" width="100">B</td>
<td class="center" width="100">74</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1754</td>
<td class="center" width="100">B</td>
<td class="center" width="100">63</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1754</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">63</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1930</td>
<td class="center" width="100">B</td>
<td class="center" width="100">13</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1931</td>
<td class="center" width="100">B</td>
<td class="center" width="100">14</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1932</td>
<td class="center" width="100">B</td>
<td class="center" width="100">14</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1933</td>
<td class="center" width="100">B</td>
<td class="center" width="100">14</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1934</td>
<td class="center" width="100">B</td>
<td class="center" width="100">16</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1935</td>
<td class="center" width="100">B</td>
<td class="center" width="100">16</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1936</td>
<td class="center" width="100">B</td>
<td class="center" width="100">14</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">1</td>
</tr>
<tr>
<td class="center" width="100">1937</td>
<td class="center" width="100">B</td>
<td class="center" width="100">14</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1938</td>
<td class="center" width="100">B</td>
<td class="center" width="100">16</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:26</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1939</td>
<td class="center" width="100">B</td>
<td class="center" width="100">17</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1940</td>
<td class="center" width="100">B</td>
<td class="center" width="100">14</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1941</td>
<td class="center" width="100">B</td>
<td class="center" width="100">14</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:36</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">3</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1942</td>
<td class="center" width="100">B</td>
<td class="center" width="100">16</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:25</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">3</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1943</td>
<td class="center" width="100">B</td>
<td class="center" width="100">55</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1944</td>
<td class="center" width="100">B</td>
<td class="center" width="100">16</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1945</td>
<td class="center" width="100">B</td>
<td class="center" width="100">18</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1946</td>
<td class="center" width="100">B</td>
<td class="center" width="100">55</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1947</td>
<td class="center" width="100">B</td>
<td class="center" width="100">9</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:43</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1948</td>
<td class="center" width="100">B</td>
<td class="center" width="100">9</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1949</td>
<td class="center" width="100">B</td>
<td class="center" width="100">17</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:45</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1950</td>
<td class="center" width="100">B</td>
<td class="center" width="100">16</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1951</td>
<td class="center" width="100">B</td>
<td class="center" width="100">55</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1952</td>
<td class="center" width="100">B</td>
<td class="center" width="100">55</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1953</td>
<td class="center" width="100">B</td>
<td class="center" width="100">55</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1954</td>
<td class="center" width="100">B</td>
<td class="center" width="100">17</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:05</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1955</td>
<td class="center" width="100">B</td>
<td class="center" width="100">16</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:33</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1956</td>
<td class="center" width="100">B</td>
<td class="center" width="100">17</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1957</td>
<td class="center" width="100">B</td>
<td class="center" width="100">9</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:53</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1958</td>
<td class="center" width="100">B</td>
<td class="center" width="100">55</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1959</td>
<td class="center" width="100">B</td>
<td class="center" width="100">55</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1960</td>
<td class="center" width="100">B</td>
<td class="center" width="100">57</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:22</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1961</td>
<td class="center" width="100">B</td>
<td class="center" width="100">9</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:26</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1962</td>
<td class="center" width="100">B</td>
<td class="center" width="100">9</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:45</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1963</td>
<td class="center" width="100">B</td>
<td class="center" width="100">62</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1964</td>
<td class="center" width="100">B</td>
<td class="center" width="100">63</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1965</td>
<td class="center" width="100">B</td>
<td class="center" width="100">9</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1966</td>
<td class="center" width="100">B</td>
<td class="center" width="100">62</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1967</td>
<td class="center" width="100">B</td>
<td class="center" width="100">62</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1968</td>
<td class="center" width="100">B</td>
<td class="center" width="100">62</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1969</td>
<td class="center" width="100">B</td>
<td class="center" width="100">63</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1970</td>
<td class="center" width="100">B</td>
<td class="center" width="100">62</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1971</td>
<td class="center" width="100">B</td>
<td class="center" width="100">61</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1972</td>
<td class="center" width="100">B</td>
<td class="center" width="100">63</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1973</td>
<td class="center" width="100">B</td>
<td class="center" width="100">63</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1974</td>
<td class="center" width="100">B</td>
<td class="center" width="100">65</td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">1975</td>
<td class="center" width="100">B</td>
<td class="center" width="100">65</td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100"></td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2012</td>
<td class="center" width="100">B</td>
<td class="center" width="100">56</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2013</td>
<td class="center" width="100">B</td>
<td class="center" width="100">56</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">10:14</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2014</td>
<td class="center" width="100">B</td>
<td class="center" width="100">56</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2015</td>
<td class="center" width="100">B</td>
<td class="center" width="100">56</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2016</td>
<td class="center" width="100">B</td>
<td class="center" width="100">58</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2017</td>
<td class="center" width="100">B</td>
<td class="center" width="100">86</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2018</td>
<td class="center" width="100">B</td>
<td class="center" width="100">86</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:15</td>
<td class="center" width="100">4</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2019</td>
<td class="center" width="100">B</td>
<td class="center" width="100">58</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2019</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">58</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2020</td>
<td class="center" width="100">B</td>
<td class="center" width="100">58</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2021</td>
<td class="center" width="100">B</td>
<td class="center" width="100">87</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2022</td>
<td class="center" width="100">B</td>
<td class="center" width="100">58</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2022</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">58</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2023</td>
<td class="center" width="100">B</td>
<td class="center" width="100">67</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2024</td>
<td class="center" width="100">B</td>
<td class="center" width="100">84</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2025</td>
<td class="center" width="100">B</td>
<td class="center" width="100">87</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:13</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2026</td>
<td class="center" width="100">B</td>
<td class="center" width="100">85</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2027</td>
<td class="center" width="100">B</td>
<td class="center" width="100">87</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2028</td>
<td class="center" width="100">B</td>
<td class="center" width="100">84</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2028</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">84</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2029</td>
<td class="center" width="100">B</td>
<td class="center" width="100">84</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2029</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">84</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2030</td>
<td class="center" width="100">B</td>
<td class="center" width="100">85</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2031</td>
<td class="center" width="100">B</td>
<td class="center" width="100">82</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:50</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2032</td>
<td class="center" width="100">B</td>
<td class="center" width="100">85</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">09:38</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2033</td>
<td class="center" width="100">B</td>
<td class="center" width="100">82</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:58</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2034</td>
<td class="center" width="100">B</td>
<td class="center" width="100">85</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">1</td>
</tr>
<tr>
<td class="center" width="100">2035</td>
<td class="center" width="100">B</td>
<td class="center" width="100">82</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2036</td>
<td class="center" width="100">B</td>
<td class="center" width="100">82</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2037</td>
<td class="center" width="100">B</td>
<td class="center" width="100">83</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:45</td>
<td class="center" width="100">2</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2038</td>
<td class="center" width="100">B</td>
<td class="center" width="100">82</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:10</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2039</td>
<td class="center" width="100">B</td>
<td class="center" width="100">82</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:30</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2040</td>
<td class="center" width="100">B</td>
<td class="center" width="100">69</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:00</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2040</td>
<td class="center" width="100">C1</td>
<td class="center" width="100">69</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"></td>
<td class="center" width="100">2</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2041</td>
<td class="center" width="100">B</td>
<td class="center" width="100">83</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2042</td>
<td class="center" width="100">B</td>
<td class="center" width="100">83</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">1</td>
</tr>
<tr>
<td class="center" width="100">2043</td>
<td class="center" width="100">B</td>
<td class="center" width="100">84</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:15</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2044</td>
<td class="center" width="100">B</td>
<td class="center" width="100">83</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:45</td>
<td class="center" width="100">2</td>
<td class="center" width="100"></td>
<td class="center" width="100">1</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2045</td>
<td class="center" width="100">B</td>
<td class="center" width="100">83</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:20</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
<tr>
<td class="center" width="100">2046</td>
<td class="center" width="100">B</td>
<td class="center" width="100">83</td>
<td class="center" width="100"><span class="label label-success"><i class="fa fa-check"></i></span></td>
<td class="center" width="100"></td>
<td class="center" width="100">08:51</td>
<td class="center" width="100">3</td>
<td class="center" width="100"><span class="label label-inverse"><i class="fa fa-times"></i></span></td>
<td class="center" width="100">0</td>
<td class="center" width="100">0</td>
</tr>
</tbody>
</table>
</div>
<script type="text/javascript">
var DATOS=new Array();
DATOS[0]='347 (99.14%)';
DATOS[1]='3 (0.86%)';
DATOS[2]='347';
DATOS[3]='0';
DATOS[4]='1004';
DATOS[5]='0';
DATOS[6]='145';
DATOS[7]='10';
DATOS[8]='99.14%';
DATOS[9]='0.00%';
DATOS[10]='72.33%';
DATOS[11]='0.00%';
DATOS[12]='14.44%';
$(".txtdatos").each(function(i, el) {
$(this).html(DATOS[i]);
});
$("#btnPDF").attr('href', 'reportes_instalacion_pdf.php?dist=MQ==&mun=MTAy');
$("#btnXLS").attr('href', 'reportes_instalacion_excel.php?dist=MQ==&mun=MTAy');
</script>
</div>
            </div>
        </div>
    </div>
</div>

@endsection @section('content_js')
{{-- <script src="{{ asset('js/planillas.js') }}"></script> --}}
 @stop