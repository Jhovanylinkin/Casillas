@extends('layouts.metronic') @section('Activo')
<a href="/pantalla4">
    <span class="selected"></span>
</a>
@endsection @section('content') 

<div class="tabbable-custom nav-justified">
<ul class="nav nav-tabs nav-justified">
<li class="active">
<a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="fa fa-plus"></i> Captura de Asistencia</a>
</li>
<li class="">
<a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-warning"></i> Sin Reportar</a>
</li>
<li class="">
<a href="#tab_3" data-toggle="tab" aria-expanded="falseS"><i class="fa fa-edit"></i> Reportados</a>
</li>
<li class="">
<a href="#tab_4" data-toggle="tab" aria-expanded="falseS"><i class="fa fa-bar-chart-o"></i> Mesa Directiva Stats</a>
</li>
<li class="">
<a href="#tab_5" data-toggle="tab" aria-expanded="falseS"><i class="fa fa-bar-chart-o"></i> Representantes Stats</a>
</li>
</ul>

<div class="tab-content">
<div class="tab-pane active" id="tab_1">
<form id="formCapturaAsistencia" novalidate="novalidate">
<div class="row indicaciones" style="font-size: 10px;">
<div class="col-md-4 col-lg-3">
<p>
</p><h5>1.- Mesa directiva:</h5>
En cada espacio seleccionar la opción que corresponda.
<p></p>
</div>

<div class="col-md-4 col-lg-3">
<p>
</p><h5>2.- Representantes acreditados en la casilla:</h5>
Se colocará el número de representantes acreditados en la casilla que se encuentren presentes al momento de la visita.
<p></p>
</div>

<div class="col-md-4 col-lg-3">
<p>
</p><h5>3.- Observadores electorales:</h5>
Marcar o desmarcar. Este dato es proporcionado por el Presidente de la Mesa Directiva de Casilla.
<p></p>
</div>
</div>

<div class="widget">
<div class="widget-head">
<h5 class="alert alert-info" role="alert"><b>Identificación</b></h5>
</div>

<div class="widget-body">
<div class="row">
<div class="col-md-4 col-lg-3">
<div class="form-group">
    <label class="control-label">Sección:</label>
    <div class="input-group">
        <input type="text" name="txtSeccion" id="txtSeccion" value="" placeholder="" class="form-control">
        <div class="input-group-btn">
            <button class="btn btn-default rounded-none" type="button" id="btnBuscar"><i class="fa fa-search"></i></button>
        </div>
    </div>
</div>
</div>

<div class="col-md-5 col-lg-4">
<div class="form-group">
    <label class="control-label">Casilla:</label>
    <select name="casilla" id="casilla" class="form-control"><option value="">Seleccione</option><option value="1 B">[B] BÁSICA</option><option value="1 C1">[C1] CONTIGUA 1</option><option value="1 C2">[C2] CONTIGUA 2</option><option value="1 C3">[C3] CONTIGUA 3</option><option value="1 E1">[E1] EXTRAORDINARIA 1</option><option value="1 E1C1">[E1C1] EXTRAORDINARIA 1 CONTIGUA 1</option></select>
</div>
</div>

<div class="col-md-3 col-lg-3">
<div class="form-group">
    <label class="control-label">Cae:</label>
    <input type="text" name="txtCae" id="txtCae" value="" placeholder="" class="form-control">
</div>

<div class="form-group">
    <label class="control-label">Celular:</label>
    <input type="text" name="txtCelCae" id="txtCelCae" value="" placeholder="" class="form-control">
</div>
</div>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6 col-lg-4">
<div class="widget">
<!-- MESA DIRECTIVA -->
<div class="widget-head">
<h5 class="alert alert-info" role="alert"><b>Integración de mesa directiva de casilla</b> <small><b>1</b></small></h5>
</div>

<div class="widget-body">
<div class="row">
    <div class="col-md-6 col-lg-6">
        <div class="form-group">
            <label class="control-label">Presidente:</label>
            
<div class="radio">
<label>
<input type="radio" name="presidente" value="1" checked="checked"> Propietario
</label>
</div>

<div class="radio">
<label>
<input type="radio" name="presidente" value="2"> Suplente general
</label>
</div>

<div class="radio">
<label>
<input type="radio" name="presidente" value="3"> Fila
</label>
</div>

<div class="radio">
<label>
<input type="radio" name="presidente" value="4"> Sin funcionario
</label>
</div>
            </div>
    </div>

    <div class="col-md-6 col-lg-6">
        <div class="form-group">
            <label class="control-label">Secretario:</label>
            
<div class="radio">
<label>
<input type="radio" name="secretario" value="1" checked="checked"> Propietario
</label>
</div>

<div class="radio">
<label>
<input type="radio" name="secretario" value="2"> Suplente general
</label>
</div>

<div class="radio">
<label>
<input type="radio" name="secretario" value="3"> Fila
</label>
</div>

<div class="radio">
<label>
<input type="radio" name="secretario" value="4"> Sin funcionario
</label>
</div>
            </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-lg-6">
        <div class="form-group">
            <label class="control-label">1er. Escrutador:</label>
            
<div class="radio">
<label>
<input type="radio" name="escrutador1" value="1"> Propietario
</label>
</div>

<div class="radio">
<label>
<input type="radio" name="escrutador1" value="2" checked="checked"> Suplente general
</label>
</div>

<div class="radio">
<label>
<input type="radio" name="escrutador1" value="3"> Fila
</label>
</div>

<div class="radio">
<label>
<input type="radio" name="escrutador1" value="4"> Sin funcionario
</label>
</div>
            </div>
    </div>

    <!-- <div class="col-md-6 col-lg-6">
        <div class="form-group">
            <label class="control-label">2do. Escrutador:</label>
                                        </div>
    </div> -->
</div>
</div>
</div>
</div>

<div class="col-md-6 col-lg-8">
<div class="widget">
<!-- REPRESENTANTES ACREDITADOS -->
<div class="widget-head">
<h5 class="alert alert-info" role="alert"><b>Número de representantes acreditados en la casilla</b> <small><b>2</b></small></h5>
</div>

<div class="widget-body">
<div class="row">
<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">PAN:</label> -->
    <img src="imagenes/adn.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtPan" id="txtPan" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">PRI:</label> -->
    <img src="img/partidos/3.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtPri" id="txtPri" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">PRD:</label> -->
    <img src="img/partidos/5.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtPrd" id="txtPrd" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">PVEM:</label> -->
    <img src="img/partidos/7.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtVerde" id="txtVerde" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">PT:</label> -->
    <img src="img/partidos/6.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtPt" id="txtPt" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">M. Ciudadano:</label> -->
    <img src="img/partidos/8.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtMovCiudadano" id="txtMovCiudadano" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">Nueva Alianza:</label> -->
    <img src="img/partidos/9.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtNuevaAlianza" id="txtNuevaAlianza" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">Morena:</label> -->
    <img src="img/partidos/12.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtMorena" id="txtMorena" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">P. Humanista:</label> -->
    <img src="img/partidos/13.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtHumanista" id="txtHumanista" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">Enc. Social:</label> -->
    <img src="img/partidos/14.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtEncuentroSoc" id="txtEncuentroSoc" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">Chiapas Unido:</label> -->
    <img src="img/partidos/10.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtChiapasUn" id="txtChiapasUn" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">Mover A Chiapas:</label> -->
    <img src="img/partidos/11.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtMoverAChiapas" id="txtMoverAChiapas" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">Independiente:</label> -->
    <img src="img/partidos/indep.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtIndependiente1" id="txtIndependiente1" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>

<div class="col-md-4 col-lg-3">
<div class="form-group">
    <!-- <label class="control-label">Independiente:</label> -->
    <img src="img/partidos/indep.png" alt="" class="center-block" style="width: 45px;">
    <input type="text" name="txtIndependiente2" id="txtIndependiente2" value="" placeholder="" class="numeros form-control" maxlength="1">
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="row">
<div class="col-md-offset-2 col-lg-offset-3 col-md-6 col-lg-3">
<div class="checkbox">
    <label>
        <input type="checkbox" name="presenciaObservadores" id="presenciaObservadores"> Presencia de observadores electorales
    </label>
</div>
</div>

<div class="col-md-3 col-lg-3">
<div class="form-group">
<label class="control-label">Hora de Visita:</label>
<!-- <div class="bootstrap-timepicker">
<input type="text" name="txtHoraVisita" id="txtHoraVisita" value="" placeholder="hh" class="form-control" maxlength="5" readonly="readonly" />
</div> -->
<div class="row">
<div class="col-md-3 col-lg-3">
    <input type="text" name="txtHoraVisita" id="txtHoraVisita" value="" placeholder="hh" class="form-control" maxlength="5">
</div>
<div class="col-md-3 col-lg-3">
    <input type="text" name="txtMinutoVisita" id="txtMinutoVisita" value="" placeholder="mm" class="form-control" maxlength="5">
</div>
</div>
</div>
</div>
</div>

<div class="form-group">
    <input type="button" id="btnGuardar" value="Guardar" class="btn btn-success center-block">
    <input type="hidden" name="distrito" id="distrito" value="16">
    <input type="hidden" name="municipio" id="municipio" value="1">
    <input type="hidden" name="op" value="guardarAsistencia">
</div>
</form>
</div>
{{-- fin  tab_1 --}}
<div class="tab-pane" id="tab_2">

<div class="tab-pane active" id="sinReportar">
<!-- casillas sin reportar -->
<span class="innerTB margin-none pull-right">Última actualización: <strong id="lastUpdate">2015-07-19 07:32:17</strong></span>

<br><br>

<div class="row">
<div class="col-md-6 col-lg-4">
<!-- distritos -->
<div class="box-generic">
<table class="table table-striped">
<thead>
<tr>
<th>Distrito</th>
<th>Total Casillas</th>
<th>Casillas No Reportadas</th>
</tr>
</thead>
<tbody>
                                                                                        <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">TUXTLA GUTIÉRREZ ORIENTE</a>
        <input type="hidden" name="" value="1">
    </td>
    <td>350</td>
    <td>3</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">TUXTLA GUTIÉRREZ PONIENTE</a>
        <input type="hidden" name="" value="2">
    </td>
    <td>317</td>
    <td>1</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">CHIAPA DE CORZO</a>
        <input type="hidden" name="" value="3">
    </td>
    <td>206</td>
    <td>4</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">VENUSTIANO CARRANZA</a>
        <input type="hidden" name="" value="4">
    </td>
    <td>128</td>
    <td>21</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">SAN CRISTÓBAL DE LAS CASAS</a>
        <input type="hidden" name="" value="5">
    </td>
    <td>240</td>
    <td>0</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">COMITÁN</a>
        <input type="hidden" name="" value="6">
    </td>
    <td>306</td>
    <td>1</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">OCOSINGO</a>
        <input type="hidden" name="" value="7">
    </td>
    <td>446</td>
    <td>93</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">YAJALÓN</a>
        <input type="hidden" name="" value="8">
    </td>
    <td>192</td>
    <td>12</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">PALENQUE</a>
        <input type="hidden" name="" value="9">
    </td>
    <td>236</td>
    <td>24</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">BOCHIL</a>
        <input type="hidden" name="" value="10">
    </td>
    <td>150</td>
    <td>13</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">PUEBLO NVO. SOLISTAHUACÁN</a>
        <input type="hidden" name="" value="11">
    </td>
    <td>94</td>
    <td>6</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">PICHUCALCO</a>
        <input type="hidden" name="" value="12">
    </td>
    <td>227</td>
    <td>11</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">COPAINALÁ</a>
        <input type="hidden" name="" value="13">
    </td>
    <td>172</td>
    <td>9</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">CINTALAPA</a>
        <input type="hidden" name="" value="14">
    </td>
    <td>296</td>
    <td>7</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">TONALÁ</a>
        <input type="hidden" name="" value="15">
    </td>
    <td>300</td>
    <td>15</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">HUIXTLA</a>
        <input type="hidden" name="" value="16">
    </td>
    <td>318</td>
    <td>3</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">MOTOZINTLA</a>
        <input type="hidden" name="" value="17">
    </td>
    <td>367</td>
    <td>26</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">TAPACHULA NORTE</a>
        <input type="hidden" name="" value="18">
    </td>
    <td>159</td>
    <td>17</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">TAPACHULA SUR</a>
        <input type="hidden" name="" value="19">
    </td>
    <td>244</td>
    <td>1</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">LAS MARGARITAS</a>
        <input type="hidden" name="" value="20">
    </td>
    <td>219</td>
    <td>14</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">TENEJAPA</a>
        <input type="hidden" name="" value="21">
    </td>
    <td>193</td>
    <td>9</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">CHAMULA</a>
        <input type="hidden" name="" value="22">
    </td>
    <td>296</td>
    <td>2</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">VILLAFLORES</a>
        <input type="hidden" name="" value="23">
    </td>
    <td>313</td>
    <td>19</td>
</tr>
                                                                                            <tr>
    <td>
        <a href="javascript:;" title="Detalle" class="busquedaNoRep">CACAHOATÁN</a>
        <input type="hidden" name="" value="24">
    </td>
    <td>189</td>
    <td>3</td>
</tr>
                                                                                    </tbody>
</table>
</div>
</div>

<div class="col-md-6 col-lg-8">
<!-- ul casillas -->
<div class="box-generic">
<span id="loading" style="display: none;"><i class="fa fa-spinner fa-spin fa-2x"></i></span>
                            <ul class="timeline-activity list-unstyled" id="ulSinReportar">
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1610 |
<a>Casilla:</a> E1C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE <br>
<a onclick="iniciaCaptura('1610', 'E1C2')" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Iniciar Captura</a>
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Navarrete Salazar Edgar Mauricio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1974 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE <br>
<a onclick="iniciaCaptura('1974', 'B')" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Iniciar Captura</a>
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Estrada López Cristina Hermila |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1975 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE <br>
<a onclick="iniciaCaptura('1975', 'B')" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Iniciar Captura</a>
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Estrada López Cristina Hermila |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li></ul>
</div>
</div>
</div>
</div>

</div>

<div class="tab-pane" id="tab_3">

        <div class="tab-pane active" id="reportados">
                <!-- casillas sin reportar -->
                <span class="innerTB margin-none pull-right">Última actualización: <strong>2015-07-19 07:32:17</strong></span>

                <br><br>

                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <!-- distritos -->
                        <div class="box-generic">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Distrito</th>
                                        <th>Total Casillas</th>
                                        <th>Casillas Reportadas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                                                                                                                <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">TUXTLA GUTIÉRREZ ORIENTE</a>
                                                <input type="hidden" name="" value="1">
                                            </td>
                                            <td>350</td>
                                            <td>347</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">TUXTLA GUTIÉRREZ PONIENTE</a>
                                                <input type="hidden" name="" value="2">
                                            </td>
                                            <td>317</td>
                                            <td>316</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">CHIAPA DE CORZO</a>
                                                <input type="hidden" name="" value="3">
                                            </td>
                                            <td>206</td>
                                            <td>202</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">VENUSTIANO CARRANZA</a>
                                                <input type="hidden" name="" value="4">
                                            </td>
                                            <td>128</td>
                                            <td>107</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">SAN CRISTÓBAL DE LAS CASAS</a>
                                                <input type="hidden" name="" value="5">
                                            </td>
                                            <td>240</td>
                                            <td>240</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">COMITÁN</a>
                                                <input type="hidden" name="" value="6">
                                            </td>
                                            <td>306</td>
                                            <td>305</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">OCOSINGO</a>
                                                <input type="hidden" name="" value="7">
                                            </td>
                                            <td>446</td>
                                            <td>353</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">YAJALÓN</a>
                                                <input type="hidden" name="" value="8">
                                            </td>
                                            <td>192</td>
                                            <td>180</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">PALENQUE</a>
                                                <input type="hidden" name="" value="9">
                                            </td>
                                            <td>236</td>
                                            <td>212</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">BOCHIL</a>
                                                <input type="hidden" name="" value="10">
                                            </td>
                                            <td>150</td>
                                            <td>137</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">PUEBLO NVO. SOLISTAHUACÁN</a>
                                                <input type="hidden" name="" value="11">
                                            </td>
                                            <td>94</td>
                                            <td>88</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">PICHUCALCO</a>
                                                <input type="hidden" name="" value="12">
                                            </td>
                                            <td>227</td>
                                            <td>216</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">COPAINALÁ</a>
                                                <input type="hidden" name="" value="13">
                                            </td>
                                            <td>172</td>
                                            <td>163</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">CINTALAPA</a>
                                                <input type="hidden" name="" value="14">
                                            </td>
                                            <td>296</td>
                                            <td>289</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">TONALÁ</a>
                                                <input type="hidden" name="" value="15">
                                            </td>
                                            <td>300</td>
                                            <td>285</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">HUIXTLA</a>
                                                <input type="hidden" name="" value="16">
                                            </td>
                                            <td>318</td>
                                            <td>315</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">MOTOZINTLA</a>
                                                <input type="hidden" name="" value="17">
                                            </td>
                                            <td>367</td>
                                            <td>341</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">TAPACHULA NORTE</a>
                                                <input type="hidden" name="" value="18">
                                            </td>
                                            <td>159</td>
                                            <td>142</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">TAPACHULA SUR</a>
                                                <input type="hidden" name="" value="19">
                                            </td>
                                            <td>244</td>
                                            <td>243</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">LAS MARGARITAS</a>
                                                <input type="hidden" name="" value="20">
                                            </td>
                                            <td>219</td>
                                            <td>205</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">TENEJAPA</a>
                                                <input type="hidden" name="" value="21">
                                            </td>
                                            <td>193</td>
                                            <td>184</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">CHAMULA</a>
                                                <input type="hidden" name="" value="22">
                                            </td>
                                            <td>296</td>
                                            <td>294</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">VILLAFLORES</a>
                                                <input type="hidden" name="" value="23">
                                            </td>
                                            <td>313</td>
                                            <td>294</td>
                                        </tr>
                                                                                                                                    <tr>
                                            <td>
                                                <a href="javascript:;" title="Detalle" class="busquedaRep">CACAHOATÁN</a>
                                                <input type="hidden" name="" value="24">
                                            </td>
                                            <td>189</td>
                                            <td>186</td>
                                        </tr>
                                                                                                                            </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-8">
                        <!-- ul casillas -->
                        <div class="box-generic">
                            <span id="loading2" style="display: none;"><i class="fa fa-spinner fa-spin fa-2x"></i></span>
                            <ul class="timeline-activity list-unstyled" id="ulReportadas">
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1604 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar Arellano Isabel Cristina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1604 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar Arellano Isabel Cristina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1604 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar Arellano Isabel Cristina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1604 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar Arellano Isabel Cristina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1604 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar Arellano Isabel Cristina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1605 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Juárez Díaz Manuel de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1605 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Juárez Díaz Manuel de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1605 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Juárez Díaz Manuel de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1605 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Juárez Díaz Manuel de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1607 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar Arellano Isabel Cristina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1607 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Juárez Díaz Manuel de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1607 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Juárez Díaz Manuel de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1608 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López  Reyes  María Julieta |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1608 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López  Reyes  María Julieta |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1608 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López  Reyes  María Julieta |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1608 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López  Reyes  María Julieta |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1608 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López  Reyes  María Julieta |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1608 |
<a>Casilla:</a> C5 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López  Reyes  María Julieta |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1610 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar López  Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1610 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar López  Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1610 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar López  Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1610 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar López  Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1610 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar López  Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1610 |
<a>Casilla:</a> C5 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Aguilar López  Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1610 |
<a>Casilla:</a> E1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Navarrete Salazar Edgar Mauricio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1610 |
<a>Casilla:</a> E1C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Navarrete Salazar Edgar Mauricio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1611 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Magdaleno Germain del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1611 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Magdaleno Germain del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1611 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Magdaleno Germain del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1611 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Magdaleno Germain del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1611 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Magdaleno Germain del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1612 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Ramírez Adriana |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1612 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Ramírez Adriana |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1612 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Araujo Martínez Dzuara Fabiola |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1612 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Araujo Martínez Dzuara Fabiola |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1618 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Ramírez Adriana |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1618 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Ramírez Adriana |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1618 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Ramírez Adriana |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1619 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Navarrete Salazar Edgar Mauricio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1619 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Navarrete Salazar Edgar Mauricio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1619 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Navarrete Salazar Edgar Mauricio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1620 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Gómez Aurelia Mercedes  |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1620 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Gómez Aurelia Mercedes  |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1620 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Gómez Aurelia Mercedes  |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1621 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Estrada López Cristina Hermila |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1621 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Estrada López Cristina Hermila |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1622 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Martínez Chavarría David |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1622 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Martínez Chavarría David |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1622 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Martínez Chavarría David |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1622 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Martínez Chavarría David |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1622 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Martínez Chavarría David |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1622 |
<a>Casilla:</a> E1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Mendoza Ruiz Marco Antonio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1622 |
<a>Casilla:</a> E1C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Mendoza Ruiz Marco Antonio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1622 |
<a>Casilla:</a> E1C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Mendoza Ruiz Marco Antonio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1623 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Esponda Cruz Juan Diego |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1623 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Esponda Cruz Juan Diego |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1623 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Esponda Cruz Juan Diego |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1624 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Esponda Cruz Juan Diego |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1624 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Esponda Cruz Juan Diego |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1624 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Esponda Cruz Juan Diego |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1625 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> De La Cruz Cruz Jesús Iván |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1625 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> De La Cruz Cruz Jesús Iván |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1625 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> De La Cruz Cruz Jesús Iván |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1625 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> De La Cruz Cruz Jesús Iván |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1626 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Estrada López Cristina Hermila |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1626 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Estrada López Cristina Hermila |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1626 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Estrada López Cristina Hermila |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1627 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Luismatus Ramírez Fricia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1627 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Luismatus Ramírez Fricia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1628 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Gómez Aurelia Mercedes  |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1628 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Gómez Aurelia Mercedes  |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1628 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Gómez Aurelia Mercedes  |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1629 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Ruiz María Veronica |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1629 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Ruiz María Veronica |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1629 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Ruiz María Veronica |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1629 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Ruiz María Veronica |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1629 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Ruiz María Veronica |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1630 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Araujo Martínez Dzuara Fabiola |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1630 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Araujo Martínez Dzuara Fabiola |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1630 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Araujo Martínez Dzuara Fabiola |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1631 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Trujillo Lorena Jorge Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1631 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Trujillo Lorena Jorge Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1631 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Trujillo Lorena Jorge Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1632 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Herrera Gamez Mario Valentin |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1632 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Herrera Gamez Mario Valentin |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1632 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Herrera Gamez Mario Valentin |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1653 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Trujillo Lorena Jorge Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1653 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Trujillo Lorena Jorge Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1653 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Trujillo Lorena Jorge Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1654 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Orantes Moguel Gabriela Lucía |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1654 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Orantes Moguel Gabriela Lucía |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1655 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Luismatus Ramírez Fricia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1655 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Luismatus Ramírez Fricia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1655 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Luismatus Ramírez Fricia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1656 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Luismatus Ramírez Fricia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1656 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Luismatus Ramírez Fricia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1657 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Burguete Jiménez Maria Fernanda |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1657 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Burguete Jiménez Maria Fernanda |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1657 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Burguete Jiménez Maria Fernanda |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1657 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Burguete Jiménez Maria Fernanda |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1657 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Burguete Jiménez Maria Fernanda |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1658 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alarcón Hernández Kenia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1658 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alarcón Hernández Kenia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1658 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alarcón Hernández Kenia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1658 |
<a>Casilla:</a> E1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Jiménez Robles Yojani Berenice |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1658 |
<a>Casilla:</a> E1C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Jiménez Robles Yojani Berenice |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1659 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molano Chacón Carmen Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1659 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molano Chacón Carmen Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1659 |
<a>Casilla:</a> E1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Jiménez Robles Yojani Berenice |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1659 |
<a>Casilla:</a> E1C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Jiménez Robles Yojani Berenice |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1659 |
<a>Casilla:</a> S1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molano Chacón Carmen Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1660 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Arce Narcía Elvia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1660 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Arce Narcía Elvia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1660 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Arce Narcía Elvia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1660 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Arce Narcía Elvia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1661 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Molina Jorge Edmundo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1661 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Molina Jorge Edmundo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1661 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Molina Jorge Edmundo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1661 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Molina Jorge Edmundo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1661 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Molina Jorge Edmundo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1662 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Páez Fernández Elia Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1662 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Páez Fernández Elia Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1662 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Páez Fernández Elia Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1662 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Espinosa Josue |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1663 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Espinosa Josue |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1663 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Espinosa Josue |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1663 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Espinosa Josue |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1663 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Espinosa Josue |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1663 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Espinosa Josue |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1664 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Orantes Moguel Gabriela Lucía |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1664 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Orantes Moguel Gabriela Lucía |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1664 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Orantes Moguel Gabriela Lucía |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1665 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cancino Gordillo  Andrés Alejandro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1665 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cancino Gordillo  Andrés Alejandro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1666 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> De la Cruz Gómez Luis Angel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1666 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> De la Cruz Gómez Luis Angel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1666 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> De la Cruz Gómez Luis Angel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1667 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cancino Gordillo  Andrés Alejandro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1667 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cancino Gordillo  Andrés Alejandro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1681 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escobar Santizo Blanca Eugenia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1681 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escobar Santizo Blanca Eugenia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1682 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> De la Cruz Gómez Luis Angel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1682 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> De la Cruz Gómez Luis Angel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1683 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Bolaños Guillén Rosa Elena |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1683 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Bolaños Guillén Rosa Elena |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1683 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Bolaños Guillén Rosa Elena |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1684 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Páez Fernández Elia Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1684 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Páez Fernández Elia Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1684 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Páez Fernández Elia Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1685 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alarcón Hernández Kenia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1685 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alarcón Hernández Kenia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1685 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alarcón Hernández Kenia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1685 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alarcón Hernández Kenia Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1686 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ortiz Camacho Rosalba |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1686 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ortiz Camacho Rosalba |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1686 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ortiz Camacho Rosalba |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1687 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Bolaños Guillén Rosa Elena |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1687 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Bolaños Guillén Rosa Elena |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1688 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández De la Cruz Abel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1688 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández De la Cruz Abel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1689 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Jiménez Selene Jazmine |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1689 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Jiménez Selene Jazmine |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1689 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Jiménez Selene Jazmine |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1690 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Jiménez Selene Jazmine |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1690 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Jiménez Selene Jazmine |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1690 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ozuna Jiménez Selene Jazmine |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1691 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escobar Santizo Blanca Eugenia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1691 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escobar Santizo Blanca Eugenia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1692 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gomez Mendez Dina Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1692 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gomez Mendez Dina Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1693 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escobar Santizo Blanca Eugenia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1693 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escobar Santizo Blanca Eugenia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1712 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Gonzalez Mauro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1712 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Gonzalez Mauro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1712 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Gonzalez Mauro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1712 |
<a>Casilla:</a> S1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Gonzalez Mauro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1713 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Gonzalez Mauro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1713 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Gonzalez Mauro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1713 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vázquez Gonzalez Mauro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1714 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández De la Cruz Abel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1714 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández De la Cruz Abel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1714 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández De la Cruz Abel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1715 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Zapata Daniel Arturo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1715 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Zapata Daniel Arturo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1715 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Zapata Daniel Arturo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1716 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ortiz Camacho Rosalba |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1716 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ortiz Camacho Rosalba |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1716 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Ortiz Camacho Rosalba |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1717 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Espinosa Guadalupe Alfonso |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1717 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Espinosa Guadalupe Alfonso |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1717 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Espinosa Guadalupe Alfonso |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1717 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Espinosa Guadalupe Alfonso |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1718 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Espinosa Guadalupe Alfonso |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1718 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Espinosa Guadalupe Alfonso |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1719 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Zapata Daniel Arturo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1719 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cruz Zapata Daniel Arturo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1720 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Plaza  Pérez Lourdes |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1720 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Plaza  Pérez Lourdes |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1720 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Plaza  Pérez Lourdes |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1721 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gomez Mendez Dina Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1721 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gomez Mendez Dina Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1721 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gomez Mendez Dina Matilde |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1727 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molina Alfaro  Rosa Isela |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1727 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molina Alfaro  Rosa Isela |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1727 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molina Alfaro  Rosa Isela |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1728 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Plaza  Pérez Lourdes |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1728 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Plaza  Pérez Lourdes |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1729 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Solis Pérez Josué Samuel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1729 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Solis Pérez Josué Samuel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1729 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Solis Pérez Josué Samuel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1729 |
<a>Casilla:</a> E1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Solis Pérez Josué Samuel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1729 |
<a>Casilla:</a> E1C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Solis Pérez Josué Samuel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1729 |
<a>Casilla:</a> E1C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Solis Pérez Josué Samuel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1729 |
<a>Casilla:</a> E1C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Solis Pérez Josué Samuel |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1730 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Interiano Castro  Margarita |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1730 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Interiano Castro  Margarita |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1730 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Interiano Castro  Margarita |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1730 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Interiano Castro  Margarita |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1730 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Interiano Castro  Margarita |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1731 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cancino Trujillo Jorge Alvaro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1731 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cancino Trujillo Jorge Alvaro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1731 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cancino Trujillo Jorge Alvaro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1731 |
<a>Casilla:</a> E1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cancino Trujillo Jorge Alvaro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1731 |
<a>Casilla:</a> E1C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Cancino Trujillo Jorge Alvaro |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1732 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Roblero Jimenez Nayeli |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1732 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Roblero Jimenez Nayeli |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1732 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Roblero Jimenez Nayeli |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1732 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Roblero Jimenez Nayeli |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1742 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> LOPEZ CIFUENTES GABRIEL |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1742 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> LOPEZ CIFUENTES GABRIEL |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1742 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> LOPEZ CIFUENTES GABRIEL |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1742 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> LOPEZ CIFUENTES GABRIEL |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1742 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> LOPEZ CIFUENTES GABRIEL |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1742 |
<a>Casilla:</a> E1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> CASTILLEJOS FLORES ISAIAS |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1742 |
<a>Casilla:</a> E1C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> CASTILLEJOS FLORES ISAIAS |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vidal Hernández Mario Alberto |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vidal Hernández Mario Alberto |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Victoria Betanzos César Giovanny |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Victoria Betanzos César Giovanny |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> E1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Victoria Betanzos César Giovanny |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> E1C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Victoria Betanzos César Giovanny |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> E1C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Victoria Betanzos César Giovanny |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> E1C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Victoria Betanzos César Giovanny |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> E1C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Victoria Betanzos César Giovanny |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> E2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vidal Hernández Mario Alberto |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> E2C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vidal Hernández Mario Alberto |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> E2C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vidal Hernández Mario Alberto |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1743 |
<a>Casilla:</a> E2C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Vidal Hernández Mario Alberto |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1750 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alvarado González Amilcar |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1750 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alvarado González Amilcar |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1750 |
<a>Casilla:</a> C2 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alvarado González Amilcar |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1750 |
<a>Casilla:</a> C3 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alvarado González Amilcar |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1750 |
<a>Casilla:</a> C4 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alvarado González Amilcar |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1750 |
<a>Casilla:</a> C5 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alvarado González Amilcar |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1750 |
<a>Casilla:</a> C6 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escalante Castañeda Teresa de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1750 |
<a>Casilla:</a> C7 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escalante Castañeda Teresa de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1751 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Saldaña Molina Carlos Freddy |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1751 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Saldaña Molina Carlos Freddy |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1751 |
<a>Casilla:</a> E1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Saldaña Molina Carlos Freddy |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1751 |
<a>Casilla:</a> E1C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Saldaña Molina Carlos Freddy |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1752 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Bolaños Guillén Rosa Elena |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1754 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gómez Dorantes Elena Josefina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1754 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gómez Dorantes Elena Josefina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1930 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Juárez Díaz Manuel de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1931 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Ruiz Yesenia del Rocío |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1932 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Ruiz Yesenia del Rocío |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1933 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Ruiz Yesenia del Rocío |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1934 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villanueva Martínez Consuelo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1935 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villanueva Martínez Consuelo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1936 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Ruiz Yesenia del Rocío |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1937 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Ruiz Yesenia del Rocío |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1938 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villanueva Martínez Consuelo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1939 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Jiménez Fuentevilla Juan José |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1940 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Ruiz Yesenia del Rocío |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1941 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Ruiz Yesenia del Rocío |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1942 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villanueva Martínez Consuelo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1943 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molina  López Carlos Martín |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1944 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villanueva Martínez Consuelo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1945 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Pérez Magdaleno Germain del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1946 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molina  López Carlos Martín |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1947 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Garrido De los Santos Rosa María |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1948 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Garrido De los Santos Rosa María |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1949 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Jiménez Fuentevilla Juan José |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1950 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villanueva Martínez Consuelo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1951 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molina  López Carlos Martín |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1952 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molina  López Carlos Martín |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1953 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molina  López Carlos Martín |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1954 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Jiménez Fuentevilla Juan José |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1955 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villanueva Martínez Consuelo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1956 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Jiménez Fuentevilla Juan José |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1957 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Garrido De los Santos Rosa María |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1958 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molina  López Carlos Martín |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1959 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molina  López Carlos Martín |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1960 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Martínez Chavarría David |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1961 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Garrido De los Santos Rosa María |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1962 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Garrido De los Santos Rosa María |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1963 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escalante Castañeda Teresa de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1964 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gómez Dorantes Elena Josefina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1965 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Garrido De los Santos Rosa María |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1966 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escalante Castañeda Teresa de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1967 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escalante Castañeda Teresa de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1968 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escalante Castañeda Teresa de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1969 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gómez Dorantes Elena Josefina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1970 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Escalante Castañeda Teresa de Jesús |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1971 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Alvarado González Amilcar |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1972 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gómez Dorantes Elena Josefina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 1973 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Gómez Dorantes Elena Josefina |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2012 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Mendoza Ruiz Marco Antonio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2013 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Mendoza Ruiz Marco Antonio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2014 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Mendoza Ruiz Marco Antonio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2015 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Mendoza Ruiz Marco Antonio |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2016 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Pérez Yony Didier |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2017 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Molina Jorge Edmundo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2018 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> León Molina Jorge Edmundo |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2019 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Pérez Yony Didier |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2019 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Pérez Yony Didier |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2020 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Pérez Yony Didier |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2021 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Arce Narcía Elvia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2022 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Pérez Yony Didier |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2022 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> López Pérez Yony Didier |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2023 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Jiménez Robles Yojani Berenice |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2024 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villafuerte Lara Juan José |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2025 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Arce Narcía Elvia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2026 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molano Chacón Carmen Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2027 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Arce Narcía Elvia |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2028 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villafuerte Lara Juan José |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2028 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villafuerte Lara Juan José |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2029 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villafuerte Lara Juan José |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2029 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villafuerte Lara Juan José |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2030 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molano Chacón Carmen Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2031 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández  González Leticia del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2032 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molano Chacón Carmen Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2033 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández  González Leticia del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2034 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Molano Chacón Carmen Guadalupe |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2035 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández  González Leticia del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2036 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández  González Leticia del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2037 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Diaz Gutierrez Jose Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2038 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández  González Leticia del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2039 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Hernández  González Leticia del Carmen |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2040 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Saldaña Molina Carlos Freddy |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2040 |
<a>Casilla:</a> C1 |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Saldaña Molina Carlos Freddy |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2041 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Diaz Gutierrez Jose Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2042 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Diaz Gutierrez Jose Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2043 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Villafuerte Lara Juan José |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2044 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Diaz Gutierrez Jose Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2045 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Diaz Gutierrez Jose Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li>
<li class="active">
<i class="list-icon fa fa-dot-circle-o"></i>
<div class="block block-inline">
<div class="caret"></div>
<div class="box-generic">
<div class="timeline-top-info bg-gray">
<div class="row">
<div class="col-md-12">
<i class="fa fa-dot-circle"></i> <a>Sección:</a> 2046 |
<a>Casilla:</a> B |
<a>Municipio:</a> TUXTLA GUTIÉRREZ |
<a>Distrito:</a> TUXTLA GUTIÉRREZ ORIENTE
</div>
</div>
</div>

<div class="media innerAll margin-none border-top border-bottom">
<div class="media-body">
<i class="fa fa-dot-circle"></i> <a>CAE:</a> Diaz Gutierrez Jose Luis |
<a>Celular:</a> -
</div>
</div>
</div>
</div>
</li></ul>
                        </div>
                    </div>
                </div>
            </div>

</div>

<div class="tab-pane" id="tab_4">

<div class="box-generic">
<a href="javascript:;" id="generalMesaDirectiva" class="btn btn-success"><i class="fa fa-bar-chart-o"></i> Gráfica General</a>

<a href="javascript:;" id="porDistritoMesaDirectiva" class="btn btn-success"><i class="fa fa-bar-chart-o"></i> Por Distritos</a>

<a href="javascript:;" id="porMunicipioMesaDirectiva" class="btn btn-success"><i class="fa fa-bar-chart-o"></i> Por Municipios</a>

<div class="separator"></div>

<div id="contenedorPorDistritoMesa" style="display: none;">
<div class="row">
<div class="col-md-8 col-lg-6">
<div class="form-group">
    <label class="control-label">Distritos:</label>
    <select name="distritosMesa" id="distritosMesa" class="form-control">
        <option value="">Seleccione</option>
        <option value="10">BOCHIL</option><option value="24">CACAHOATÁN</option><option value="22">CHAMULA</option><option value="3">CHIAPA DE CORZO</option><option value="14">CINTALAPA</option><option value="6">COMITÁN</option><option value="13">COPAINALÁ</option><option value="16">HUIXTLA</option><option value="20">LAS MARGARITAS</option><option value="17">MOTOZINTLA</option><option value="7">OCOSINGO</option><option value="9">PALENQUE</option><option value="12">PICHUCALCO</option><option value="11">PUEBLO NVO. SOLISTAHUACÁN</option><option value="5">SAN CRISTÓBAL DE LAS CASAS</option><option value="18">TAPACHULA NORTE</option><option value="19">TAPACHULA SUR</option><option value="21">TENEJAPA</option><option value="15">TONALÁ</option><option value="1">TUXTLA GUTIÉRREZ ORIENTE</option><option value="2">TUXTLA GUTIÉRREZ PONIENTE</option><option value="4">VENUSTIANO CARRANZA</option><option value="23">VILLAFLORES</option><option value="8">YAJALÓN</option>																						</select>
</div>
</div>
</div>
</div>

<div id="contenedorPorMunicipioMesa" style="display: none;">
<div class="row">
<div class="col-md-8 col-lg-6">
<div class="form-group">
    <label class="control-label">Municipios:</label>
    <select name="municipiosMesa" id="municipiosMesa" class="form-control">
        <option value="">Seleccione</option>
        <option value="1">ACACOYAGUA</option><option value="2">ACALA</option><option value="3">ACAPETAHUA</option><option value="112">ALDAMA</option><option value="4">ALTAMIRANO</option><option value="5">AMATÁN</option><option value="6">AMATENANGO DE LA FRONTERA</option><option value="7">AMATENANGO DEL VALLE</option><option value="8">ÁNGEL ALBINO CORZO</option><option value="9">ARRIAGA</option><option value="10">BEJUCAL DE OCAMPO</option><option value="119">BELISARIO DOMÍNGUEZ</option><option value="11">BELLA VISTA</option><option value="113">BENEMÉRITO DE LAS AMÉRICAS</option><option value="12">BERRIOZÁBAL</option><option value="13">BOCHIL</option><option value="15">CACAHOATÁN</option><option value="16">CATAZAJÁ</option><option value="22">CHALCHIHUITÁN</option><option value="23">CHAMULA</option><option value="24">CHANAL</option><option value="25">CHAPULTENANGO</option><option value="26">CHENALHÓ</option><option value="27">CHIAPA DE CORZO</option><option value="28">CHIAPILLA</option><option value="29">CHICOASÉN</option><option value="30">CHICOMUSELO</option><option value="31">CHILÓN</option><option value="17">CINTALAPA</option><option value="18">COAPILLA</option><option value="19">COMITÁN DE DOMÍNGUEZ</option><option value="21">COPAINALÁ</option><option value="14">EL BOSQUE</option><option value="122">EL PARRAL</option><option value="70">EL PORVENIR</option><option value="120">EMILIANO ZAPATA</option><option value="32">ESCUINTLA</option><option value="33">FRANCISCO LEÓN</option><option value="34">FRONTERA COMALAPA</option><option value="35">FRONTERA HIDALGO</option><option value="37">HUEHUETÁN</option><option value="38">HUITIUPÁN</option><option value="39">HUIXTÁN</option><option value="40">HUIXTLA</option><option value="42">IXHUATÁN</option><option value="43">IXTACOMITÁN</option><option value="44">IXTAPA</option><option value="45">IXTAPANGAJOYA</option><option value="46">JIQUIPILAS</option><option value="47">JITOTOL</option><option value="48">JUÁREZ</option><option value="20">LA CONCORDIA</option><option value="36">LA GRANDEZA</option><option value="41">LA INDEPENDENCIA</option><option value="50">LA LIBERTAD</option><option value="99">LA TRINITARIA</option><option value="49">LARRÁINZAR</option><option value="52">LAS MARGARITAS</option><option value="74">LAS ROSAS</option><option value="51">MAPASTEPEC</option><option value="114">MARAVILLA TENEJAPA</option><option value="115">MARQUÉS DE COMILLAS</option><option value="53">MAZAPA DE MADERO</option><option value="54">MAZATÁN</option><option value="55">METAPA DE DOMÍNGUEZ</option><option value="121">MEZCALAPA</option><option value="56">MITONTIC</option><option value="116">MONTECRISTO DE GUERRERO</option><option value="57">MOTOZINTLA</option><option value="58">NICOLAS RUIZ</option><option value="59">OCOSINGO</option><option value="60">OCOTEPEC</option><option value="61">OCOZOCOAUTLA DE ESPINOSA</option><option value="62">OSTUACÁN</option><option value="63">OSUMACINTA</option><option value="64">OXCHUC</option><option value="65">PALENQUE</option><option value="66">PANTELHÓ</option><option value="67">PANTEPEC</option><option value="68">PICHUCALCO</option><option value="69">PIJIJIAPAN</option><option value="71">PUEBLO NUEVO SOLISTAHUACÁN</option><option value="72">RAYÓN</option><option value="73">REFORMA</option><option value="75">SABANILLA</option><option value="76">SALTO DE AGUA</option><option value="117">SAN ANDRÉS DURAZNAL</option><option value="77">SAN CRISTÓBAL DE LAS CASAS</option><option value="78">SAN FERNANDO</option><option value="79">SAN JUAN CANCUC</option><option value="80">SAN LUCAS</option><option value="118">SANTIAGO EL PINAR</option><option value="81">SILTEPEC</option><option value="82">SIMOJOVEL</option><option value="83">SITALÁ</option><option value="84">SOCOLTENANGO</option><option value="85">SOLOSUCHIAPA</option><option value="86">SOYALÓ</option><option value="87">SUCHIAPA</option><option value="88">SUCHIATE</option><option value="89">SUNUAPA</option><option value="90">TAPACHULA</option><option value="91">TAPALAPA</option><option value="92">TAPILULA</option><option value="93">TECPATÁN</option><option value="94">TENEJAPA</option><option value="95">TEOPISCA</option><option value="96">TILA</option><option value="97">TONALÁ</option><option value="98">TOTOLAPA</option><option value="100">TUMBALÁ</option><option value="101">TUXTLA CHICO</option><option value="102">TUXTLA GUTIÉRREZ</option><option value="103">TUZANTÁN</option><option value="104">TZIMOL</option><option value="105">UNIÓN JUÁREZ</option><option value="106">VENUSTIANO CARRANZA</option><option value="108">VILLA CORZO</option><option value="107">VILLACOMALTITLÁN</option><option value="109">VILLAFLORES</option><option value="110">YAJALÓN</option><option value="111">ZINACANTÁN</option>																						</select>
</div>
</div>
</div>
</div>

<div id="contenedorGraficasMesa" style="min-width: 70%;  margin: 0 auto" data-highcharts-chart="0"><div class="highcharts-container" id="highcharts-0" style="position: relative; overflow: hidden; width: 1024px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="1024" height="400"><desc>Created with Highcharts 4.1.5</desc><defs><clipPath id="highcharts-1"><rect x="0" y="0" width="933" height="248"></rect></clipPath></defs><rect x="0" y="0" width="1024" height="400" strokeWidth="0" fill="#FFFFFF" class=" highcharts-background"></rect><g class="highcharts-grid" zIndex="1"></g><g class="highcharts-grid" zIndex="1"><path fill="none" d="M 81 319.5 L 1014 319.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 81 278.5 L 1014 278.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 81 236.5 L 1014 236.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 81 195.5 L 1014 195.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 81 154.5 L 1014 154.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 81 112.5 L 1014 112.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 81 70.5 L 1014 70.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path></g><g class="highcharts-axis" zIndex="2"><path fill="none" d="M 391.5 319 L 391.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 702.5 319 L 702.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 1014.5 319 L 1014.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 80.5 319 L 80.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 0 0" stroke="#C0D0E0" stroke-width="1" zIndex="7" visibility="visible"></path></g><g class="highcharts-axis" zIndex="2"><text x="28.140625" zIndex="7" text-anchor="middle" transform="translate(0,0) rotate(270 28.140625 195)" class=" highcharts-yaxis-title" style="color:#707070;fill:#707070;" visibility="visible" y="195"><tspan>Funcionarios de Casilla</tspan></text></g><path fill="none" d="M 547.5 71 L 547.5 319" stroke="rgba(155,200,255,0.2)" stroke-width="311" zIndex="2" visibility="hidden"></path><g class="highcharts-series-group" zIndex="3"><g class="highcharts-series highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(81,71) scale(1 1)" style="" clip-path="url(#highcharts-1)"><rect x="70" y="50" width="23" height="199" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="381" y="50" width="23" height="199" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="692" y="50" width="23" height="199" fill="#7cb5ec" rx="0" ry="0"></rect></g><g class="highcharts-markers" visibility="visible" zIndex="0.1" transform="translate(81,71) scale(1 1)" clip-path="none"></g><g class="highcharts-series highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(81,71) scale(1 1)" style="" clip-path="url(#highcharts-1)"><rect x="107" y="57" width="23" height="192" fill="#434348" rx="0" ry="0"></rect><rect x="418" y="78" width="23" height="171" fill="#434348" rx="0" ry="0"></rect><rect x="729" y="138" width="23" height="111" fill="#434348" rx="0" ry="0"></rect></g><g class="highcharts-markers" visibility="visible" zIndex="0.1" transform="translate(81,71) scale(1 1)" clip-path="none"></g><g class="highcharts-series highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(81,71) scale(1 1)" style="" clip-path="url(#highcharts-1)"><rect x="144" y="243" width="23" height="6" fill="#90ed7d" rx="0" ry="0"></rect><rect x="455" y="227" width="23" height="22" fill="#90ed7d" rx="0" ry="0"></rect><rect x="766" y="185" width="23" height="64" fill="#90ed7d" rx="0" ry="0"></rect></g><g class="highcharts-markers" visibility="visible" zIndex="0.1" transform="translate(81,71) scale(1 1)" clip-path="none"></g><g class="highcharts-series highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(81,71) scale(1 1)" style="" clip-path="url(#highcharts-1)"><rect x="182" y="248" width="23" height="1" fill="#f7a35c" rx="0" ry="0"></rect><rect x="493" y="244" width="23" height="5" fill="#f7a35c" rx="0" ry="0"></rect><rect x="804" y="230" width="23" height="19" fill="#f7a35c" rx="0" ry="0"></rect></g><g class="highcharts-markers" visibility="visible" zIndex="0.1" transform="translate(81,71) scale(1 1)" clip-path="none"></g><g class="highcharts-series highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(81,71) scale(1 1)" style="" clip-path="url(#highcharts-1)"><rect x="219" y="249" width="23" height="0" fill="#8085e9" rx="0" ry="0"></rect><rect x="530" y="248" width="23" height="1" fill="#8085e9" rx="0" ry="0"></rect><rect x="841" y="245" width="23" height="4" fill="#8085e9" rx="0" ry="0"></rect></g><g class="highcharts-markers" visibility="visible" zIndex="0.1" transform="translate(81,71) scale(1 1)" clip-path="none"></g></g><text x="512" text-anchor="middle" class="highcharts-title" zIndex="4" style="color:#333333;font-size:18px;fill:#333333;width:960px;" y="24"><tspan>Asistencia Mesa Directiva de Casilla</tspan></text><text x="512" text-anchor="middle" class="highcharts-subtitle" zIndex="4" style="color:#555555;fill:#555555;width:960px;" y="52"><tspan>Corte 19/Apr/2018 Hora:18:12:23</tspan></text><g class="highcharts-legend" zIndex="7" transform="translate(216,356)"><g zIndex="1"><g><g class="highcharts-legend-item" zIndex="1" transform="translate(8,3)"><text x="21" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2" y="15">Total</text><rect x="0" y="4" width="16" height="12" zIndex="3" fill="#7cb5ec"></rect></g><g class="highcharts-legend-item" zIndex="1" transform="translate(81.03125,3)"><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2">Propietario</text><rect x="0" y="4" width="16" height="12" zIndex="3" fill="#434348"></rect></g><g class="highcharts-legend-item" zIndex="1" transform="translate(189.546875,3)"><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2"><tspan>Suplente General</tspan></text><rect x="0" y="4" width="16" height="12" zIndex="3" fill="#90ed7d"></rect></g><g class="highcharts-legend-item" zIndex="1" transform="translate(332.96875,3)"><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2"><tspan>Tomado de Fila</tspan></text><rect x="0" y="4" width="16" height="12" zIndex="3" fill="#f7a35c"></rect></g><g class="highcharts-legend-item" zIndex="1" transform="translate(468.515625,3)"><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2"><tspan>Sin Funcionario</tspan></text><rect x="0" y="4" width="16" height="12" zIndex="3" fill="#8085e9"></rect></g></g></g></g><g class="highcharts-axis-labels highcharts-xaxis-labels" zIndex="7"><text x="236.5" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:325px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">Presidente</text><text x="547.5" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:325px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">Secretario</text><text x="858.5" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:325px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1"><tspan>1er. Escrutador</tspan></text></g><g class="highcharts-axis-labels highcharts-yaxis-labels" zIndex="7"><text x="66" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="322" opacity="1">0</text><text x="66" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="280" opacity="1">250</text><text x="66" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="239" opacity="1">500</text><text x="66" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="198" opacity="1">750</text><text x="66" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="156" opacity="1">1000</text><text x="66" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="115" opacity="1">1250</text><text x="66" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="74" opacity="1">1500</text></g><g class="highcharts-tooltip" zIndex="8" style="cursor:default;padding:0;white-space:nowrap;" transform="translate(316,-9999)" opacity="0"><path fill="none" d="M 3.5 0.5 L 148.5 0.5 C 151.5 0.5 151.5 0.5 151.5 3.5 L 151.5 121.5 C 151.5 124.5 151.5 124.5 148.5 124.5 L 3.5 124.5 C 0.5 124.5 0.5 124.5 0.5 121.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)" width="151" height="124"></path><path fill="none" d="M 3.5 0.5 L 148.5 0.5 C 151.5 0.5 151.5 0.5 151.5 3.5 L 151.5 121.5 C 151.5 124.5 151.5 124.5 148.5 124.5 L 3.5 124.5 C 0.5 124.5 0.5 124.5 0.5 121.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)" width="151" height="124"></path><path fill="none" d="M 3.5 0.5 L 148.5 0.5 C 151.5 0.5 151.5 0.5 151.5 3.5 L 151.5 121.5 C 151.5 124.5 151.5 124.5 148.5 124.5 L 3.5 124.5 C 0.5 124.5 0.5 124.5 0.5 121.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)" width="151" height="124"></path><path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 148.5 0.5 C 151.5 0.5 151.5 0.5 151.5 3.5 L 151.5 121.5 C 151.5 124.5 151.5 124.5 148.5 124.5 L 3.5 124.5 C 0.5 124.5 0.5 124.5 0.5 121.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#7cb5ec" stroke-width="1"></path></g><text x="1014" text-anchor="end" zIndex="8" style="cursor:pointer;color:#909090;font-size:9px;fill:#909090;" y="395">Highcharts.com</text></svg><div class="highcharts-tooltip" style="position: absolute; left: 316px; top: -9999px; visibility: visible;"><span style="position: absolute; font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif; font-size: 12px; white-space: nowrap; color: rgb(51, 51, 51); margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;" zindex="1"><span style="font-size:10px">Secretario</span><table><tbody><tr><td style="color:#7cb5ec;padding:0">Total: </td><td style="padding:0"><b>1202 </b></td></tr><tr><td style="color:#434348;padding:0">Propietario: </td><td style="padding:0"><b>1036 </b></td></tr><tr><td style="color:#90ed7d;padding:0">Suplente General: </td><td style="padding:0"><b>132 </b></td></tr><tr><td style="color:#f7a35c;padding:0">Tomado de Fila: </td><td style="padding:0"><b>30 </b></td></tr><tr><td style="color:#8085e9;padding:0">Sin Funcionario: </td><td style="padding:0"><b>4 </b></td></tr></tbody></table></span></div></div></div>
</div>

</div>
<div class="tab-pane" id="tab_5">

<div class="box-generic">
<a href="javascript:;" id="generalPartidos" class="btn btn-success"><i class="fa fa-bar-chart-o"></i> Gráfica General</a>

<a href="javascript:;" id="porDistritoPartidos" class="btn btn-success"><i class="fa fa-bar-chart-o"></i> Por Distritos</a>

<a href="javascript:;" id="porMunicipioPartidos" class="btn btn-success"><i class="fa fa-bar-chart-o"></i> Por Municipios</a>

<div class="separator"></div>

<div id="contenedorPorDistritoPartidos" style="display: none;">
<div class="row">
<div class="col-md-8 col-lg-6">
<div class="form-group">
    <label class="control-label">Distritos:</label>
    <select name="distritosPartidos" id="distritosPartidos" class="form-control">
        <option value="">Seleccione</option>
        <option value="10">BOCHIL</option><option value="24">CACAHOATÁN</option><option value="22">CHAMULA</option><option value="3">CHIAPA DE CORZO</option><option value="14">CINTALAPA</option><option value="6">COMITÁN</option><option value="13">COPAINALÁ</option><option value="16">HUIXTLA</option><option value="20">LAS MARGARITAS</option><option value="17">MOTOZINTLA</option><option value="7">OCOSINGO</option><option value="9">PALENQUE</option><option value="12">PICHUCALCO</option><option value="11">PUEBLO NVO. SOLISTAHUACÁN</option><option value="5">SAN CRISTÓBAL DE LAS CASAS</option><option value="18">TAPACHULA NORTE</option><option value="19">TAPACHULA SUR</option><option value="21">TENEJAPA</option><option value="15">TONALÁ</option><option value="1">TUXTLA GUTIÉRREZ ORIENTE</option><option value="2">TUXTLA GUTIÉRREZ PONIENTE</option><option value="4">VENUSTIANO CARRANZA</option><option value="23">VILLAFLORES</option><option value="8">YAJALÓN</option>																						</select>
</div>
</div>
</div>
</div>

<div id="contenedorPorMunicipioPartidos" style="display: none;">
<div class="row">
<div class="col-md-8 col-lg-6">
<div class="form-group">
    <label class="control-label">Municipios:</label>
    <select name="municipiosPartidos" id="municipiosPartidos" class="form-control">
        <option value="">Seleccione</option>
        <option value="1">ACACOYAGUA</option><option value="2">ACALA</option><option value="3">ACAPETAHUA</option><option value="112">ALDAMA</option><option value="4">ALTAMIRANO</option><option value="5">AMATÁN</option><option value="6">AMATENANGO DE LA FRONTERA</option><option value="7">AMATENANGO DEL VALLE</option><option value="8">ÁNGEL ALBINO CORZO</option><option value="9">ARRIAGA</option><option value="10">BEJUCAL DE OCAMPO</option><option value="119">BELISARIO DOMÍNGUEZ</option><option value="11">BELLA VISTA</option><option value="113">BENEMÉRITO DE LAS AMÉRICAS</option><option value="12">BERRIOZÁBAL</option><option value="13">BOCHIL</option><option value="15">CACAHOATÁN</option><option value="16">CATAZAJÁ</option><option value="22">CHALCHIHUITÁN</option><option value="23">CHAMULA</option><option value="24">CHANAL</option><option value="25">CHAPULTENANGO</option><option value="26">CHENALHÓ</option><option value="27">CHIAPA DE CORZO</option><option value="28">CHIAPILLA</option><option value="29">CHICOASÉN</option><option value="30">CHICOMUSELO</option><option value="31">CHILÓN</option><option value="17">CINTALAPA</option><option value="18">COAPILLA</option><option value="19">COMITÁN DE DOMÍNGUEZ</option><option value="21">COPAINALÁ</option><option value="14">EL BOSQUE</option><option value="122">EL PARRAL</option><option value="70">EL PORVENIR</option><option value="120">EMILIANO ZAPATA</option><option value="32">ESCUINTLA</option><option value="33">FRANCISCO LEÓN</option><option value="34">FRONTERA COMALAPA</option><option value="35">FRONTERA HIDALGO</option><option value="37">HUEHUETÁN</option><option value="38">HUITIUPÁN</option><option value="39">HUIXTÁN</option><option value="40">HUIXTLA</option><option value="42">IXHUATÁN</option><option value="43">IXTACOMITÁN</option><option value="44">IXTAPA</option><option value="45">IXTAPANGAJOYA</option><option value="46">JIQUIPILAS</option><option value="47">JITOTOL</option><option value="48">JUÁREZ</option><option value="20">LA CONCORDIA</option><option value="36">LA GRANDEZA</option><option value="41">LA INDEPENDENCIA</option><option value="50">LA LIBERTAD</option><option value="99">LA TRINITARIA</option><option value="49">LARRÁINZAR</option><option value="52">LAS MARGARITAS</option><option value="74">LAS ROSAS</option><option value="51">MAPASTEPEC</option><option value="114">MARAVILLA TENEJAPA</option><option value="115">MARQUÉS DE COMILLAS</option><option value="53">MAZAPA DE MADERO</option><option value="54">MAZATÁN</option><option value="55">METAPA DE DOMÍNGUEZ</option><option value="121">MEZCALAPA</option><option value="56">MITONTIC</option><option value="116">MONTECRISTO DE GUERRERO</option><option value="57">MOTOZINTLA</option><option value="58">NICOLAS RUIZ</option><option value="59">OCOSINGO</option><option value="60">OCOTEPEC</option><option value="61">OCOZOCOAUTLA DE ESPINOSA</option><option value="62">OSTUACÁN</option><option value="63">OSUMACINTA</option><option value="64">OXCHUC</option><option value="65">PALENQUE</option><option value="66">PANTELHÓ</option><option value="67">PANTEPEC</option><option value="68">PICHUCALCO</option><option value="69">PIJIJIAPAN</option><option value="71">PUEBLO NUEVO SOLISTAHUACÁN</option><option value="72">RAYÓN</option><option value="73">REFORMA</option><option value="75">SABANILLA</option><option value="76">SALTO DE AGUA</option><option value="117">SAN ANDRÉS DURAZNAL</option><option value="77">SAN CRISTÓBAL DE LAS CASAS</option><option value="78">SAN FERNANDO</option><option value="79">SAN JUAN CANCUC</option><option value="80">SAN LUCAS</option><option value="118">SANTIAGO EL PINAR</option><option value="81">SILTEPEC</option><option value="82">SIMOJOVEL</option><option value="83">SITALÁ</option><option value="84">SOCOLTENANGO</option><option value="85">SOLOSUCHIAPA</option><option value="86">SOYALÓ</option><option value="87">SUCHIAPA</option><option value="88">SUCHIATE</option><option value="89">SUNUAPA</option><option value="90">TAPACHULA</option><option value="91">TAPALAPA</option><option value="92">TAPILULA</option><option value="93">TECPATÁN</option><option value="94">TENEJAPA</option><option value="95">TEOPISCA</option><option value="96">TILA</option><option value="97">TONALÁ</option><option value="98">TOTOLAPA</option><option value="100">TUMBALÁ</option><option value="101">TUXTLA CHICO</option><option value="102">TUXTLA GUTIÉRREZ</option><option value="103">TUZANTÁN</option><option value="104">TZIMOL</option><option value="105">UNIÓN JUÁREZ</option><option value="106">VENUSTIANO CARRANZA</option><option value="108">VILLA CORZO</option><option value="107">VILLACOMALTITLÁN</option><option value="109">VILLAFLORES</option><option value="110">YAJALÓN</option><option value="111">ZINACANTÁN</option>																						</select>
</div>
</div>
</div>
</div>

<div id="contenedorGraficasPartidos" style="min-width: 70%;  margin: 0 auto" data-highcharts-chart="0"><div class="highcharts-container" id="highcharts-0" style="position: relative; overflow: hidden; width: 1024px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="1024" height="400"><desc>Created with Highcharts 4.1.5</desc><defs><clipPath id="highcharts-1"><rect x="0" y="0" width="930" height="248"></rect></clipPath></defs><rect x="0" y="0" width="1024" height="400" strokeWidth="0" fill="#FFFFFF" class=" highcharts-background"></rect><g class="highcharts-grid" zIndex="1"></g><g class="highcharts-grid" zIndex="1"><path fill="none" d="M 84 319.5 L 1014 319.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 84 269.5 L 1014 269.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 84 220.5 L 1014 220.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 84 170.5 L 1014 170.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 84 121.5 L 1014 121.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path><path fill="none" d="M 84 70.5 L 1014 70.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path></g><g class="highcharts-axis" zIndex="2"><path fill="none" d="M 149.5 319 L 149.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 216.5 319 L 216.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 282.5 319 L 282.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 349.5 319 L 349.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 415.5 319 L 415.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 482.5 319 L 482.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 548.5 319 L 548.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 614.5 319 L 614.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 681.5 319 L 681.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 747.5 319 L 747.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 814.5 319 L 814.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 880.5 319 L 880.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 947.5 319 L 947.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 1014.5 319 L 1014.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 83.5 319 L 83.5 329" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 0 0" stroke="#C0D0E0" stroke-width="1" zIndex="7" visibility="visible"></path></g><g class="highcharts-axis" zIndex="2"><text x="27.65625" zIndex="7" text-anchor="middle" transform="translate(0,0) rotate(270 27.65625 195)" class=" highcharts-yaxis-title" style="color:#707070;fill:#707070;" visibility="visible" y="195">Representantes</text></g><path fill="none" d="M 515.5 71 L 515.5 319" stroke="rgba(155,200,255,0.2)" stroke-width="66.42857142857143" zIndex="2" visibility="hidden"></path><g class="highcharts-series-group" zIndex="3"><g class="highcharts-series highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(84,71) scale(1 1)" style="" clip-path="url(#highcharts-1)"><rect x="21" y="59" width="24" height="190" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="88" y="231" width="24" height="18" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="154" y="218" width="24" height="31" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="221" y="235" width="24" height="14" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="287" y="217" width="24" height="32" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="353" y="237" width="24" height="12" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="420" y="242" width="24" height="7" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="486" y="237" width="24" height="12" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="553" y="232" width="24" height="17" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="619" y="247" width="24" height="2" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="686" y="247" width="24" height="2" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="752" y="229" width="24" height="20" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="818" y="230" width="24" height="19" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="885" y="247" width="24" height="2" fill="#7cb5ec" rx="0" ry="0"></rect></g><g class="highcharts-markers" visibility="visible" zIndex="0.1" transform="translate(84,71) scale(1 1)" clip-path="none"></g></g><text x="512" text-anchor="middle" class="highcharts-title" zIndex="4" style="color:#333333;font-size:18px;fill:#333333;width:960px;" y="24"><tspan>Asistencia Partidos Políticos</tspan></text><text x="512" text-anchor="middle" class="highcharts-subtitle" zIndex="4" style="color:#555555;fill:#555555;width:960px;" y="52"><tspan>Corte 19/Apr/2018 Hora:18:19:43</tspan></text><g class="highcharts-legend" zIndex="7" transform="translate(447,356)"><g zIndex="1"><g><g class="highcharts-legend-item" zIndex="1" transform="translate(8,3)"><text x="21" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2" y="15">Representantes</text><rect x="0" y="4" width="16" height="12" zIndex="3" fill="#7cb5ec"></rect></g></g></g></g><g class="highcharts-axis-labels highcharts-xaxis-labels" zIndex="7"><text x="117.21428571428572" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">TOTAL</text><text x="183.64285714285714" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">PAN</text><text x="250.0714285714286" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">PRI</text><text x="316.5" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">PRD</text><text x="382.92857142857144" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">PVEM</text><text x="449.3571428571429" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">PT</text><text x="515.7857142857143" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">MC</text><text x="582.2142857142858" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">NA</text><text x="648.6428571428571" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">MO</text><text x="715.0714285714286" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">PH</text><text x="781.5" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">ES</text><text x="847.9285714285714" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">PCU</text><text x="914.3571428571429" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">MVC</text><text x="980.7857142857143" style="color:#606060;cursor:default;font-size:11px;fill:#606060;" text-anchor="middle" transform="translate(0,0)" y="338" opacity="1">IND</text></g><g class="highcharts-axis-labels highcharts-yaxis-labels" zIndex="7"><text x="69" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="322" opacity="1">0k</text><text x="69" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="272" opacity="1">2.5k</text><text x="69" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="222" opacity="1">5k</text><text x="69" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="173" opacity="1">7.5k</text><text x="69" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="123" opacity="1">10k</text><text x="69" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:328px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="74" opacity="1">12.5k</text></g><g class="highcharts-tooltip" zIndex="8" style="cursor:default;padding:0;white-space:nowrap;" transform="translate(449,-9999)" opacity="0"><path fill="none" d="M 3.5 0.5 L 131.5 0.5 C 134.5 0.5 134.5 0.5 134.5 3.5 L 134.5 49.5 C 134.5 52.5 134.5 52.5 131.5 52.5 L 73.5 52.5 67.5 58.5 61.5 52.5 3.5 52.5 C 0.5 52.5 0.5 52.5 0.5 49.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)" width="134" height="52"></path><path fill="none" d="M 3.5 0.5 L 131.5 0.5 C 134.5 0.5 134.5 0.5 134.5 3.5 L 134.5 49.5 C 134.5 52.5 134.5 52.5 131.5 52.5 L 73.5 52.5 67.5 58.5 61.5 52.5 3.5 52.5 C 0.5 52.5 0.5 52.5 0.5 49.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)" width="134" height="52"></path><path fill="none" d="M 3.5 0.5 L 131.5 0.5 C 134.5 0.5 134.5 0.5 134.5 3.5 L 134.5 49.5 C 134.5 52.5 134.5 52.5 131.5 52.5 L 73.5 52.5 67.5 58.5 61.5 52.5 3.5 52.5 C 0.5 52.5 0.5 52.5 0.5 49.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)" width="134" height="52"></path><path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 131.5 0.5 C 134.5 0.5 134.5 0.5 134.5 3.5 L 134.5 49.5 C 134.5 52.5 134.5 52.5 131.5 52.5 L 73.5 52.5 67.5 58.5 61.5 52.5 3.5 52.5 C 0.5 52.5 0.5 52.5 0.5 49.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#7cb5ec" stroke-width="1"></path></g><text x="1014" text-anchor="end" zIndex="8" style="cursor:pointer;color:#909090;font-size:9px;fill:#909090;" y="395">Highcharts.com</text></svg><div class="highcharts-tooltip" style="position: absolute; left: 449px; top: -9999px; visibility: visible;"><span style="position: absolute; font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif; font-size: 12px; white-space: nowrap; color: rgb(51, 51, 51); margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;" zindex="1"><span style="font-size:10px">MC</span><table><tbody><tr><td style="color:#7cb5ec;padding:0">Representantes: </td><td style="padding:0"><b>369 </b></td></tr></tbody></table></span></div></div></div>
</div>
    
</div>

</div>
</div>
  
@endsection @section('content_js')
{{-- <script src="{{ asset('js/planillas.js') }}"></script> --}}
 @stop