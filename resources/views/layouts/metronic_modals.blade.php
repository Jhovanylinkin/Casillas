<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ app()->getLocale() }}">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>{{ config('app.name', 'Código27') }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{--  <link href="{{ asset('css/googlefotns.css?family=Open+Sans:400,300,600,700&subset=all') }}" rel="stylesheet" type="text/css">  --}}
        {{--  <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" type="text/css">  --}}
        <link href="{{ asset('css/simple-line-icons.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/nacional.css') }}" rel="stylesheet">
        @yield('content_popup_css')
        </head>
    <!-- END HEAD -->

    <body>
        <div class="portlet-body">
        
        @yield('content_popup_modals')
        </div>
        @yield('content_popup_js')
</body>
</html>