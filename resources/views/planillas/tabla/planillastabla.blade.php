<table id="tablaplanillas" class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Tipo Eleccion</th>
      <th scope="col">Cargo</th>
      <th scope="col">Estado</th>
      <th scope="col">Municipio</th>
      <th scope="col">Nombre</th>
      <th scope="col">Partido</th>
    </tr>
  </thead>
  <tbody>
    @foreach ( $planillas as $p )
    <tr>
      <th class="uppercase">{{$p->TIPOELECCION}}</th>
      <td class="uppercase" id='cargopopup'><a >{{$p->CARGO}}</a></td>
      <td class="uppercase">{{$p->Entidad}}</td>
      <td class="uppercase">{{$p->Municipio}}</td>
      <td class="uppercase">{{$p->NOMBRE}}</td>
      <td class="uppercase">{{$p->PARTIDO}}</td>
      <td class="uppercase" style="display:none;" >{{$p->ESTADO}}</td>
      <td class="uppercase" style="display:none;" >{{$p->MUNICIPIO_DISTRITO}}</td>     
    </tr>
    @endforeach
  </tbody>
</table>
{{$planillas->links('pagination::bootstrap-4')}}

<script> 
$(document).ready(function () {
    $('#tablaplanillas').on('click','#cargopopup', function(e) {
        let currow = $(this).closest('tr');
        let TIPOELECCION = currow.find('td:eq()').text();
        let CARGO = currow.find('td:eq(0)').text();
        let ESTADO = currow.find('td:eq(5)').text();
        let Municipio = currow.find('td:eq(2)').text();
        let MUNICIPIO_DISTRITO = currow.find('td:eq(6)').text();
        let PARTIDO = currow.find('td:eq(4)').text();
        f_popup_info('','/planillapopup',`<strong>Municipio:</strong>&nbsp;${Municipio} <br><strong>Partido político:</strong>&nbsp;${PARTIDO}`,TIPOELECCION,CARGO,ESTADO,MUNICIPIO_DISTRITO,PARTIDO);
      });
});
</script>

        <div class="modal fade bs-modal-lg in" id="ModalInfo" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title" id="ModalTitleInfo">Formulario</h4>
                    </div>
                    <div id="ModalBody" class="modal-body"></div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
@section('content_js')
<script src="{{ asset('js/planillas.js') }}"></script>
@stop