<table id="tablaplanillas" class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Tipo Eleccion</th>
      <th scope="col">Cargo</th>
      <th scope="col">Estado</th>
      <th scope="col">Nombre</th>
      <th scope="col">Partido</th>
    </tr>
  </thead>
  <tbody>
    @foreach ( $planillas as $p )
    <tr>
      <th class="uppercase">{{$p->TIPOELECCION}}</th>
      <td class="uppercase" id='cargopopup'>{{$p->CARGO}}</td>
      <td class="uppercase">{{$p->Entidad}}</td>
      <td class="uppercase">{{$p->NOMBRE}}</td>
      <td class="uppercase">{{$p->PARTIDO}}</td>  
    </tr>
    @endforeach
  </tbody>
</table>
{{$planillas->links('pagination::bootstrap-4')}}
@section('content_js')
@stop