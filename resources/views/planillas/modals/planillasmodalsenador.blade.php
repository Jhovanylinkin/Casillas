@extends('layouts.metronic_modals') @section('content_popup_modals') 

<table id="" class="table table-bordered">
    <thead>
        <tr style="background-color:black;">
            <th colspan="12">
                <center style="color:white;">SENADORES</center>
            </th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th colspan="6">
                <center>FORMULA TITULAR</center>
            </th>
            <th colspan="6">
                <center>FORMULA SUPLENTE</center>
            </th>
        </tr>
        <thead>
            <tbody>
                <tr>
                    <td colspan="6">1. {{$Planilla->keyBy('CARGO')->get('SENADOR FORMULA 1 ')->NOMBRE}}</td>
                    <td colspan="6">1. {{$Planilla->keyBy('CARGO')->get('SENADOR SUPLENTE 1')->NOMBRE}}</td>
                </tr>
                <tr>
                    <td colspan="6">2. {{$Planilla->keyBy('CARGO')->get('SENADOR FORMULA 2')->NOMBRE}}</td>
                    <td colspan="6">2.
                        @if($Planilla->keyBy('CARGO')->get('SENADOR SUPLENTE 2'))
                         {{$Planilla->keyBy('CARGO')->get('SENADOR SUPLENTE 2')->NOMBRE}}</td>
                        @endif
                </tr>
            </tbody>
</table>
@endsection 
@section('content_popup_js') 

@endsection