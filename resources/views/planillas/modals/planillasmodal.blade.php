@extends('layouts.metronic_modals')
@section('content_popup_modals')
<table id="" class="table table-bordered">
<thead>
<tr style="background-color:black;">
    <th colspan="12"><center style="color:white;">Presidente</center></th>
</tr>
</thead>
<tbody>
<tr>
    <td colspan="12"><center>
        {{$Planilla->keyBy('CARGO')->get('PRESIDENTE MUNICIPAL')->NOMBRE}}
    </center></td>
</tr>
</tbody>
<thead>
<tr>
    <th colspan="12"><center>Síndicos</center></th>
</tr>
</thead>
<thead>
<tr>
    <th colspan="6"><center>Propietario</center></th>
    <th colspan="6"><center>Suplente</center></th>   
    </tr>
<thead>
<tbody>
<tr>
    <td colspan="6">1 {{$Planilla->keyBy('CARGO')->get('SINDICO MUNICIPAL')->NOMBRE}}</td>
    @if($Planilla->keyBy('CARGO')->get('SINDICO SUPLENTE'))
    <td colspan="6">1 {{$Planilla->keyBy('CARGO')->get('SINDICO SUPLENTE')->NOMBRE}}</td>
    @elseif($Planilla->keyBy('CARGO')->get('SINDICO MUNICIPAL SUPLENTE'))
    <td colspan="6">1 {{$Planilla->keyBy('CARGO')->get('SINDICO MUNICIPAL SUPLENTE')->NOMBRE}}</td>
    @endif
</tr>
</tbody>
<thead>
<tr>
    <th colspan="12"><center>Regidores</center></th>
</tr>
</thead>
<thead>
<tr>
    <th colspan="6"><center>Propietario</center></th>
    <th colspan="6"><center>Suplente</center></th>   
    </tr>
<thead>
<tbody>
@if(!is_null($Planilla->keyBy('CARGO')->get('REGIDOR1')))
<tr>
    <td colspan="6">1. {{$Planilla->keyBy('CARGO')->get('REGIDOR1')->NOMBRE}}</td>
    <td colspan="6">1. {{$Planilla->keyBy('CARGO')->get('REGIDOR1 SUPLENTE')->NOMBRE}}</td>   
</tr>
@endif
@if(!is_null($Planilla->keyBy('CARGO')->get('REGIDOR2')))
<tr>
    <td colspan="6">2. {{$Planilla->keyBy('CARGO')->get('REGIDOR2')->NOMBRE}}</td>
    <td colspan="6">2. {{$Planilla->keyBy('CARGO')->get('REGIDOR2 SUPLENTE')->NOMBRE}}</td>   
</tr>
@endif
@if(!is_null($Planilla->keyBy('CARGO')->get('REGIDOR3')))
<tr>
    <td colspan="6">3. {{$Planilla->keyBy('CARGO')->get('REGIDOR3')->NOMBRE}}</td>
    <td colspan="6">3. {{$Planilla->keyBy('CARGO')->get('REGIDOR3 SUPLENTE')->NOMBRE}}</td>   
</tr>
@endif
@if(!is_null($Planilla->keyBy('CARGO')->get('REGIDOR4')))
<tr>
    <td colspan="6">4. {{$Planilla->keyBy('CARGO')->get('REGIDOR4')->NOMBRE}}</td>
    <td colspan="6">4. {{$Planilla->keyBy('CARGO')->get('REGIDOR4 SUPLENTE')->NOMBRE}}</td>   
</tr>
@endif
@if(!is_null($Planilla->keyBy('CARGO')->get('REGIDOR5')))
<tr>
    <td colspan="6">5. {{$Planilla->keyBy('CARGO')->get('REGIDOR5')->NOMBRE}}</td>
    <td colspan="6">5. {{$Planilla->keyBy('CARGO')->get('REGIDOR5 SUPLENTE')->NOMBRE}}</td>   
</tr>
@endif
@if(!is_null($Planilla->keyBy('CARGO')->get('REGIDOR6')))
<tr>
    <td colspan="6">6. {{$Planilla->keyBy('CARGO')->get('REGIDOR6')->NOMBRE}}</td>
    <td colspan="6">6. {{$Planilla->keyBy('CARGO')->get('REGIDOR6 SUPLENTE')->NOMBRE}}</td>   
</tr>
@endif
@if(!is_null($Planilla->keyBy('CARGO')->get('REGIDOR7')))
<tr>
    <td colspan="6">7. {{$Planilla->keyBy('CARGO')->get('REGIDOR7')->NOMBRE}}</td>
    <td colspan="6">7. {{$Planilla->keyBy('CARGO')->get('REGIDOR7 SUPLENTE')->NOMBRE}}</td>   
</tr>
@endif
@if(!is_null($Planilla->keyBy('CARGO')->get('REGIDOR8')))
<tr>
    <td colspan="6">8. {{$Planilla->keyBy('CARGO')->get('REGIDOR8')->NOMBRE}}</td>
    <td colspan="6">8. {{$Planilla->keyBy('CARGO')->get('REGIDOR8 SUPLENTE')->NOMBRE}}</td>   
</tr>
@endif
@if($Planilla->keyBy('CARGO')->get('REGIDOR9') == null)
@else
    <tr>
        <td colspan="6">9. {{$Planilla->keyBy('CARGO')->get('REGIDOR9')->NOMBRE}}</td>
        <td colspan="6">9. {{$Planilla->keyBy('CARGO')->get('REGIDOR9 SUPLENTE')->NOMBRE}}</td>
    </tr>
@endif
@if($Planilla->keyBy('CARGO')->get('REGIDOR10') == null)
@else
    <tr>
        <td colspan="6">10. {{$Planilla->keyBy('CARGO')->get('REGIDOR10')->NOMBRE}}</td>
        <td colspan="6">10. {{$Planilla->keyBy('CARGO')->get('REGIDOR10 SUPLENTE')->NOMBRE}}</td>
    </tr>
@endif
@if($Planilla->keyBy('CARGO')->get('REGIDOR11') == null)
@else
    <tr>
        <td colspan="6">11. {{$Planilla->keyBy('CARGO')->get('REGIDOR11')->NOMBRE}}</td>
        <td colspan="6">11. {{$Planilla->keyBy('CARGO')->get('REGIDOR11 SUPLENTE')->NOMBRE}}</td>
    </tr>
@endif
@if($Planilla->keyBy('CARGO')->get('REGIDOR12') == null)
@else
    <tr>
        <td colspan="6">12. {{$Planilla->keyBy('CARGO')->get('REGIDOR12')->NOMBRE}}</td>
        <td colspan="6">12. {{$Planilla->keyBy('CARGO')->get('REGIDOR12 SUPLENTE')->NOMBRE}}</td>
    </tr>
@endif
</tbody>
</table>

@endsection
@section('content_popup_js')
@endsection
