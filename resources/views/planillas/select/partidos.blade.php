@unless($Partidos->count()>0)
	<option value="">Sin partidos</option>
@else
	@if($Partidos->count()>=1)
	<option value="" selected>Partido</option>
	@endif
 @foreach($Partidos as $M)
    <option value="{{ $M->PARTIDO}}">{{ $M->PARTIDO}}</option>
@endforeach
@endunless