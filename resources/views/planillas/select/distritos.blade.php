@unless($DF->count()>0)
	<option value="">Sin Municipios</option>
@else
	@if($DF->count()>=1)
	<option value="" selected>Distritos</option>
	@endif
 @foreach($DF as $M)
    <option value="{{ $M->Distrito}}">{{ $M->Distrito}}- {{ $M->Cabecera}}</option>
@endforeach
@endunless