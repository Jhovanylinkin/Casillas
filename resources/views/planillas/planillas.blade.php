@extends('layouts.metronic') @section('Activo')
<a href="/planillas">
    <span class="selected">Planillas</span>
</a>
@endsection @section('content_css')
<link href="{{ asset('css/bootstrap-fileinput.css') }}" rel="stylesheet"> @endsection @section('content')
<h1 class="page-title"> Planillas
    <small></small>
</h1>
<div id="planillascontainer" class="container">
    <form id="filtroplanillas" action="submit" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Tipo de Elección</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-cube"></i>
                        <select class="form-control" name="TIPOELECCION" id="tiposdeelecciones" required>
                            <option value="" disabled selected>Tipo Elección</option>
                            @forelse($TipoE as $TiEl)
                            <option value="{{$TiEl->TIPOELECCION}}">{{$TiEl->TIPOELECCION}}</option>
                            @empty
                            <p>No hay contenido que mostrar</p>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div id="filtro2" class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Estado</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-cube"></i>
                        <select class="form-control" name="ESTADO" id="ESTADO">
                            <option value="" selected>Estado</option>
                            @forelse($Estados as $E)
                            <option value="{{$E->id}}">{{$E->Entidad}}</option>
                            @empty
                            <p>No hay contenido que mostrar</p>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div id="filtro3" class="col-md-3">
                <div class="form-group">
                    <label id="fxmunicipio" class="control-label">Municipios</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-cube"></i>
                        <select class="form-control" name="MUNICIPIO_DISTRITO" id="municipios">
                            <option value="" selected>Municipios</option>
                            <option value=""></option>
                            <option style="color:red;" >Seleccione primero el estado</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="filtro4" class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Partido político</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-cube"></i>
                        <select class="form-control" name="PARTIDO" id="partidos">
                            <option value="" selected>Partido</option>
                            @forelse($Partidos as $p)
                            <option value="{{$p->PARTIDO}}">{{$p->PARTIDO}}</option>
                            @empty @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                  <label class="control-label">Nombre del candidato</label>
                  <div class="input-icon left tooltips" data-placement="top">
                      <i class="fa fa-user"></i>
                      <input class="form-control" type="text" name="NombreC" placeholder="Búsqueda por nombre">
                  </div>
              </div>
            </div>
            <div class="col-md-7"></div>
            <div class="col-md-2">
              <div class="form-group">
                <br>
                <button style="float:right;" type="submit" class="btn btn-warning mt-ladda-btn ladda-button btn-circle" id="busqueda-filtro"
                    data-style="zoom-in" style="display: none;">
                    <span class="ladda-label">
                        <i class="icon-magnifier"></i> Realizar búsqueda</span>
                    <span class="ladda-spinner"></span>
                </button>&nbsp
              </div>
            </div>
        </div>
    </form>
    <hr>
    <form action="submit" id="formEXCEL" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="form-group">
                <div class="col-md-3">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                <i class="fa fa-file-excel-o fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename"> Actualizar la tabla</span>
                            </div>
                            <span class="input-group-addon btn default btn-file">
                                <span class="fileinput-new">Seleccionar Archivo </span>
                                <span class="fileinput-exists">Cambiar</span>
                                <input type="hidden" value="" name="">
                                <input type="file" name="excelPLANILLAS"> </span>
                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">Remover</a>
                            <div style="margin-left: 2px;" class="col-md-1">
                                <button class="btn green fileinput-exists" type="submit">
                                    <i class="fa fa-check"></i>
                                    Subir
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<br>
<br>
<div id="datosTablaplanillas">

</div>

@endsection @section('content_js')
<script src="{{ asset('js/planillas.js') }}"></script>
<script src="{{ asset('js/bootstrap-fileinput.js') }}"></script>
<script src="{{ asset('js/ui-blockui.min.js') }}"></script>
@stop
