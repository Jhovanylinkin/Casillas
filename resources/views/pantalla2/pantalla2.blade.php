@extends('layouts.metronic') @section('Activo')
<a href="/pantalla2">
    <span class="selected"></span>
</a>
@endsection @section('content') 

<div class="tabbable-custom nav-justified">
<ul class="nav nav-tabs nav-justified">
<li class="active">
    <a href="#tab_1_1_1" data-toggle="tab" aria-expanded="true"> <i class="fa fa-edit"></i> Captura</a>
</li>
<li class="">
    <a href="#tab_1_1_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-comments"></i>Reportadas </a>
</li>
<li class="">
    <a href="#tab_1_1_3" data-toggle="tab" aria-expanded="falseS"><i class="fa fa-question-circle"></i> No Reportadas</a>
</li>
</ul>
<div class="tab-content">
<div class="tab-pane active" id="tab_1_1_1">
<div class="col-table-row"> 
    <div class="col-app col-unscrollable"> 
    <div class="col-app col-unscrollable">
    <div class="widget widget-tabs widget-tabs-responsive">
    <div class="widget-body">
    <div class="tab-content">
    <div class="tab-pane active" id="tab-1">
    <form id="formInstalacion" novalidate="novalidate">
    <div  class="alert bg-info" role="alert">
    <strong>Datos de la casilla</strong>
    </div>
    <div class="innerAll inner-2x">
    <div class="col-md-6 col-lg-4">
    <div class="input-group" data-toggle="tooltip" data-original-title="Escribe Sección" data-placement="top">
            <span class="input-group-addon inputLabel"><i class="fa fa-th-large text-primary"></i> Sección</span>
        <input type="text" class="form-control" id="Seccion" name="Seccion" value="">
        <span class="input-group-btn">
            <button onclick="getCasillas()" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
        </span>
    </div>
    </div>
    <div class="col-md-6 col-lg-4">
    <div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Casilla" data-placement="top">
            <span class="input-group-addon inputLabel"><i class="fa fa-inbox text-primary"></i> Casilla</span>
            <select class="form-control" id="Casilla" name="Casilla">
                <option value="">Selecciona...</option>
            </select>
    </div>
    </div>
    <div class="clearfix"></div>
    </div><br>
    <div  class="alert bg-info" role="alert">
    <strong>Datos de Instalación</strong>

    </div>
    <div id="divInputs" class="innerAll">
    <div class="alert bg-success">
    <strong >Casillas capturadas por distrito: <span id="txtcapturado"></span></strong>
    </div>
    <div class="clearfix"></div><br>
    <div class="col-md-12"><label>Hora de Instalación (formato de 24 horas, por ej: 13:30)</label></div>
    <div class="col-md-4 col-lg-3">
    <div class="input-group">
            <span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
        <input type="text" class="form-control" name="HoraInstalacion" value="">
            <span class="input-group-addon inputLabel">Hrs</span>
    </div>
    </div>
    <div class="col-md-4 col-lg-3">
    <div class="input-group">
            <span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
        <input type="text" class="form-control" name="MinutosInstalacion" value="">
            <span class="input-group-addon inputLabel">Min</span>
    </div>
    </div>
    <div class="col-md-12"><label>Número de funcionarios de casilla presente</label></div>
    <div class="col-md-4 col-lg-3">
    <div class="input-group">
            <span class="input-group-addon inputLabel text-primary">#</span>
        <input type="text" class="form-control" name="NumFuncionariosPresentes" value="">
    </div>
    </div>
    <div class="col-md-12"><label>¿Tomados de la fila?</label></div>
    <div class="col-md-4 col-lg-3">
    <div class="input-group">
            <span class="input-group-addon inputLabel text-primary">#</span>
        <input type="text" class="form-control" name="NumFuncionariosFila" value="">
    </div>
    </div>
    <div class="col-md-12"><label>Hora de visita (formato de 24 horas, por ej: 13:30)</label></div>
    <div class="col-md-4 col-lg-3">
    <div class="input-group">
            <span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
        <input type="text" class="form-control" name="HoraVisita" value="">
            <span class="input-group-addon inputLabel">Hrs</span>
            <input type="hidden" id="validHora" name="validHora" value="">
    </div>
    </div>
    <div class="col-md-4 col-lg-3">
    <div class="input-group">
            <span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
        <input type="text" class="form-control" name="MinutosVisita" value="">
            <span class="input-group-addon inputLabel">Min</span>
    </div>
    </div>
    <div class="clearfix"></div><br>
    <div class="col-md-8 col-lg-10 innerAll center">
    <button onclick="$('#formInstalacion').submit()" id="btnGuardar" type="button" class="btn btn-success"><i class="fa fa-check-circle"></i> Guardar</button> &nbsp;&nbsp;&nbsp;
    <a href="?act=3x00" type="button" class="btn btn-danger no-ajaxify"><i class="fa fa-exclamation-triangle"></i> Registrar Incidente</a>
    <div class="clearfix"></div><br><br>
    </div>
    </div>
    </form>
    </div>
    <div class="tab-pane" id="tab-2">
    <div class="innerAll"><br>
    <div class="col-md-4">
    <div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Distrito" data-placement="top">
        <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-sitemap text-primary"></i></span>
    <select name="Distrito" class="form-control">
    <option value="">Selecciona Distrito...</option>
        <option value="1">TUXTLA GUTIÉRREZ ORIENTE</option>
        <option value="2">TUXTLA GUTIÉRREZ PONIENTE</option>
        <option value="3">CHIAPA DE CORZO</option>
        <option value="4">VENUSTIANO CARRANZA</option>
        <option value="5">SAN CRISTÓBAL DE LAS CASAS</option>
        <option value="6">COMITÁN</option>
        <option value="7">OCOSINGO</option>
        <option value="8">YAJALÓN</option>
        <option value="9">PALENQUE</option>
        <option value="10">BOCHIL</option>
        <option value="11">PUEBLO NVO. SOLISTAHUACÁN</option>
        <option value="12">PICHUCALCO</option>
        <option value="13">COPAINALÁ</option>
        <option value="14">CINTALAPA</option>
        <option value="15">TONALÁ</option>
        <option value="16">HUIXTLA</option>
        <option value="17">MOTOZINTLA</option>
        <option value="18">TAPACHULA NORTE</option>
        <option value="19">TAPACHULA SUR</option>
        <option value="20">LAS MARGARITAS</option>
        <option value="21">TENEJAPA</option>
        <option value="22">CHAMULA</option>
        <option value="23">VILLAFLORES</option>
        <option value="24">CACAHOATÁN</option>
    </select>
    </div>
    </div>
    <div class="col-md-4">
    <div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Municipio" data-placement="top">
    <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-globe text-primary"></i></span>
    <select name="Municipio" class="form-control">
    <option value="">Selecciona Municipio...</option>
    </select>
    </div>
    </div>
    <div class="col-md-4">
    <div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Sección" data-placement="top">
    <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-th-large text-primary"></i></span>
    <select name="Seccion" class="form-control">
    <option value="">Selecciona Sección...</option>
    </select>
    </div>
    </div>
    </div><br>&nbsp;
    <div class="divDatos"></div>
    </div>
    <div class="tab-pane" id="tab-3">
    <div class="innerAll"><br>
    <div class="col-md-4">
    <div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Distrito" data-placement="top">
    <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-sitemap text-primary"></i></span>
    <select name="Distrito" class="form-control">
    <option value="">Selecciona Distrito...</option>
            <option value="1">TUXTLA GUTIÉRREZ ORIENTE</option>
            <option value="2">TUXTLA GUTIÉRREZ PONIENTE</option>
            <option value="3">CHIAPA DE CORZO</option>
            <option value="4">VENUSTIANO CARRANZA</option>
            <option value="5">SAN CRISTÓBAL DE LAS CASAS</option>
            <option value="6">COMITÁN</option>
            <option value="7">OCOSINGO</option>
            <option value="8">YAJALÓN</option>
            <option value="9">PALENQUE</option>
            <option value="10">BOCHIL</option>
            <option value="11">PUEBLO NVO. SOLISTAHUACÁN</option>
            <option value="12">PICHUCALCO</option>
            <option value="13">COPAINALÁ</option>
            <option value="14">CINTALAPA</option>
            <option value="15">TONALÁ</option>
            <option value="16">HUIXTLA</option>
            <option value="17">MOTOZINTLA</option>
            <option value="18">TAPACHULA NORTE</option>
            <option value="19">TAPACHULA SUR</option>
            <option value="20">LAS MARGARITAS</option>
            <option value="21">TENEJAPA</option>
            <option value="22">CHAMULA</option>
            <option value="23">VILLAFLORES</option>
            <option value="24">CACAHOATÁN</option>
    </select>
    </div>
    </div>
    <div class="col-md-4">
        <div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Municipio" data-placement="top">
                <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-globe text-primary"></i></span>
            <select name="Municipio" class="form-control">
                <option value="">Selecciona Municipio...</option>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Sección" data-placement="top">
                <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-th-large text-primary"></i></span>
            <select name="Seccion" class="form-control">
                <option value="">Selecciona Sección...</option>
            </select>
        </div>
    </div>
    </div><br>&nbsp;
    <div class="divDatos"></div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
</div>
<div class="tab-pane" id="tab_1_1_2">

<div class="col-table-row"> 
<div class="col-app col-unscrollable"> 
<div class="col-app col-unscrollable">
<div class="widget widget-tabs widget-tabs-responsive">
<div class="widget-body">
<div class="tab-content">
<div class="tab-pane" id="tab-1">
<form id="formInstalacion" novalidate="novalidate">
<div class="innerAll bg-gray border">
<strong>Datos de la casilla</strong>
<div class="clearfix"></div>
</div>
<div class="innerAll inner-2x">
<div class="col-md-6 col-lg-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Escribe Sección" data-placement="top">
<span class="input-group-addon inputLabel"><i class="fa fa-th-large text-primary"></i> Sección</span>
<input type="text" class="form-control error" id="Seccion" name="Seccion" value=""><label for="Seccion" class="error">Escribe número de Sección</label>
<span class="input-group-btn">
<button onclick="getCasillas()" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
</span>
</div>
</div>
<div class="col-md-6 col-lg-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Casilla" data-placement="top">
<span class="input-group-addon inputLabel"><i class="fa fa-inbox text-primary"></i> Casilla</span>
<select class="form-control" id="Casilla" name="Casilla">
    <option value="">Selecciona...</option>
</select>
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="innerAll bg-gray border">
<strong>Datos de Instalación</strong>
<div class="clearfix"></div>
</div>
<div id="divInputs" class="innerAll">
<div class="alert alert-primary">
<strong>Casillas capturadas por distrito: <span id="txtcapturado"></span></strong>
</div>
<div class="clearfix"></div><br>
<div class="col-md-12"><label>Hora de Instalación (formato de 24 horas, por ej: 13:30)</label></div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
<span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
<input type="text" class="form-control" name="HoraInstalacion" value="">
<span class="input-group-addon inputLabel">Hrs</span>
</div>
</div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
<span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
<input type="text" class="form-control" name="MinutosInstalacion" value="">
<span class="input-group-addon inputLabel">Min</span>
</div>
</div>
<div class="col-md-12"><label>Número de funcionarios de casilla presente</label></div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
<span class="input-group-addon inputLabel text-primary">#</span>
<input type="text" class="form-control" name="NumFuncionariosPresentes" value="">
</div>
</div>
<div class="col-md-12"><label>¿Tomados de la fila?</label></div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
<span class="input-group-addon inputLabel text-primary">#</span>
<input type="text" class="form-control" name="NumFuncionariosFila" value="">
</div>
</div>
<div class="col-md-12"><label>Hora de visita (formato de 24 horas, por ej: 13:30)</label></div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
<span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
<input type="text" class="form-control" name="HoraVisita" value="">
<span class="input-group-addon inputLabel">Hrs</span>
<input type="hidden" id="validHora" name="validHora" value="">
</div>
</div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
<span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
<input type="text" class="form-control" name="MinutosVisita" value="">
<span class="input-group-addon inputLabel">Min</span>
</div>
</div>
<div class="clearfix"></div><br>
<div class="col-md-8 col-lg-6 innerAll center">
<button onclick="$('#formInstalacion').submit()" id="btnGuardar" type="button" class="btn btn-success"><i class="fa fa-check-circle"></i> Guardar</button> &nbsp;&nbsp;&nbsp;
<a href="?act=3x00" type="button" class="btn btn-danger no-ajaxify"><i class="fa fa-exclamation-triangle"></i> Registrar Incidente</a>
<div class="clearfix"></div><br><br>
</div>
</div>
</form>
</div>
<div class="tab-pane" id="tab-2">
<div class="innerAll"><br>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Distrito" data-placement="top">
<span class="input-group-addon inputLabel loadingDist"><i class="fa fa-sitemap text-primary"></i></span>
<select name="Distrito" class="form-control">
<option value="">Selecciona Distrito...</option>
    <option value="1">TUXTLA GUTIÉRREZ ORIENTE</option>
    <option value="2">TUXTLA GUTIÉRREZ PONIENTE</option>
    <option value="3">CHIAPA DE CORZO</option>
    <option value="4">VENUSTIANO CARRANZA</option>
    <option value="5">SAN CRISTÓBAL DE LAS CASAS</option>
    <option value="6">COMITÁN</option>
    <option value="7">OCOSINGO</option>
    <option value="8">YAJALÓN</option>
    <option value="9">PALENQUE</option>
    <option value="10">BOCHIL</option>
    <option value="11">PUEBLO NVO. SOLISTAHUACÁN</option>
    <option value="12">PICHUCALCO</option>
    <option value="13">COPAINALÁ</option>
    <option value="14">CINTALAPA</option>
    <option value="15">TONALÁ</option>
    <option value="16">HUIXTLA</option>
    <option value="17">MOTOZINTLA</option>
    <option value="18">TAPACHULA NORTE</option>
    <option value="19">TAPACHULA SUR</option>
    <option value="20">LAS MARGARITAS</option>
    <option value="21">TENEJAPA</option>
    <option value="22">CHAMULA</option>
    <option value="23">VILLAFLORES</option>
    <option value="24">CACAHOATÁN</option>
</select>
</div>
</div>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Municipio" data-placement="top">
    <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-globe text-primary"></i></span>
<select name="Municipio" class="form-control">
    <option value="">Selecciona Municipio...</option>
</select>
</div>
</div>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Sección" data-placement="top">
    <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-th-large text-primary"></i></span>
<select name="Seccion" class="form-control">
    <option value="">Selecciona Sección...</option>
</select>
</div>
</div>
</div><br>&nbsp;
<div class="divDatos">	<table class="table table-striped table-primary">
<thead>
<tr>
<th class="left" width="200">Distrito</th>
<th class="left" width="200">Municipio</th>
<th class="left" width="100">Seccion</th>
<th class="left" width="150">Casilla</th>
<th class="left" width="500">Supervisor y CAE</th>
<th class="left" width="100">Status</th>
</tr>
</thead>
<tbody>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1610</td>
<td onclick="setCaptura(1610,'E1C2')" class="left clickcasilla">
[E1C2] EXTRAORDINARIA 1 CONTIGUA 2						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> LÓPEZ  GUTIÉRREZ MARÍA GUADALUPE - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> NAVARRETE SALAZAR EDGAR MAURICIO - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1974</td>
<td onclick="setCaptura(1974,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> LANDEROS MENECES RAFAEL - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> ESTRADA LÓPEZ CRISTINA HERMILA - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1975</td>
<td onclick="setCaptura(1975,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> LANDEROS MENECES RAFAEL - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> ESTRADA LÓPEZ CRISTINA HERMILA - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ PONIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1739</td>
<td onclick="setCaptura(1739,'C6')" class="left clickcasilla">
[C6] CONTIGUA 6						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> AQUINO BORRAZ ROSA ANGELICA - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> MENDOZA OVILLA JOSE LUIS - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">CHIAPA DE CORZO</td>
<td class="left">ACALA</td>
<td class="left">7</td>
<td onclick="setCaptura(7,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> GOMEZ ZAPATA OSWALDO DE JESUS - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> ROBLES  MENDEZ MERCEDES YANELI - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">CHIAPA DE CORZO</td>
<td class="left">CHIAPA DE CORZO</td>
<td class="left">425</td>
<td onclick="setCaptura(425,'E1')" class="left clickcasilla">
[E1] EXTRAORDINARIA 1						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> SUAREZ SANCHEZ LILLIAN DINORAH - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> MENDEZ TOALA CARLOS ALBERTO - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">CHIAPA DE CORZO</td>
<td class="left">IXTAPA</td>
<td class="left">629</td>
<td onclick="setCaptura(629,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> HERNANDEZ LOPEZ BERSAIN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> ESCOBAR GOMEZ JUAN CARLOS - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">CHIAPA DE CORZO</td>
<td class="left">IXTAPA</td>
<td class="left">629</td>
<td onclick="setCaptura(629,'C1')" class="left clickcasilla">
[C1] CONTIGUA 1						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> HERNANDEZ LOPEZ BERSAIN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> ESCOBAR GOMEZ JUAN CARLOS - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">VENUSTIANO CARRANZA</td>
<td class="left">CHIAPILLA</td>
<td class="left">435</td>
<td onclick="setCaptura(435,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> RUIZ GUTIERREZ HUANERGE EDMAR  - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> VILA VILLATORO MARIA TERESA - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">VENUSTIANO CARRANZA</td>
<td class="left">NICOLAS RUIZ</td>
<td class="left">814</td>
<td onclick="setCaptura(814,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> SANCHEZ DIAZ ALBERTO ANTONIO - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> MORENO PEREZ LORENZO - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
</tbody>
</table>
<div class="center strong">
<small>Pag. 1 de 32 &nbsp;&nbsp; - &nbsp;&nbsp; 314 casillas sin reportar.</small>
</div>
<div class="center">
<ul class="pagination">
<li class="prev disabled"><a href="#">← Primero</a></li> 		<li class="prev disabled"><a href="#">&lt; Ant.</a></li>					<li class="prev "><a href="#" onclick="getNoRep('1','10','32');">Sig. &gt; </a></li>					<li class="prev "><a href="#" onclick="getNoRep('31','10','32')">Ultimo → </a></li>		</ul>
</div>
</div>
</div>
<div class="tab-pane active" id="tab-3">
<div class="innerAll"><br>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Distrito" data-placement="top">
<span class="input-group-addon inputLabel loadingDist"><i class="fa fa-sitemap text-primary"></i></span>
<select name="Distrito" class="form-control">
<option value="">Selecciona Distrito...</option>
    <option value="1">TUXTLA GUTIÉRREZ ORIENTE</option>
    <option value="2">TUXTLA GUTIÉRREZ PONIENTE</option>
    <option value="3">CHIAPA DE CORZO</option>
    <option value="4">VENUSTIANO CARRANZA</option>
    <option value="5">SAN CRISTÓBAL DE LAS CASAS</option>
    <option value="6">COMITÁN</option>
    <option value="7">OCOSINGO</option>
    <option value="8">YAJALÓN</option>
    <option value="9">PALENQUE</option>
    <option value="10">BOCHIL</option>
    <option value="11">PUEBLO NVO. SOLISTAHUACÁN</option>
    <option value="12">PICHUCALCO</option>
    <option value="13">COPAINALÁ</option>
    <option value="14">CINTALAPA</option>
    <option value="15">TONALÁ</option>
    <option value="16">HUIXTLA</option>
    <option value="17">MOTOZINTLA</option>
    <option value="18">TAPACHULA NORTE</option>
    <option value="19">TAPACHULA SUR</option>
    <option value="20">LAS MARGARITAS</option>
    <option value="21">TENEJAPA</option>
    <option value="22">CHAMULA</option>
    <option value="23">VILLAFLORES</option>
    <option value="24">CACAHOATÁN</option>
</select>
</div>
</div>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Municipio" data-placement="top">
    <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-globe text-primary"></i></span>
<select name="Municipio" class="form-control">
    <option value="">Selecciona Municipio...</option>
</select>
</div>
</div>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Sección" data-placement="top">
    <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-th-large text-primary"></i></span>
<select name="Seccion" class="form-control">
    <option value="">Selecciona Sección...</option>
</select>
</div>
</div>
</div><br>&nbsp;
<div class="divDatos">	<table class="table table-striped table-primary">
<thead>
<tr>
<th class="left" width="200">Distrito</th>
<th class="left" width="200">Municipio</th>
<th class="left" width="100">Seccion</th>
<th class="left" width="150">Casilla</th>
<th class="left" width="500">Supervisor y CAE</th>
<th class="left" width="100">Status</th>
</tr>
</thead>
<tbody>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1604</td>
<td onclick="setCaptura(1604,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						</td>
<td class="left">
<span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
</td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1604</td>
<td onclick="setCaptura(1604,'C1')" class="left clickcasilla">
[C1] CONTIGUA 1						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						</td>
<td class="left">
<span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
</td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1604</td>
<td onclick="setCaptura(1604,'C2')" class="left clickcasilla">
[C2] CONTIGUA 2						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						</td>
<td class="left">
<span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
</td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1604</td>
<td onclick="setCaptura(1604,'C3')" class="left clickcasilla">
[C3] CONTIGUA 3						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						</td>
<td class="left">
<span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
</td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1604</td>
<td onclick="setCaptura(1604,'C4')" class="left clickcasilla">
[C4] CONTIGUA 4						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						</td>
<td class="left">
<span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
</td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1605</td>
<td onclick="setCaptura(1605,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> JUÁREZ DÍAZ MANUEL DE JESÚS - <i class="fa fa-phone"></i>						</td>
<td class="left">
<span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
</td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1605</td>
<td onclick="setCaptura(1605,'C1')" class="left clickcasilla">
[C1] CONTIGUA 1						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> JUÁREZ DÍAZ MANUEL DE JESÚS - <i class="fa fa-phone"></i>						</td>
<td class="left">
<span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
</td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1605</td>
<td onclick="setCaptura(1605,'C2')" class="left clickcasilla">
[C2] CONTIGUA 2						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> JUÁREZ DÍAZ MANUEL DE JESÚS - <i class="fa fa-phone"></i>						</td>
<td class="left">
<span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
</td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1605</td>
<td onclick="setCaptura(1605,'C3')" class="left clickcasilla">
[C3] CONTIGUA 3						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> JUÁREZ DÍAZ MANUEL DE JESÚS - <i class="fa fa-phone"></i>						</td>
<td class="left">
<span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
</td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1607</td>
<td onclick="setCaptura(1607,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						</td>
<td class="left">
<span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
</td>
</tr>
</tbody>
</table>
<div class="center strong">
<small>Pag. 1 de 565 &nbsp;&nbsp; - &nbsp;&nbsp; 5644 casillas reportadas.</small>
</div>
<div>
<ul class="pagination">
<li class="prev disabled"><a href="#">← Primero</a></li> 		<li class="prev disabled"><a href="#">&lt; Ant.</a></li>					
<li class="prev "><a href="#" onclick="getRep('1','10','565');">Sig. &gt; </a></li>					
<li class="prev "><a href="#" onclick="getRep('564','10','565')">Ultimo → </a></li>		
</ul>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="tab-pane" id="tab_1_1_3">
<div class="col-table-row"> 
<div class="col-app col-unscrollable"> 
<div class="col-app col-unscrollable">
<div class="widget widget-tabs widget-tabs-responsive">
<div class="widget-body">
<div class="tab-content">
<div class="tab-pane" id="tab-1">
<form id="formInstalacion" novalidate="novalidate">
<div class="innerAll bg-gray border">
<strong>Datos de la casilla</strong>
<div class="clearfix"></div>
</div>
<div class="innerAll inner-2x">
<div class="col-md-6 col-lg-4">
    <div class="input-group" data-toggle="tooltip" data-original-title="Escribe Sección" data-placement="top">
            <span class="input-group-addon inputLabel"><i class="fa fa-th-large text-primary"></i> Sección</span>
        <input type="text" class="form-control" id="Seccion" name="Seccion" value="">
        <span class="input-group-btn">
            <button onclick="getCasillas()" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
        </span>
    </div>
</div>
<div class="col-md-6 col-lg-4">
    <div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Casilla" data-placement="top">
            <span class="input-group-addon inputLabel"><i class="fa fa-inbox text-primary"></i> Casilla</span>
            <select class="form-control" id="Casilla" name="Casilla">
                <option value="">Selecciona...</option>
            </select>
    </div>
</div>
<div class="clearfix"></div>
</div>
<div class="innerAll bg-gray border">
<strong>Datos de Instalación</strong>
<div class="clearfix"></div>
</div>
<div id="divInputs" class="innerAll">
<div class="alert alert-primary">
    <strong>Casillas capturadas por distrito: <span id="txtcapturado"></span></strong>
</div>
<div class="clearfix"></div><br>
<div class="col-md-12"><label>Hora de Instalación (formato de 24 horas, por ej: 13:30)</label></div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
    <span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
<input type="text" class="form-control" name="HoraInstalacion" value="">
    <span class="input-group-addon inputLabel">Hrs</span>
</div>
</div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
    <span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
<input type="text" class="form-control" name="MinutosInstalacion" value="">
    <span class="input-group-addon inputLabel">Min</span>
</div>
</div>
<div class="col-md-12"><label>Número de funcionarios de casilla presente</label></div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
    <span class="input-group-addon inputLabel text-primary">#</span>
<input type="text" class="form-control" name="NumFuncionariosPresentes" value="">
</div>
</div>
<div class="col-md-12"><label>¿Tomados de la fila?</label></div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
    <span class="input-group-addon inputLabel text-primary">#</span>
<input type="text" class="form-control" name="NumFuncionariosFila" value="">
</div>
</div>
<div class="col-md-12"><label>Hora de visita (formato de 24 horas, por ej: 13:30)</label></div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
    <span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
<input type="text" class="form-control" name="HoraVisita" value="">
    <span class="input-group-addon inputLabel">Hrs</span>
    <input type="hidden" id="validHora" name="validHora" value="">
</div>
</div>
<div class="col-md-4 col-lg-3">
<div class="input-group">
    <span class="input-group-addon inputLabel"><i class="fa fa-clock-o text-primary"></i></span>
<input type="text" class="form-control" name="MinutosVisita" value="">
    <span class="input-group-addon inputLabel">Min</span>
</div>
</div>
<div class="clearfix"></div><br>
<div class="col-md-8 col-lg-6 innerAll center">
    <button onclick="$('#formInstalacion').submit()" id="btnGuardar" type="button" class="btn btn-success"><i class="fa fa-check-circle"></i> Guardar</button> &nbsp;&nbsp;&nbsp;
    <a href="?act=3x00" type="button" class="btn btn-danger no-ajaxify"><i class="fa fa-exclamation-triangle"></i> Registrar Incidente</a>
    <div class="clearfix"></div><br><br>
</div>
</div>
</form>
</div>
<div class="tab-pane active" id="tab-2">
<div class="innerAll"><br>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Distrito" data-placement="top">
<span class="input-group-addon inputLabel loadingDist"><i class="fa fa-sitemap text-primary"></i></span>
<select name="Distrito" class="form-control">
<option value="">Selecciona Distrito...</option>
        <option value="1">TUXTLA GUTIÉRREZ ORIENTE</option>
        <option value="2">TUXTLA GUTIÉRREZ PONIENTE</option>
        <option value="3">CHIAPA DE CORZO</option>
        <option value="4">VENUSTIANO CARRANZA</option>
        <option value="5">SAN CRISTÓBAL DE LAS CASAS</option>
        <option value="6">COMITÁN</option>
        <option value="7">OCOSINGO</option>
        <option value="8">YAJALÓN</option>
        <option value="9">PALENQUE</option>
        <option value="10">BOCHIL</option>
        <option value="11">PUEBLO NVO. SOLISTAHUACÁN</option>
        <option value="12">PICHUCALCO</option>
        <option value="13">COPAINALÁ</option>
        <option value="14">CINTALAPA</option>
        <option value="15">TONALÁ</option>
        <option value="16">HUIXTLA</option>
        <option value="17">MOTOZINTLA</option>
        <option value="18">TAPACHULA NORTE</option>
        <option value="19">TAPACHULA SUR</option>
        <option value="20">LAS MARGARITAS</option>
        <option value="21">TENEJAPA</option>
        <option value="22">CHAMULA</option>
        <option value="23">VILLAFLORES</option>
        <option value="24">CACAHOATÁN</option>
</select>
</div>
</div>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Municipio" data-placement="top">
        <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-globe text-primary"></i></span>
    <select name="Municipio" class="form-control">
        <option value="">Selecciona Municipio...</option>
    </select>
</div>
</div>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Sección" data-placement="top">
        <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-th-large text-primary"></i></span>
    <select name="Seccion" class="form-control">
        <option value="">Selecciona Sección...</option>
    </select>
</div>
</div>
</div><br>&nbsp;
<div class="divDatos">	<table class="table table-striped table-primary">
<thead>
<tr>
<th class="left" width="200">Distrito</th>
<th class="left" width="200">Municipio</th>
<th class="left" width="100">Seccion</th>
<th class="left" width="150">Casilla</th>
<th class="left" width="500">Supervisor y CAE</th>
<th class="left" width="100">Status</th>
</tr>
</thead>
<tbody>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1610</td>
<td onclick="setCaptura(1610,'E1C2')" class="left clickcasilla">
[E1C2] EXTRAORDINARIA 1 CONTIGUA 2						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> LÓPEZ  GUTIÉRREZ MARÍA GUADALUPE - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> NAVARRETE SALAZAR EDGAR MAURICIO - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1974</td>
<td onclick="setCaptura(1974,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> LANDEROS MENECES RAFAEL - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> ESTRADA LÓPEZ CRISTINA HERMILA - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1975</td>
<td onclick="setCaptura(1975,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> LANDEROS MENECES RAFAEL - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> ESTRADA LÓPEZ CRISTINA HERMILA - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ PONIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1739</td>
<td onclick="setCaptura(1739,'C6')" class="left clickcasilla">
[C6] CONTIGUA 6						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> AQUINO BORRAZ ROSA ANGELICA - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> MENDOZA OVILLA JOSE LUIS - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">CHIAPA DE CORZO</td>
<td class="left">ACALA</td>
<td class="left">7</td>
<td onclick="setCaptura(7,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> GOMEZ ZAPATA OSWALDO DE JESUS - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> ROBLES  MENDEZ MERCEDES YANELI - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">CHIAPA DE CORZO</td>
<td class="left">CHIAPA DE CORZO</td>
<td class="left">425</td>
<td onclick="setCaptura(425,'E1')" class="left clickcasilla">
[E1] EXTRAORDINARIA 1						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> SUAREZ SANCHEZ LILLIAN DINORAH - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> MENDEZ TOALA CARLOS ALBERTO - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">CHIAPA DE CORZO</td>
<td class="left">IXTAPA</td>
<td class="left">629</td>
<td onclick="setCaptura(629,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> HERNANDEZ LOPEZ BERSAIN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> ESCOBAR GOMEZ JUAN CARLOS - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">CHIAPA DE CORZO</td>
<td class="left">IXTAPA</td>
<td class="left">629</td>
<td onclick="setCaptura(629,'C1')" class="left clickcasilla">
[C1] CONTIGUA 1						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> HERNANDEZ LOPEZ BERSAIN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> ESCOBAR GOMEZ JUAN CARLOS - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">VENUSTIANO CARRANZA</td>
<td class="left">CHIAPILLA</td>
<td class="left">435</td>
<td onclick="setCaptura(435,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> RUIZ GUTIERREZ HUANERGE EDMAR  - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> VILA VILLATORO MARIA TERESA - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
<tr>
<td class="left">VENUSTIANO CARRANZA</td>
<td class="left">NICOLAS RUIZ</td>
<td class="left">814</td>
<td onclick="setCaptura(814,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> SANCHEZ DIAZ ALBERTO ANTONIO - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> MORENO PEREZ LORENZO - <i class="fa fa-phone"></i>						</td>
<td class="left"><span class="label label-default"><i class="fa fa-question-circle"></i> SIN REPORTAR</span></td>
</tr>
</tbody>
</table>
<div class="center strong">
<small>Pag. 1 de 32 &nbsp;&nbsp; - &nbsp;&nbsp; 314 casillas sin reportar.</small>
</div>
<div class="center">
<ul class="pagination">
<li class="prev disabled"><a href="#">← Primero</a></li> 		<li class="prev disabled"><a href="#">&lt; Ant.</a></li>					<li class="prev "><a href="#" onclick="getNoRep('1','10','32');">Sig. &gt; </a></li>					<li class="prev "><a href="#" onclick="getNoRep('31','10','32')">Ultimo → </a></li>		</ul>
</div>
</div>
</div>
<div class="tab-pane" id="tab-3">
<div class="innerAll"><br>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Distrito" data-placement="top">
<span class="input-group-addon inputLabel loadingDist"><i class="fa fa-sitemap text-primary"></i></span>
<select name="Distrito" class="form-control">
<option value="">Selecciona Distrito...</option>
        <option value="1">TUXTLA GUTIÉRREZ ORIENTE</option>
        <option value="2">TUXTLA GUTIÉRREZ PONIENTE</option>
        <option value="3">CHIAPA DE CORZO</option>
        <option value="4">VENUSTIANO CARRANZA</option>
        <option value="5">SAN CRISTÓBAL DE LAS CASAS</option>
        <option value="6">COMITÁN</option>
        <option value="7">OCOSINGO</option>
        <option value="8">YAJALÓN</option>
        <option value="9">PALENQUE</option>
        <option value="10">BOCHIL</option>
        <option value="11">PUEBLO NVO. SOLISTAHUACÁN</option>
        <option value="12">PICHUCALCO</option>
        <option value="13">COPAINALÁ</option>
        <option value="14">CINTALAPA</option>
        <option value="15">TONALÁ</option>
        <option value="16">HUIXTLA</option>
        <option value="17">MOTOZINTLA</option>
        <option value="18">TAPACHULA NORTE</option>
        <option value="19">TAPACHULA SUR</option>
        <option value="20">LAS MARGARITAS</option>
        <option value="21">TENEJAPA</option>
        <option value="22">CHAMULA</option>
        <option value="23">VILLAFLORES</option>
        <option value="24">CACAHOATÁN</option>
</select>
</div>
</div>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Municipio" data-placement="top">
        <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-globe text-primary"></i></span>
    <select name="Municipio" class="form-control">
        <option value="">Selecciona Municipio...</option>
    </select>
</div>
</div>
<div class="col-md-4">
<div class="input-group" data-toggle="tooltip" data-original-title="Selecciona Sección" data-placement="top">
        <span class="input-group-addon inputLabel loadingDist"><i class="fa fa-th-large text-primary"></i></span>
    <select name="Seccion" class="form-control">
        <option value="">Selecciona Sección...</option>
    </select>
</div>
</div>
</div><br>&nbsp;
<div class="divDatos">	<table class="table table-striped table-primary">
<thead>
<tr>
<th class="left" width="200">Distrito</th>
<th class="left" width="200">Municipio</th>
<th class="left" width="100">Seccion</th>
<th class="left" width="150">Casilla</th>
<th class="left" width="500">Supervisor y CAE</th>
<th class="left" width="100">Status</th>
</tr>
</thead>
<tbody>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1604</td>
<td onclick="setCaptura(1604,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						
</td>
<td class="left">
            <span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
        </td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1604</td>
<td onclick="setCaptura(1604,'C1')" class="left clickcasilla">
[C1] CONTIGUA 1						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						
</td>
<td class="left">
            <span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
        </td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1604</td>
<td onclick="setCaptura(1604,'C2')" class="left clickcasilla">
[C2] CONTIGUA 2						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						
</td>
<td class="left">
            <span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
        </td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1604</td>
<td onclick="setCaptura(1604,'C3')" class="left clickcasilla">
[C3] CONTIGUA 3						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						
</td>
<td class="left">
            <span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
        </td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1604</td>
<td onclick="setCaptura(1604,'C4')" class="left clickcasilla">
[C4] CONTIGUA 4						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						
</td>
<td class="left">
            <span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
        </td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1605</td>
<td onclick="setCaptura(1605,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> JUÁREZ DÍAZ MANUEL DE JESÚS - <i class="fa fa-phone"></i>						
</td>
<td class="left">
            <span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
        </td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1605</td>
<td onclick="setCaptura(1605,'C1')" class="left clickcasilla">
[C1] CONTIGUA 1						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> JUÁREZ DÍAZ MANUEL DE JESÚS - <i class="fa fa-phone"></i>						
</td>
<td class="left">
            <span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
        </td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1605</td>
<td onclick="setCaptura(1605,'C2')" class="left clickcasilla">
[C2] CONTIGUA 2						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> JUÁREZ DÍAZ MANUEL DE JESÚS - <i class="fa fa-phone"></i>						
</td>
<td class="left">
            <span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
        </td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1605</td>
<td onclick="setCaptura(1605,'C3')" class="left clickcasilla">
[C3] CONTIGUA 3						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> JUÁREZ DÍAZ MANUEL DE JESÚS - <i class="fa fa-phone"></i>						
</td>
<td class="left">
            <span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
        </td>
</tr>
<tr>
<td class="left">TUXTLA GUTIÉRREZ ORIENTE</td>
<td class="left">TUXTLA GUTIÉRREZ</td>
<td class="left">1607</td>
<td onclick="setCaptura(1607,'B')" class="left clickcasilla">
[B] BÁSICA						</td>
<td class="left">
<span class="label label-info">SUPERVISOR</span> VALDÉZ FRANCO FERMÍN - <i class="fa fa-phone"></i> <br>
<span class="label label-info">CAE</span> AGUILAR ARELLANO ISABEL CRISTINA - <i class="fa fa-phone"></i>						
</td>
<td class="left">
            <span class="label label-success"><i class="fa fa-check"></i> INSTALADA</span>
        </td>
</tr>
</tbody>
</table>
<div class="strong">
<small>Pag. 1 de 565 &nbsp;&nbsp; - &nbsp;&nbsp; 5644 casillas reportadas.</small>
</div>
<div>
<ul class="pagination">
<li class="prev disabled"><a href="#">← Primero</a></li> 		<li class="prev disabled"><a href="#">&lt; Ant.</a></li>					
<li class="prev "><a href="#" onclick="getRep('1','10','565');">Sig. &gt; </a></li>					
<li class="prev "><a href="#" onclick="getRep('564','10','565')">Ultimo → </a></li>		
</ul>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
  

  
@endsection @section('content_js')
{{-- <script src="{{ asset('js/planillas.js') }}"></script> --}}
 @stop