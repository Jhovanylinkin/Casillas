<table class="table table-bordered table-striped table-condensed flip-content" id="tablaprep">
    <thead class="flip-content">
    <th>Acta</th>
    <th>Sección</th>
    <th>Casilla</th>
    <th>Municipio</th>
    @foreach($Partidos as $P)
        <th>{{$P->Partido}}</th>
    @endforeach
    </thead>
    <tbody>
        @foreach($dataToTable as $T)
        <tr>
            <td></td>
            <td>{{$T->SECCION}}</td>
            <td>{{$T->CASILLA}}</td>
            <td>{{$T->Mun}}</td>
            @foreach($Partidos as $P)
            <td style="display:none;">{{$VAL = $P->CampoSave}}</td>
            <td>{{$T->$VAL}}</td>
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
{{$dataToTable->links('pagination::bootstrap-4')}}