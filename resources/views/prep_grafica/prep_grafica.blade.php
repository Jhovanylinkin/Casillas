@extends('layouts.metronic') 
    @section('Activo')
    <a href="/prep_grafica">
        <span class="selected">Prep_Grafica</span>
    </a>
    @endsection 
@section('content')
<h1 class="page-title"> Prep Graficas
    <small></small>
</h1>
<div class="container">
    <form id="prepform" method="POST" class="form">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Tipo de elección<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                    <i class="fa fa-cube"></i>
                    <select type="text" id="TipoEleccion" class="form-control" name="TipoEleccion" required>
                        <option value="" default>Tipo Elección</option>
                        @forelse ($TipoE as $TE)
                        <option value="{{$TE->id}}">{{$TE->TipoEleccion}}</option>
                        @empty
                        <option value="">TipoEleccion is null</option>
                        @endforelse
                    </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4" id="comboMunicipio">
                <div class="form-group">
                    <label id="labelC" class="control-label">Municipio<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-map-marker"></i>
                        <select type="text" class="form-control" id="Municipio" name="Municipio">
                            <option value="" default>Municipio</option>
                            @forelse ($Muni as $M)
                            <option value="{{$M->Clave}}">{{$M->Municipio}}</option>
                            @empty
                            <option value="">Sin Municipios</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2" id="comboMunicipio">
                <div class="form-group">
                    <label class="control-label">&nbsp;</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <button style="float:right;" type="submit" class="btn btn-success mt-ladda-btn ladda-button" id="busqueda-filtro"
                            data-style="zoom-in" style="display: none;">
                            <span class="ladda-label">
                                <i class="icon-eye"></i> Ver gráfica</span>
                            <span class="ladda-spinner"></span>
                        </button>&nbsp
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="graphPrep" style="height: 400px; min-width: 100%; max-width: 800px; margin: 0 auto;"></div>
<div id="tablePrep"></div>
@endsection
@section('content_js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="{{ asset('js/jquery.bootstrap-growl.min.js') }}"></script>
<script src="{{ asset('js/prep_grafica.js')}}"></script>
<script src="{{ asset('js/prep_progress_graph.js')}}"></script> 
@stop