@extends('layouts.metronic') @section('Activo')
<a href="/pantalla4">
    <span class="selected"></span>
</a>
@endsection @section('content') 

<div class="box-generic">
<span><small>*Click sobre el nombre del distrito para ir al detalle</small></span><br>
<span><small>*El porcentaje es calculado tomando el número total del funcionario según corresponda entre el número de casillas instaladas, multiplicado por cien.</small></span>

<span class="pull-right">
<a href="index.php?act=1x02&amp;op=distPdf" title="PDF" class="btn btn-default" target="_blank"><i class="fa fa-file-text"></i> PDF</a>
&nbsp;
<a href="index.php?act=1x02&amp;op=distExcel" title="Excel" class="btn btn-default no-ajaxify"><i class="fa fa-file-text"></i> Excel</a>
</span>
<br><br>
<div class="clearfix"></div>

<div style="overflow-x: auto;" class="table-responsive">
<style type="text/css" media="screen">
table.table th{
    background: rgb(160, 160, 110);
    color: #ffffff;
}
</style>
<table class="table table-bordered table-striped">
<thead>
    <tr>
        <th colspan="3">&nbsp;</th>
        <th colspan="4">Presidente</th>
        <th colspan="4">Secretario</th>
        <th colspan="4">1er. Escrutador</th>
        <th colspan="13">Partidos</th>
        <th>&nbsp;</th>
    </tr>
    <tr>
        <th>Distrito</th>
        <th>Sección</th>
        <th>Casillas</th>
        <!-- <th>CAE</th> -->
        <!-- PRESIDENTE -->
        <th>P</th>
        <th>SG</th>
        <th>F</th>
        <th>SF</th>
        <!-- SECRETARIO -->
        <th>P</th>
        <th>SG</th>
        <th>F</th>
        <th>SF</th>
        <!-- 1ER ESCRUTADOR -->
        <th>P</th>
        <th>SG</th>
        <th>F</th>
        <th>SF</th>

        <!-- PARTIDOS -->
        <th><img src="img/partidos/4.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/3.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/5.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/7.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/6.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/8.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/9.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/12.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/13.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/14.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/10.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/11.png" alt="" style="width:25px;"></th>
        <th><img src="img/partidos/indep.png" alt="" style="width:25px;"></th>
        <!-- OBSERVADORES -->
        <th>Observadores</th>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <th>Totales:</th>
        <th>5958</th>
        <th>1161</th>
        <th>37</th>
        <th>4</th>
        <th>0</th>
        <th>1036</th>
        <th>132</th>
        <th>30</th>
        <th>4</th>
        <th>672</th>
        <th>390</th>
        <th>115</th>
        <th>25</th>
        <th>925</th>
        <th>1562</th>
        <th>698</th>
        <th>1605</th>
        <th>588</th>
        <th>369</th>
        <th>602</th>
        <th>849</th>
        <th>124</th>
        <th>126</th>
        <th>1023</th>
        <th>965</th>
        <th>120</th>
        <th>153</th>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <th>%</th>
        <th>&nbsp;</th>
        <th>20.65</th>
        <th>0.66</th>
        <th>0.07</th>
        <th>0</th>
        <th>18.43</th>
        <th>2.35</th>
        <th>0.53</th>
        <th>0.07</th>
        <th>11.95</th>
        <th>6.94</th>
        <th>2.05</th>
        <th>0.44</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
    </tr>
</thead>
<tbody>
                                                                            <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">TUXTLA GUTIÉRREZ ORIENTE</a></td>
            <td>153</td>
            <td>350</td>
            <!-- <th>&nbsp;</th> -->
            <td>70</td>
            <td>2</td>
            <td>1</td>
            <td>0</td>
            <td>58</td>
            <td>12</td>
            <td>3</td>
            <td>0</td>
            <td>17</td>
            <td>39</td>
            <td>12</td>
            <td>5</td>
            <td>71</td>
            <td>62</td>
            <td>25</td>
            <td>68</td>
            <td>45</td>
            <td>28</td>
            <td>62</td>
            <td>29</td>
            <td>4</td>
            <td>4</td>
            <td>7</td>
            <td>19</td>
            <td>3</td>
            <td>27</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">TUXTLA GUTIÉRREZ PONIENTE</a></td>
            <td>79</td>
            <td>317</td>
            <!-- <th>&nbsp;</th> -->
            <td>105</td>
            <td>9</td>
            <td>0</td>
            <td>0</td>
            <td>79</td>
            <td>25</td>
            <td>9</td>
            <td>1</td>
            <td>46</td>
            <td>41</td>
            <td>13</td>
            <td>14</td>
            <td>110</td>
            <td>109</td>
            <td>34</td>
            <td>111</td>
            <td>36</td>
            <td>28</td>
            <td>70</td>
            <td>36</td>
            <td>3</td>
            <td>12</td>
            <td>20</td>
            <td>24</td>
            <td>29</td>
            <td>51</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">CHIAPA DE CORZO</a></td>
            <td>69</td>
            <td>206</td>
            <!-- <th>&nbsp;</th> -->
            <td>69</td>
            <td>2</td>
            <td>1</td>
            <td>0</td>
            <td>58</td>
            <td>13</td>
            <td>1</td>
            <td>0</td>
            <td>41</td>
            <td>27</td>
            <td>4</td>
            <td>0</td>
            <td>52</td>
            <td>95</td>
            <td>23</td>
            <td>110</td>
            <td>33</td>
            <td>40</td>
            <td>55</td>
            <td>86</td>
            <td>3</td>
            <td>5</td>
            <td>84</td>
            <td>82</td>
            <td>0</td>
            <td>18</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">VENUSTIANO CARRANZA</a></td>
            <td>44</td>
            <td>128</td>
            <!-- <th>&nbsp;</th> -->
            <td>23</td>
            <td>4</td>
            <td>1</td>
            <td>0</td>
            <td>24</td>
            <td>4</td>
            <td>0</td>
            <td>0</td>
            <td>16</td>
            <td>8</td>
            <td>4</td>
            <td>0</td>
            <td>10</td>
            <td>38</td>
            <td>17</td>
            <td>40</td>
            <td>6</td>
            <td>4</td>
            <td>11</td>
            <td>17</td>
            <td>2</td>
            <td>1</td>
            <td>18</td>
            <td>11</td>
            <td>0</td>
            <td>0</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">SAN CRISTÓBAL DE LAS CASAS</a></td>
            <td>62</td>
            <td>240</td>
            <!-- <th>&nbsp;</th> -->
            <td>42</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>35</td>
            <td>7</td>
            <td>1</td>
            <td>0</td>
            <td>16</td>
            <td>17</td>
            <td>10</td>
            <td>0</td>
            <td>32</td>
            <td>64</td>
            <td>34</td>
            <td>51</td>
            <td>12</td>
            <td>22</td>
            <td>16</td>
            <td>33</td>
            <td>0</td>
            <td>27</td>
            <td>53</td>
            <td>59</td>
            <td>0</td>
            <td>6</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">COMITÁN</a></td>
            <td>109</td>
            <td>306</td>
            <!-- <th>&nbsp;</th> -->
            <td>20</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>18</td>
            <td>3</td>
            <td>0</td>
            <td>0</td>
            <td>13</td>
            <td>4</td>
            <td>4</td>
            <td>0</td>
            <td>16</td>
            <td>31</td>
            <td>5</td>
            <td>29</td>
            <td>18</td>
            <td>6</td>
            <td>17</td>
            <td>14</td>
            <td>0</td>
            <td>0</td>
            <td>20</td>
            <td>18</td>
            <td>0</td>
            <td>16</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">OCOSINGO</a></td>
            <td>119</td>
            <td>446</td>
            <!-- <th>&nbsp;</th> -->
            <td>49</td>
            <td>3</td>
            <td>0</td>
            <td>0</td>
            <td>47</td>
            <td>3</td>
            <td>2</td>
            <td>0</td>
            <td>34</td>
            <td>9</td>
            <td>8</td>
            <td>1</td>
            <td>29</td>
            <td>66</td>
            <td>17</td>
            <td>72</td>
            <td>29</td>
            <td>27</td>
            <td>21</td>
            <td>28</td>
            <td>1</td>
            <td>10</td>
            <td>34</td>
            <td>56</td>
            <td>0</td>
            <td>1</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">YAJALÓN</a></td>
            <td>57</td>
            <td>192</td>
            <!-- <th>&nbsp;</th> -->
            <td>27</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>23</td>
            <td>5</td>
            <td>0</td>
            <td>0</td>
            <td>14</td>
            <td>14</td>
            <td>0</td>
            <td>0</td>
            <td>8</td>
            <td>40</td>
            <td>26</td>
            <td>40</td>
            <td>17</td>
            <td>0</td>
            <td>11</td>
            <td>18</td>
            <td>2</td>
            <td>0</td>
            <td>33</td>
            <td>29</td>
            <td>0</td>
            <td>3</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">PALENQUE</a></td>
            <td>70</td>
            <td>236</td>
            <!-- <th>&nbsp;</th> -->
            <td>50</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
            <td>49</td>
            <td>3</td>
            <td>0</td>
            <td>0</td>
            <td>31</td>
            <td>19</td>
            <td>2</td>
            <td>0</td>
            <td>42</td>
            <td>58</td>
            <td>44</td>
            <td>62</td>
            <td>34</td>
            <td>29</td>
            <td>30</td>
            <td>39</td>
            <td>23</td>
            <td>10</td>
            <td>53</td>
            <td>58</td>
            <td>16</td>
            <td>1</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">BOCHIL</a></td>
            <td>46</td>
            <td>150</td>
            <!-- <th>&nbsp;</th> -->
            <td>48</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>47</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>34</td>
            <td>13</td>
            <td>1</td>
            <td>0</td>
            <td>39</td>
            <td>74</td>
            <td>29</td>
            <td>69</td>
            <td>19</td>
            <td>6</td>
            <td>16</td>
            <td>25</td>
            <td>2</td>
            <td>17</td>
            <td>31</td>
            <td>28</td>
            <td>0</td>
            <td>1</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">PUEBLO NVO. SOLISTAHUACÁN</a></td>
            <td>31</td>
            <td>94</td>
            <!-- <th>&nbsp;</th> -->
            <td>18</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>15</td>
            <td>3</td>
            <td>0</td>
            <td>0</td>
            <td>14</td>
            <td>3</td>
            <td>1</td>
            <td>0</td>
            <td>11</td>
            <td>28</td>
            <td>22</td>
            <td>30</td>
            <td>5</td>
            <td>6</td>
            <td>2</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>20</td>
            <td>26</td>
            <td>2</td>
            <td>3</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">PICHUCALCO</a></td>
            <td>88</td>
            <td>227</td>
            <!-- <th>&nbsp;</th> -->
            <td>20</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
            <td>20</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
            <td>16</td>
            <td>6</td>
            <td>0</td>
            <td>0</td>
            <td>14</td>
            <td>29</td>
            <td>28</td>
            <td>37</td>
            <td>14</td>
            <td>16</td>
            <td>14</td>
            <td>11</td>
            <td>0</td>
            <td>1</td>
            <td>27</td>
            <td>17</td>
            <td>0</td>
            <td>0</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">COPAINALÁ</a></td>
            <td>69</td>
            <td>172</td>
            <!-- <th>&nbsp;</th> -->
            <td>22</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>22</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>13</td>
            <td>8</td>
            <td>1</td>
            <td>1</td>
            <td>25</td>
            <td>30</td>
            <td>7</td>
            <td>31</td>
            <td>18</td>
            <td>1</td>
            <td>14</td>
            <td>16</td>
            <td>0</td>
            <td>0</td>
            <td>17</td>
            <td>27</td>
            <td>20</td>
            <td>5</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">CINTALAPA</a></td>
            <td>104</td>
            <td>296</td>
            <!-- <th>&nbsp;</th> -->
            <td>30</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
            <td>26</td>
            <td>4</td>
            <td>0</td>
            <td>2</td>
            <td>17</td>
            <td>9</td>
            <td>6</td>
            <td>0</td>
            <td>24</td>
            <td>37</td>
            <td>18</td>
            <td>37</td>
            <td>17</td>
            <td>14</td>
            <td>24</td>
            <td>19</td>
            <td>5</td>
            <td>10</td>
            <td>30</td>
            <td>27</td>
            <td>0</td>
            <td>3</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">TONALÁ</a></td>
            <td>116</td>
            <td>300</td>
            <!-- <th>&nbsp;</th> -->
            <td>59</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>54</td>
            <td>4</td>
            <td>1</td>
            <td>0</td>
            <td>32</td>
            <td>21</td>
            <td>6</td>
            <td>0</td>
            <td>79</td>
            <td>90</td>
            <td>57</td>
            <td>86</td>
            <td>54</td>
            <td>29</td>
            <td>26</td>
            <td>55</td>
            <td>23</td>
            <td>2</td>
            <td>80</td>
            <td>58</td>
            <td>0</td>
            <td>1</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">HUIXTLA</a></td>
            <td>121</td>
            <td>318</td>
            <!-- <th>&nbsp;</th> -->
            <td>92</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>89</td>
            <td>3</td>
            <td>0</td>
            <td>0</td>
            <td>66</td>
            <td>25</td>
            <td>1</td>
            <td>0</td>
            <td>84</td>
            <td>108</td>
            <td>74</td>
            <td>116</td>
            <td>72</td>
            <td>28</td>
            <td>59</td>
            <td>98</td>
            <td>13</td>
            <td>9</td>
            <td>118</td>
            <td>96</td>
            <td>0</td>
            <td>2</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">MOTOZINTLA</a></td>
            <td>134</td>
            <td>367</td>
            <!-- <th>&nbsp;</th> -->
            <td>47</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>44</td>
            <td>3</td>
            <td>0</td>
            <td>0</td>
            <td>34</td>
            <td>11</td>
            <td>2</td>
            <td>0</td>
            <td>24</td>
            <td>61</td>
            <td>36</td>
            <td>64</td>
            <td>42</td>
            <td>8</td>
            <td>8</td>
            <td>37</td>
            <td>10</td>
            <td>2</td>
            <td>43</td>
            <td>35</td>
            <td>0</td>
            <td>1</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">TAPACHULA NORTE</a></td>
            <td>69</td>
            <td>159</td>
            <!-- <th>&nbsp;</th> -->
            <td>42</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>39</td>
            <td>1</td>
            <td>3</td>
            <td>0</td>
            <td>20</td>
            <td>18</td>
            <td>5</td>
            <td>0</td>
            <td>35</td>
            <td>55</td>
            <td>31</td>
            <td>54</td>
            <td>15</td>
            <td>13</td>
            <td>19</td>
            <td>47</td>
            <td>0</td>
            <td>0</td>
            <td>21</td>
            <td>23</td>
            <td>14</td>
            <td>1</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">TAPACHULA SUR</a></td>
            <td>103</td>
            <td>244</td>
            <!-- <th>&nbsp;</th> -->
            <td>118</td>
            <td>3</td>
            <td>0</td>
            <td>0</td>
            <td>99</td>
            <td>16</td>
            <td>6</td>
            <td>0</td>
            <td>49</td>
            <td>39</td>
            <td>29</td>
            <td>4</td>
            <td>130</td>
            <td>174</td>
            <td>40</td>
            <td>174</td>
            <td>45</td>
            <td>25</td>
            <td>58</td>
            <td>153</td>
            <td>15</td>
            <td>2</td>
            <td>95</td>
            <td>101</td>
            <td>30</td>
            <td>7</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">LAS MARGARITAS</a></td>
            <td>63</td>
            <td>219</td>
            <!-- <th>&nbsp;</th> -->
            <td>31</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>31</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>20</td>
            <td>11</td>
            <td>1</td>
            <td>0</td>
            <td>3</td>
            <td>20</td>
            <td>35</td>
            <td>39</td>
            <td>6</td>
            <td>2</td>
            <td>5</td>
            <td>2</td>
            <td>3</td>
            <td>0</td>
            <td>31</td>
            <td>33</td>
            <td>0</td>
            <td>0</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">TENEJAPA</a></td>
            <td>61</td>
            <td>193</td>
            <!-- <th>&nbsp;</th> -->
            <td>9</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>9</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>8</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>8</td>
            <td>16</td>
            <td>13</td>
            <td>17</td>
            <td>1</td>
            <td>1</td>
            <td>7</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
            <td>13</td>
            <td>9</td>
            <td>0</td>
            <td>0</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">CHAMULA</a></td>
            <td>103</td>
            <td>296</td>
            <!-- <th>&nbsp;</th> -->
            <td>107</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>92</td>
            <td>12</td>
            <td>3</td>
            <td>1</td>
            <td>74</td>
            <td>30</td>
            <td>4</td>
            <td>0</td>
            <td>19</td>
            <td>196</td>
            <td>35</td>
            <td>188</td>
            <td>14</td>
            <td>7</td>
            <td>14</td>
            <td>21</td>
            <td>0</td>
            <td>1</td>
            <td>109</td>
            <td>64</td>
            <td>0</td>
            <td>5</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">VILLAFLORES</a></td>
            <td>116</td>
            <td>313</td>
            <!-- <th>&nbsp;</th> -->
            <td>31</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>30</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>26</td>
            <td>4</td>
            <td>1</td>
            <td>0</td>
            <td>23</td>
            <td>36</td>
            <td>28</td>
            <td>37</td>
            <td>10</td>
            <td>7</td>
            <td>17</td>
            <td>27</td>
            <td>1</td>
            <td>12</td>
            <td>33</td>
            <td>36</td>
            <td>6</td>
            <td>1</td>
        </tr>
                                                                                <tr>
            <td><a href="#" title="Ir a detalle de distrito" class="no-ajaxify">CACAHOATÁN</a></td>
            <td>77</td>
            <td>189</td>
            <!-- <th>&nbsp;</th> -->
            <td>32</td>
            <td>1</td>
            <td>1</td>
            <td>0</td>
            <td>28</td>
            <td>6</td>
            <td>0</td>
            <td>0</td>
            <td>21</td>
            <td>13</td>
            <td>0</td>
            <td>0</td>
            <td>37</td>
            <td>45</td>
            <td>20</td>
            <td>43</td>
            <td>26</td>
            <td>22</td>
            <td>26</td>
            <td>35</td>
            <td>14</td>
            <td>1</td>
            <td>33</td>
            <td>29</td>
            <td>0</td>
            <td>0</td>
        </tr>
                                                                        </tbody>
</table>
</div>
</div>


<div class="box-generic">

<br><br>
<div class="clearfix"></div>

<div style="overflow-x: scroll;" class="table-responsive">
<style type="text/css" media="screen">
    table.table th{
        background: rgb(160, 160, 110);
        color: #ffffff;
    }
</style>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th colspan="3">&nbsp;</th>
            <th colspan="4">Presidente</th>
            <th colspan="4">Secretario</th>
            <th colspan="4">1er. Escrutador</th>

            <th colspan="13">Partidos</th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>Municipio</th>
            <th>Sección</th>
            <th>Casillas</th>
            <!-- <th>CAE</th> -->
            <!-- PRESIDENTE -->
            <th>P</th>
            <th>SG</th>
            <th>F</th>
            <th>SF</th>
            <!-- SECRETARIO -->
            <th>P</th>
            <th>SG</th>
            <th>F</th>
            <th>SF</th>
            <!-- 1ER ESCRUTADOR -->
            <th>P</th>
            <th>SG</th>
            <th>F</th>
            <th>SF</th>

            <!-- PARTIDOS -->
            <th><img src="img/partidos/4.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/3.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/5.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/7.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/6.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/8.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/9.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/12.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/13.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/14.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/10.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/11.png" alt="" style="width:25px;"></th>
            <th><img src="img/partidos/indep.png" alt="" style="width:25px;"></th>
            <!-- OBSERVADORES -->
            <th>Observadores</th>
        </tr>
        <tr>
            <th>Totales:</th>
            <th>153</th>
            <th>350</th>
            <!-- <th>&nbsp;</th> -->
            <th>70</th>
            <th>2</th>
            <th>1</th>
            <th>0</th>
            <th>58</th>
            <th>12</th>
            <th>3</th>
            <th>0</th>
            <th>17</th>
            <th>39</th>
            <th>12</th>
            <th>5</th>
            <th>71</th>
            <th>62</th>
            <th>25</th>
            <th>68</th>
            <th>45</th>
            <th>28</th>
            <th>62</th>
            <th>29</th>
            <th>4</th>
            <th>4</th>
            <th>7</th>
            <th>19</th>
            <th>3</th>
            <th>27</th>
        </tr>
        <tr>
            <th>%</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>20.17</th>
            <th>0.58</th>
            <th>0.29</th>
            <th></th>
            <th>16.71</th>
            <th>3.46</th>
            <th>0.86</th>
            <th></th>
            <th>4.9</th>
            <th>11.24</th>
            <th>3.46</th>
            <th>1.44</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
                                                                                <tr>
                <td><a href="#" title="Ir a detalle de Municipio" class="no-ajaxify">TUXTLA GUTIÉRREZ</a></td>
                <td>153</td>
                <td>350</td>
                <!-- <th>&nbsp;</th> -->
                <td>70</td>
                <td>2</td>
                <td>1</td>
                <td>0</td>
                <td>58</td>
                <td>12</td>
                <td>3</td>
                <td>0</td>
                <td>17</td>
                <td>39</td>
                <td>12</td>
                <td>5</td>
                <td>71</td>
                <td>62</td>
                <td>25</td>
                <td>68</td>
                <td>45</td>
                <td>28</td>
                <td>62</td>
                <td>29</td>
                <td>4</td>
                <td>4</td>
                <td>7</td>
                <td>19</td>
                <td>3</td>
                <td>27</td>
            </tr>
                                                                            </tbody>
</table>
</div>
</div>


@endsection @section('content_js')
{{-- <script src="{{ asset('js/planillas.js') }}"></script> --}}
 @stop