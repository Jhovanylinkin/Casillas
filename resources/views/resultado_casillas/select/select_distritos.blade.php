<div class="col-md-4" id="">
    <div class="form-group">
        <label class="control-label">Distrito
            <span class="required"> * </span>
        </label>
        <div class="input-icon left tooltips" data-placement="top">
            <i class="fa fa-th"></i>
            <select type="text" class="form-control" id="comboDistritoSelect" name="Distrito" required>
                <option value="" default>Distrito</option>
                @unless($DISTRITO->count()>0)
                    <option value="">No Distrito</option>
                @else
                    @if($DISTRITO->count()>=1)
                    <option value="" selected>Distrito</option>
                    @endif
                @foreach($DISTRITO as $D)
                    <option value="{{$D->Distrito}}">{{$D->Cabecera}}</option>
                @endforeach
                @endunless
            </select>
        </div>
    </div>
</div>