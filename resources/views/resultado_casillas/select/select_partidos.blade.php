@unless($Partidos->count()>0)
	<option value="">Sin partidos registrados</option>
@else
	@if($Partidos->count()>=1)
	<option value="" selected>Partido</option>
	@endif
 @foreach($Partidos as $C)
    <option value="{{ $C->CampoSave}}">{{ $C->Partido}}</option>
@endforeach
@endunless