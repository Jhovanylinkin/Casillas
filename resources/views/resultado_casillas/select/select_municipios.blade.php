@unless($Municipios->count()>0)
	<option value="">Sin Municipios</option>
@else
	@if($Municipios->count()>=1)
	<option value="" selected>Municipio</option>
	@endif
 @foreach($Municipios as $M)
    <option value="{{$M->Clave}}">{{$M->Municipio}}</option>
@endforeach
@endunless