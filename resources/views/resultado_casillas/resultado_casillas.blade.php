@extends('layouts.metronic') @section('Activo')
<a href="/casillas_resultados">Casillas resultados
    <span class="selected"></span>
</a>
@endsection 
@section('content')
<h1 class="page-title"> Casillas resultados
    <small></small> 
</h1>
<div class="container">
    <form id="capturaResultadosForm" method="POST" class="form">
    {{ csrf_field() }}
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Tipo de elección<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                    <i class="fa fa-cube"></i>
                    <select type="text" id="TipoEleccion" class="form-control" name="TipoEleccion" required>
                        <option value="0" default>Tipo Elección</option>
                        @forelse ($TipoE as $TE)
                        <option value="{{$TE->id}}">{{$TE->TipoEleccion}}</option>
                        @empty
                        <option value="">TipoEleccion is null</option>
                        @endforelse
                    </select>
                    </div>
                </div>
            </div>
            <div id="comboDistrito" style="display:none;">
                
            </div>
            <div class="col-md-4" id="comboMunicipio">
                <div class="form-group">
                    <label id="labelC" class="control-label">Municipio<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-map-marker"></i>
                        <select type="text" class="form-control" id="Municipio" name="Municipio" required>
                            <option value="" default>Municipio</option>
                            @forelse ($Muni as $M)
                            <option value="{{$M->Clave}}">{{$M->Municipio}}</option>
                            @empty
                            <option value="">Sin Municipios</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Casilla<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                    <i class="fa fa-cube"></i>
                    <select type="text" id="Casilla" class="form-control" name="Casilla" required>
                        <option value="" default>Casilla</option>
                        <option value="" style="color:red;" disabled>Seleccione el municipio</option>
                    </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Partido<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                    <i class="fa fa-circle"></i>
                    <select type="text" id="Partido" class="form-control" name="Partido" required>
                        <option value="" default>Partido</option>
                        <option value="" style="color:red;" disabled>Seleccione el municipio</option>
                    </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Total<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                    <i class="fa fa-cube"></i>
                    <input type="number" name="Total" class="form-control" placeholder="Total">
                    </div>
                </div>
            </div>
            <div class="col-md-2" id="">
                <div class="form-group">
                    <label class="control-label">&nbsp;</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <button style="float:right;" type="submit" class="btn btn-success mt-ladda-btn ladda-button" id="busqueda-filtro"
                            data-style="zoom-in" style="display: none;">
                            <span class="ladda-label">
                                <i class="fa fa-save"></i> Guardar</span>
                            <span class="ladda-spinner"></span>
                        </button>&nbsp
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('content_js')
<script src="{{ asset('js/jquery.bootstrap-growl.min.js') }}"></script>
<script src="{{ asset('js/casillas_resultados.js') }}"></script>
@stop