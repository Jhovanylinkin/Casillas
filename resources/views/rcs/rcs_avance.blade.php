@extends('layouts.metronic') @section('Activo')
<a href="/rcsavance">
    <span class="selected"> </span>
</a>
@endsection @section('content')
<div id="replacespace">
    <div class="container">
        {{--
        <h1>{{$casillas}}</h1> --}}
        <div class="row">
            <div class="row">
                <div class="col-md-9 text-center">
                    <div id="graph" style="width:70%; height: 400px; margin: 0 auto"></div>
                </div>
                <div style="margin-top:10%;" class="col-md-3">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                        <h4 class="widget-thumb-heading">Aspirantes registrados</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-blue fa fa-user"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle">Por asignar casilla</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{$ASPIRANTES}}">{{$ASPIRANTES}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Filtro de busqueda</span>
                    </div>
                    <div class="actions">&nbsp
                        @if($USRlgd->idTipoNivel == 2 || $USRlgd->idTipoNivel == 3)
                        <a href="/rcs_eliminated" style="margin-right:0px; float:right;" class="btn btn-transparent red btn-outline btn-circle btn-sm active">
                            Eliminados
                            <i class="fa fa-trash"></i>
                        </a>&nbsp
                        @endif
                        <a href="/rcs_aspirantes" style="margin-right:0px; float:right; color:white;" class="btn btn-transparent bg-success btn-outline btn-circle btn-sm">
                            Aspirantes
                            <i class="fa fa-user"></i>
                        </a>&nbsp
                        <a href="/rcs_add" style="margin-right:0px; float:right;" class="btn btn-transparent green btn-outline btn-circle btn-sm active">
                            Agregar
                            <i class="fa fa-plus"></i>
                        </a>
                        <button class="btn btn-transparent blue btn-outline btn-circle btn-sm active" id="abrir-filtro" type="button">Abrir Buscador
                            <i class="fa fa-search"></i>
                        </button>&nbsp
                        <button class="btn btn-transparent red btn-outline btn-circle btn-sm active" id="cerrar-filtro" type="button" style="display: none;">Cerrar Buscador
                            <i class="fa fa-times"></i>
                        </button>&nbsp
                    </div>
                </div>

                <form id="form-filtro" method="POST" class="form-horizontal" style="display: none;">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <button style="float:right;" type="submit" class="btn btn-warning mt-ladda-btn ladda-button btn-circle" id="busqueda-filtro"
                                data-style="zoom-in" style="display: none;">
                                <span class="ladda-label">
                                    <i class="icon-magnifier"></i> Realizar búsqueda</span>
                                <span class="ladda-spinner"></span>
                            </button>&nbsp
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nombre" class="control-label col-sm-4 col-xs-12">Nombre</label>
                                <div class="col-sm-8 col-xs-12">
                                    <input type="text" name="NOMBRE" id="NOMBRE" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="colonia" class="control-label col-sm-4 col-xs-12">INE</label>
                                <div class="col-sm-8 col-xs-12">
                                    <input type="text" name="INE" id="INEinput" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="seccion" class="control-label col-sm-4 col-xs-12">Sección</label>
                                <div class="col-sm-3 col-xs-12">
                                    <input type="text" name="SECCION" id="SECCIONinput" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-xs-12">Region</label>
                                <div class="col-sm-4 col-xs-12">
                                    <select style="width:100%;" class=" btn btn-default" data-label="left" data-height="300" name="REGION" id="REGIONselect">
                                        <option value="" selected>Region</option>
                                        @foreach($Regiones as $region)
                                        <option value="{{ $region->Region }}">{{ $region->Region }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="municipio" class="control-label col-sm-4 col-xs-12">Municipio</label>
                                <div class="col-sm-4 col-xs-12">
                                    <select style="width:100%;" class=" btn btn-default" data-label="left" data-height="300" name="MUNICIPIO" id="MUNICIPIOselect">
                                        <option value="" selected>Municipio</option>
                                        @if($USRlgd->idTipoNivel==4 || $USRlgd->idTipoNivel == 5 || $USRlgd->idTipoNivel == 6)
                                        <option value="{{$cat_municipios->Clave}}">{{$cat_municipios->Municipio}}</option>
                                        @else
                                            @foreach($cat_municipios as $municipio)
                                                <option value="{{$municipio->Clave}}">{{$municipio->Municipio}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="municipio" class="control-label col-sm-4 col-xs-12">Casilla</label>
                                <div class="col-sm-4 col-xs-12">
                                    <select style="width:100%;" class=" btn btn-default" data-label="left" data-height="300" name="CASILLA" id="CASILLASselect">
                                        <option value="" selected>Casilla</option>
                                        @foreach($Cat_Casillas as $casilla)
                                        <option value="{{ $casilla->casilla }}">{{ $casilla->casilla }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-8">
                            <div class="form-group form-md-radios">
                                <div class="col-md-9">
                                    <div class="md-radio-inline">
                                        <div class="md-radio" style="display:none;">
                                            <input type="radio" id="checkbox1_7" name="radio" value="0" class="md-radiobtn">
                                            <label for="checkbox1_7">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Sin Propietario </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_8" name="radio" value="1" class="md-radiobtn">
                                            <label for="checkbox1_8">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Sin Propietario </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_9" name="radio" value="2" class="md-radiobtn">
                                            <label for="checkbox1_9">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Sin Suplente </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_10" name="radio" value="3" class="md-radiobtn">
                                            <label for="checkbox1_10">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Sin propietario Sin Suplente </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_11" name="radio" value="4" class="md-radiobtn">
                                            <label for="checkbox1_11">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Se Ignora </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="portlet-body flip-scroll">
                    <div id="datosTablavancercs">

                    </div>
                </div>
                <div class="row">
                    {{-- ROW ONLY FOR DEVELOPMENT TEST --}}
                    <div class="col-md-1">
                        <strong>Nombre:</strong>
                    </div>
                    <div class="col-md-1">{{$USRlgd->Nombre}}</div>
                    <div class="col-md-1">
                        <strong>Nivel:</strong>
                    </div>
                    <div class="col-md-1">{{$USRlgd->idTipoNivel}}</div>
                    <div class="col-md-1">
                        <strong>Asignado:</strong>
                    </div>
                    <div class="col-md-1">{{$USRlgd->uidNivel}}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('content_js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.rcs.js')}}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="{{ asset('js/rcs_progress_graph.js')}}"></script> 
@stop