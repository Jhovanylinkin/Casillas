@extends('layouts.metronic') @section('Activo')
<a href="/cat_temas">Temas
    <span class="selected"> </span>
</a>
@endsection @section('content')
<form id="newrcs" method="POST" name="form23" class="horizontal-form">
    {{ csrf_field() }}
    <div class="form-body">
        <h3 class="card-title text-muted">Datos Personales</h3>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Municipio<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-home"></i>
                        <select class="form-control" name="ID_MUNICIPIO" id="MUNICIPIOselect" required>
                            @if($USRLGD->idTipoNivel!=5)
                            <option value="" disabled selected>Seleccione Municipio</option>
                                @if($USRLGD->idTipoNivel==4)
                                        <option value="{{$municipios->Clave}}">{{$municipios->Municipio}}</option>
                                    @else
                                    @foreach($municipios as $municipio)
                                        <option value="{{$municipio->Clave}}">{{$municipio->Municipio}}</option>
                                    @endforeach
                                @endif
                             @elseif($USRLGD->idTipoNivel=5) 
                                @foreach($municipios as $municipio)
                                <option value="{{$municipio->Clave}}">{{$municipio->Municipio}}</option>
                                @endforeach
                                <p>No hay contenido que mostrar</p>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Colonia <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-th"></i>
                        <select class="form-control" name="Colonia" id="Colonias" required>
                            <option value="" disabled selected>Seleccione la Colonia</option>
                            <option style="color:red;" value="" disabled>Seleccione primero Municipio</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Código postal</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-envelope-square"></i>
                        <input class="form-control" type="text" placeholder="Código postal" name="Cpostal" minlength="5" maxlength="6">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Calle <span class="required">*</span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-road"></i>
                        <input class="form-control uppercase" type="text" placeholder="Calle" name="Calle" required>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Número exterior <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-home"></i>
                        <input class="form-control" type="text" placeholder="Número exterior" name="NoExterior" required>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Número interior</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-home"></i>
                        <input class="form-control" type="text" placeholder="Número interior" name="NoInterior">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            {{--<div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">CURP</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-credit-card"></i>
                        <input class="form-control uppercase" type="text" name="CURP" placeholder="CURP" minlength="18" maxlength="18" value="">
                    </div>
                </div>
            </div>--}}
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Nombre <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-user"></i>
                        <input name="Nombre" class="form-control uppercase" type="text" placeholder="Nombre" required>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Apellido Paterno <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-user"></i>
                        <input name="Paterno" class="form-control uppercase" type="text" placeholder="Apellido Paterno" required>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Apellido Materno</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-user"></i>
                        <input name="Materno" class="form-control uppercase" type="text" placeholder="Apellido Materno">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">            
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Sexo <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-venus-mars"></i>
                        <select class="form-control" name="Sexo" required>
                            <option value="" disabled selected>Mujer/Hombre</option>
                            <option value="M">Mujer</option>
                            <option value="H">Hombre</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Fecha de Nacimiento</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-venus-mars"></i>
                        <input class="form-control form-control-inline date-picker" name="FechaNacimiento" type="date" value="">
                    </div>
                </div>
            </div>
        </div>
        <h3 class="card-title text-muted">Datos de Elector</h3>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Sección <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-th-large"></i>
                        <select class="form-control" name="SECCION" id="seccionIDD" required>
                            <option value="" disabled selected>Seleccione la Sección</option>
                            @forelse($Secciones as $seccion)
                            <option value="{{$seccion->SECCION}}">{{$seccion->SECCION}}</option>
                            @empty
                            <p>No hay contenido que mostrar</p>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Propietario</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-user"></i>
                        <select class="form-control" name="PROPIETARIO">
                            <option value="" disabled selected>Propietario o Suplente</option>
                            <option value="1">Propietario 1</option>
                            <option value="2">Propietario 2</option>
                            <option value="3">Suplente 1</option>
                            <option value="4">Suplente 2</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Clave Electoral <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa fa-credit-card"></i>
                        <input class="form-control uppercase"  name="INE" id="inputINE" minlength="18" maxlength="18" type="text" placeholder="Clave Electoral" required>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">OCR <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-qrcode"></i>
                        <input type="text" minlength="13" maxlength="13" class="form-control uppercase" name="OCR">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Casilla</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-cube"></i>
                        <select class="form-control" name="CASILLA" id="CASILLASselect" required>
                            <option value="" disabled selected>Seleccione la Casilla</option>
                            <option value="1" style="color:red;">Seleccione primero una sección</option>
                            @forelse($Casilla as $casilla)
                            <option value="{{$casilla->casilla}}">{{$casilla->casilla}}</option>
                            @empty
                            <p>No hay contenido que mostrar</p>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Distrito Federal</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-codepen"></i>
                        <input class="form-control" readonly name="DFederal" id="DFederal" placeholder="Distrito Federal" required>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Distrito Local</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-codepen"></i>
                        <input class="form-control" readonly name="DLocal" id="DLocal" placeholder="Distrito Local" required>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Partido Político</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-circle"></i>
                         <select class="form-control" name="idPartido" required>
                            <option class="form-control" value="form-control" disabled selected>Seleccione un elemento</option>
                            @forelse($Partidos as $p)
                                @if($p->Partido === "PAN")
                                    <option class="form-control" value="{{$p->id}}" selected>{{$p->Partido}}</option>
                                @else
                                    <option class="form-control" value="{{$p->id}}">{{$p->Partido}}</option>
                                @endif                            
                            @empty
                            <p>No hay contenido que mostrar</p>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <h3 class="card-title text-muted">Datos de Contacto</h3>
            </div>
            <div class="col-md-5">
                <h3 class="card-title text-muted">Comentarios</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Lada</label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-phone"></i>
                                <input name="LADA" class="form-control" type="text" placeholder="Lada" minlength="3" maxlength="3">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Telefono de Casa</label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-phone"></i>
                                <input name="TelCasa" class="form-control" type="text" placeholder="Telefono Celular" maxlength="10">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Telefono Celular <span class="required"> * </span></label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-mobile"></i>
                                <input name="Celular" class="form-control" type="text" placeholder="Telefono Celular" required minlength="10" maxlength="10">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">e-mail</label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-at"></i>
                                <input name="email" class="form-control" type="text" placeholder="email@example.com">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">FaceBook</label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-facebook-f"></i>
                                <input name="facebook" class="form-control" type="text" placeholder="FaceBook">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Twitter</label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-twitter"></i>
                                <input name="twitter" class="form-control" type="text" placeholder="@Twitter">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">Comentarios</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-commenting"></i>
                        <textarea name="Comentarios" rows="5" class="form-control uppercase" type="text" placeholder="Comentarios"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
                <div class="col-md-7"></div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Nombre de quien lo invitó</label>
                        <div class="input-icon left tooltips" data-placement="top">
                            <i class="fa fa-user"></i>
                            <input name="Promotor" class="form-control" type="text" placeholder="Nombre del promotor">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                    <label class="control-label">¿Recibió capacitación?</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-circle"></i>
                         <select class="form-control" name="Capacitacion">
                            <option class="form-control" value="0" disabled selected>¿Si/No?</option>
                            <option value="1">Si</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>
                </div>
        </div>
        <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-1">
                <button value="Refresh Page" onclick="goBack()" type="reset" style="float:right;" class="btn btn-danger">Volver</button>&nbsp;
            </div>
            <div class="col-md-1">
                <button style="float:right;" type="submit" class="btn btn-success">Guardar</button>&nbsp;
            </div>
        </div>
    </div>
</form>
<script>
    function goBack() {
        window.history.back();
    }
</script> @endsection 
@section('content_modals')
        <div class="modal fade bs-modal-lg in" id="ModalInfo" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title" id="ModalTitleInfo">Formulario</h4>
                    </div>
                    <div id="ModalBody" class="modal-body"></div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
@endsection 
@section('content_js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.rcs.js')}}"></script>
<script type="text/javascript">
    $(document).on('ready', function(){
        $("#idPartido").val().prop('selected', true);
    });
</script>
@stop