<table class="table table-bordered table-striped table-condensed flip-content" id="tablarcs">
    <thead class="flip-content">
        <tr>
            <th> Region. </th>
            <th width="12%"> Municipio </th>
            <th> Poligono </th>
            <th> Sección </th>
            <th> INE </th>
            <th> Nombre </th>
        </tr>
    </thead>
    <tbody>
        @foreach($Tabla as $Dato)
        <tr class="">
            <td>{{$Dato->Region}}</td>
            <td>{{$Dato->NombreMunicipio}}</td>
            <td>{{$Dato->POLIGONO}}</td>
            <td>{{$Dato->SECCION}}</td>
            <td id="editP1"> <a title="Editar">{{$Dato->INE}}</a></td>
            <td id="editP2"><a title="Editar">{{$Dato->Nombre}}</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<script>
    $('#tablarcs').on('click','#editP1', function(e) {
     let currow = $(this).closest('tr');
     var col1 = currow.find('td:eq(4)').text();
     replace(col1);
    });
    $('#tablarcs').on('click','#editP2', function(e) {
     let currow = $(this).closest('tr');
     var col1 = currow.find('td:eq(4)').text();
     replace(col1);
    });

    function replace(valuetofind) {
        $.ajax({
            url: '/rcs_replaceAspirantes',
            type: "post",
            dateType: 'json',
            data: ({ INE: valuetofind }),
            success: function(data) {
                let xD = $('#replacespace');
                xD.html(data);
            }
        })
    }
    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        var page = $(this).attr('href').split('?page=')[1];
        let $datosTabla = $('#aspirantesTable');
        let $filtro = $('#form-filtro');
        let XS = new FormData($filtro);
        let NOMBRE = $('#NOMBRE').val();
        let INE = $('#INEinput').val();
        let SECCION = $('#SECCIONinput').val();
        let REGION = $('#REGIONselect').val();
        let MUNICIPIO = $('#MUNICIPIOselect').val();
        let CASILLA = $('#CASILLASselect').val();
        let RADIO = $('input[name = radio]:checked').val();
        $.ajax({
            type: 'post',
            url: `/rcs_aspirantesfiltrotabla?page=${page}&NOMBRE=${NOMBRE}&INE=${INE}&SECCION=${SECCION}&REGION=${REGION}&MUNICIPIO=${MUNICIPIO}&CASILLA=${CASILLA}&radio=${RADIO}`,
            data: new FormData($filtro),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function (data) {
                $datosTabla.html(data)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
            }

        })
    });
</script>
{{$Tabla->links('pagination::bootstrap-4')}}