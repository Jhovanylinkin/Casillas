<table class="table table-bordered table-striped table-condensed flip-content" id="tablarcs">
    <thead class="flip-content">
        <tr>
            <th> Region. </th>
            <th width="12%"> Municipio </th>
            <th> Poligono </th>
            <th> Sección </th>
            <th> Casilla </th>
            <th> INE </th>
            <th> Nombre </th>
            <th> Motivo </th>
            <th> Restaurar </th>
        </tr>
    </thead>
    <tbody>
        @foreach($Tabla as $Dato)
        <tr class="">
            <td>{{$Dato->Region}}</td>
            <td>{{$Dato->NombreMunicipio}}</td>
            <td>{{$Dato->POLIGONO}}</td>
            <td>{{$Dato->SECCION}}</td>
            <td>{{$Dato->CASILLA}}</td>
            <td style="display:none;">{{$Dato->id}}</td>
            <td id="editP1">{{$Dato->INE}}</td>
            <td id="editP2">{{$Dato->Nombre}}</td>
            <td> {{$Dato->Motivo}} </td>
            <td>
                <a id="restorebtn" class="btn btn-outline btn-circle green btn-sm blue">
                    <i class="fa fa-share"></i> Restaurar 
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<script>
    $(document).ready(function() {
        $('#tablarcs').on('click', '#restorebtn', function (e) {
            let currow = $(this).closest('tr');
            var col1 = currow.find('td:eq(5)').text();
            replace(col1);
        });
    });

    function replace(valuetofind) {
        $.ajax({
            url: '/restorercs',
            type: 'POST',
            dateType: 'json',
            data: ({ ID: valuetofind }),
            success: function(data) {
                bootbox.alert(`<strong>Restaurado</strong><br><br><pre>${data.success}</pre>`); 
                get_datosTablaIni();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                bootbox.alert(`<strong>Ocurrio un error.</strong><br><br><pre>${errorThrown}</pre>`);
        }
        })
    }
    function get_datosTablaIni() {
        let $datosTabla = $('#datosTablavancercs');
        $.ajax({
            url: '/tablarcsdeleted',
            type: "GET",
            dateType: 'json',
            data: ({ sid: Math.random() }),
            success: function (data) {
                $datosTabla.html(data)
            }

        })
    }
    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        var page = $(this).attr('href').split('?page=')[1];
        let $datosTabla = $('#datosTablavancercs');
        $.ajax({
            type: 'post',
            url: `/tablarcsdeleted?page=${page}`,
            data: '',
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function (data) {
                $datosTabla.html(data)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
            }

        })
    });
</script>
{{$Tabla->links('pagination::bootstrap-4')}}