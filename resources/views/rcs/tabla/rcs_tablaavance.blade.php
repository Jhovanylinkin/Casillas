<table class="table table-bordered table-striped table-condensed flip-content" id="tablarcs">
    <thead class="flip-content">
        <tr>
            <th> Region. </th>
            <th width="12%"> Municipio </th>
            <th> Poligono </th>
            <th> Sección </th>
            <th> Casilla </th>
            <th> Propietario 1 </th>
            <th> Propietario 2 </th>
            <th> Suplente 1 </th>
            <th> Suplente 2 </th>
        </tr>
    </thead>
    <tbody>
        @foreach($Tabla as $Dato)
        <tr class="">
            <td>{{$Dato->Region}}</td>
            <td>{{$Dato->NombreMunicipio}}</td>
            <td>{{$Dato->POLIGONO}}</td>
            <td>{{$Dato->SECCION}}</td>
            <td>{{$Dato->CASILLA}}</td>
            <td id="editP1" style="cursor: pointer;" title="Editar"><a>{{$Dato->NombreP1}}</a></td>
            <td id="editP2" style="cursor: pointer;" title="Editar"><a>{{$Dato->NombreP2}}</a></td>
            <td id="editS1" style="cursor: pointer;" title="Editar"><a>{{$Dato->NombreS1}}</a></td>
            <td id="editS2" style="cursor: pointer;" title="Editar"><a>{{$Dato->NombreS2}}</a></td>
            <td style="display:none;">{{$Dato->INEP1}}</td>
            <td style="display:none;">{{$Dato->INEP2}}</td>
            <td style="display:none;">{{$Dato->INES1}}</td>
            <td style="display:none;">{{$Dato->INES2}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<script>
    $('#tablarcs').on('click','#editP1', function(e) {
     let currow = $(this).closest('tr');
     var col1 = currow.find('td:eq(9)').text();
     replace(col1);
    });
    $('#tablarcs').on('click','#editP2', function(e) {
     let currow = $(this).closest('tr');
     var col1 = currow.find('td:eq(10)').text();
     replace(col1);
    });
    $('#tablarcs').on('click','#editS1', function(e) {
     let currow = $(this).closest('tr');
     var col1 = currow.find('td:eq(11)').text();
     replace(col1);
    });
    $('#tablarcs').on('click','#editS2', function(e) {
     let currow = $(this).closest('tr');
     var col1 = currow.find('td:eq(12)').text();
     replace(col1);
    });

    function replace(valuetofind) {
        $.ajax({
            url: '/rcs_replace',
            type: "post",
            dateType: 'json',
            data: ({ INE: valuetofind }),
            success: function(data) {
                let xD = $('#replacespace');
                xD.html(data);
            }
        })
    }
    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        var page = $(this).attr('href').split('?page=')[1];
        let $datosTabla = $('#datosTablavancercs');
        let $filtro = $('#form-filtro');
        let XS = new FormData($filtro);
        let NOMBRE = $('#NOMBRE').val();
        let INE = $('#INEinput').val();
        let SECCION = $('#SECCIONinput').val();
        let REGION = $('#REGIONselect').val();
        let MUNICIPIO = $('#MUNICIPIOselect').val();
        let CASILLA = $('#CASILLASselect').val();
        let RADIO = $('input[name = radio]:checked').val();
        $.ajax({
            type: 'post',
            url: `/rcsavanc?page=${page}&NOMBRE=${NOMBRE}&INE=${INE}&SECCION=${SECCION}&REGION=${REGION}&MUNICIPIO=${MUNICIPIO}&CASILLA=${CASILLA}&radio=${RADIO}`,
            data: new FormData($filtro),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function (data) {
                $datosTabla.html(data)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
            }

        })
    });
</script>
{{$Tabla->links('pagination::bootstrap-4')}}
