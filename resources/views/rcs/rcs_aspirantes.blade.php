@extends('layouts.metronic') @section('Activo')
<a href="/rcs_aspirantes">RCS Aspirantes
    <span class="selected"> </span>
</a>
@endsection @section('content')
    <div id="replacespace">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2 class="bd-title" id="content">RCS Aspirantes</h2>
            </div>
            <div class="col-md-4">
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading">Aspirantes registrados</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-blue fa fa-user"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">Por asignar casilla</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{$ASPIRANTES}}">{{$ASPIRANTES}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <form id="form-filtro-aspirantes" method="POST" class="form-horizontal" style="">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nombre" class="control-label col-sm-4 col-xs-12">Nombre</label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" name="NOMBRE" id="NOMBRE" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="colonia" class="control-label col-sm-4 col-xs-12">INE</label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" name="INE" id="INEinput" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="seccion" class="control-label col-sm-4 col-xs-12">Sección</label>
                            <div class="col-sm-3 col-xs-12">
                                <input type="text" name="SECCION" id="SECCIONinput" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="municipio" class="control-label col-sm-4 col-xs-12">Region</label>
                            <div class="col-sm-4 col-xs-12">
                                <select style="width:100%;" class=" btn btn-default" data-label="left" data-height="300" name="REGION" id="REGIONselect">
                                    <option value="" selected>Region</option>
                                    @foreach($Regiones as $region)
                                    <option value="{{ $region->Region }}">{{ $region->Region }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="municipio" class="control-label col-sm-4 col-xs-12">Municipio</label>
                            <div class="col-sm-4 col-xs-12">
                                <select style="width:100%;" class=" btn btn-default" data-label="left" data-height="300" name="MUNICIPIO" id="MUNICIPIOselect">
                                    <option value="" selected>Municipio</option>
                                    @if($USRLGD->idTipoNivel==4 || $USRLGD->idTipoNivel==5 || $USRLGD->idTipoNivel==6)
                                        <option value="{{$cat_municipios->Clave}}">{{$cat_municipios->Municipio}}</option>
                                        @else
                                            @foreach($cat_municipios as $municipio)
                                                <option value="{{$municipio->Clave}}">{{$municipio->Municipio}}</option>
                                            @endforeach
                                        @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                        <button type="submit" class="btn btn-warning mt-ladda-btn ladda-button btn-circle" id="busqueda-filtro" data-style="zoom-in">
                        <span class="ladda-label">
                            <i class="icon-magnifier"></i> Realizar búsqueda</span>
                        <span class="ladda-spinner"></span>
                        </button>&nbsp
                        </div>
                    </div>
                </div>
            </form>
    </div>
    <div id="aspirantesTable"></div>
    </div>
@endsection
@section('content_js')
<script src="{{ asset('js/rcs_aspirantes.js') }}"></script>
<script src="{{ asset('js/jquery.rcs.js') }}"></script>
@stop