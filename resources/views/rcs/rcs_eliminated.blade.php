@extends('layouts.metronic') @section('Activo')
<a href="/rcs_eliminated">RCS eliminados
    <span class="selected"> </span>
</a>
@endsection @section('content')
    <h1>RCS Eliminados</h1>
    <div class="portlet-body flip-scroll">
        <div id="datosTablavancercs"></div>
    </div>
@endsection
@section('content_js')
<script src="{{ asset('js/jquery.rcs-eliminated.js') }}"></script>
@stop