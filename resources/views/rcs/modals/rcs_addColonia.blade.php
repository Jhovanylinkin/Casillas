@extends('layouts.metronic_modals')
@section('content_popup_modals')
    <form id="addColoniaModal" action="" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Municipio</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-home"></i>
                        <select class="form-control" name="CVE_MPIO" id="MUNICIPIOselectmodal" required>
                            <option value="" disabled selected>Seleccione Municipio</option>
                            @forelse($municipios as $municipio)
                            <option value="{{$municipio->Clave}}">{{$municipio->Municipio}}</option>
                            @empty
                            <p>No hay contenido que mostrar</p>
                            @endforelse 
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Sección</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-th-large"></i>
                        <select class="form-control" name="SECCION" id="seccionIDDmodal" required>
                            <option value="" disabled selected>Seleccione la Sección</option>
                            <option value="" style="color:red;">Seleccione primero el monucipio</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Nombre</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-th"></i>
                        <input class="form-control" type="text" placeholder="Nombre de la colonia" name="Colonia" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">  
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <button value="Refresh Page" id="saveColoniaModal" type="submit" style="float:right;" class="btn btn-success">Guardar</button>&nbsp;
            </div>
        </div>
    </form>
    <script>
            let municipio = $('#MUNICIPIOselectmodal');
                municipio.on('change', function () {
                if (municipio.val() !== '-1' && municipio.val() !== '') {
                    $.ajax({
                        url: '/seccionselect',
                        type: "POST",
                        dateType: 'json',
                        data: ({ municipioID: municipio.val() })
                    })
                        .done(function (response) {
                            let $seccionesSelect = $('#seccionIDDmodal');
                            if (response.status == 'success') {
                                $seccionesSelect.html(response.html);
                            }
                        }).fail(function (jqXHR, textStatus, error) {
                            console.log("Post error: " + error);
                        });
                }
            });
            $('#addColoniaModal').on('submit',function (e) {
                e.preventDefault();
                $.ajax({
                type: 'POST',
                url: '/savecolonia',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                timeout: 60000,
                success: function (data) {
                    console.log(data);
                    
                    alert('Se guardo correctamente ✔');
                    location.href=location.href
                }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    bootbox.alert(`<strong>Ocurrio un error.</strong><br><br><pre>${jqXHR.responseJSON.message}</pre>`);
                }
                });
            });
    </script>
@endsection
@section('content_popup_js')
@endsection
