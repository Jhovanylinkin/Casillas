@extends('layouts.metronic')
@section('Activo')
<a href="/rgsavance">
    <span class="selected"> </span>
</a>
@endsection
@section('content')
<div id="replacespace">
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-4">
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Filtro de busqueda RGS</span>
                </div>
                <div class="actions">
                    <a href="/rgs_add" id="nuevorgs" style="margin-right:0px; float:right;" class="btn btn-transparent green btn-outline btn-circle btn-sm active">
                        Agregar
                        <i class="fa fa-plus"></i>
                    </a>
                    <button class="btn btn-transparent blue btn-outline btn-circle btn-sm active" id="abrir-filtro" type="button" style="">Abrir Buscador
                        <i class="fa fa-search"></i>
                    </button>&nbsp;
                    <button class="btn btn-transparent red btn-outline btn-circle btn-sm active" id="cerrar-filtro" type="button" style="display: none;">Cerrar Buscador
                        <i class="fa fa-times"></i>
                    </button>&nbsp;
                </div>
            </div>
            <form id="form-filtro" method="POST" class="form-horizontal" style="display: none;">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <button style="float:right;" type="submit" class="btn btn-warning mt-ladda-btn ladda-button btn-circle" id="busqueda-filtro" data-style="zoom-in"
                        style="display: none;">
                        <span class="ladda-label">
                            <i class="icon-magnifier"></i> Realizar búsqueda</span>
                        <span class="ladda-spinner"></span>
                        </button>&nbsp
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nombre" class="control-label col-sm-4 col-xs-12">Nombre</label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" name="NOMBRE" id="NOMBRE" class="form-control uppercase">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="colonia" class="control-label col-sm-4 col-xs-12">INE</label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" name="INE" id="INEinput" class="form-control uppercase" minlength="18" maxlength="18">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="seccion" class="control-label col-sm-4 col-xs-12">Sección</label>
                            <div class="col-sm-3 col-xs-12">
                                <input type="text" name="SECCION" id="SECCIONinput" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-4 col-xs-12">Region</label>
                            <div class="col-sm-4 col-xs-12">
                                <select style="width:100%;" class=" btn btn-default" data-label="left" data-height="300" name="REGION" id="REGIONselect">
                                    <option value="" selected>Region</option>
                                    @foreach($Regiones as $region)
                                    <option value="{{ $region->Region }}">{{ $region->Region }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="Municipio" class="control-label col-sm-4 col-xs-12">Municipio</label>
                            <div class="col-sm-4 col-xs-12">
                                <select style="width:100%;" class=" btn btn-default" data-label="left" data-height="300" name="MUNICIPIO" id="MUNICIPIOselect">
                                    <option value="" selected>Municipio</option>
                                        @if($USRlgd->idTipoNivel == 4 || $USRlgd->idTipoNivel == 5|| $USRlgd->idTipoNivel == 6)
                                            <option value="{{$cat_municipios->Clave}}">{{$cat_municipios->Municipio}}</option>
                                        @else
                                            @foreach($cat_municipios as $municipio)
                                                <option value="{{$municipio->Clave}}">{{$municipio->Municipio}}</option>
                                            @endforeach
                                        @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div style="display:none;"  class="form-group">
                            <label for="municipio" class="control-label col-sm-4 col-xs-12">Casilla</label>
                            <div class="col-sm-4 col-xs-12">
                                <select style="width:100%;" class=" btn btn-default" data-label="left" data-height="300" name="CASILLA" id="CASILLASselect">
                                    <option value="" selected>Casilla</option>
                                    {{--  @foreach($Cat_Casillas as $casilla)
                                    <option value="{{ $casilla->casilla }}">{{ $casilla->casilla }}</option>
                                    @endforeach  --}}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                    </div>
                    <div class="col-md-5">
                        <div class="form-group form-md-radios">
                            <div class="">
                                <div class="md-radio-inline">
                                    <div class="md-radio" style="display:none;">
                                        <input type="radio" id="checkbox1_7" name="radio" value="0" class="md-radiobtn">
                                        <label for="checkbox1_7">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> Sin Propietario </label>
                                    </div>
                                    <div class="md-radio">
                                        <input type="radio" id="checkbox1_10" name="radio" value="3" class="md-radiobtn">
                                        <label for="checkbox1_10">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> Sin Asignar </label>
                                    </div>
                                    <div class="md-radio">
                                        <input type="radio" id="checkbox1_11" name="radio" value="4" class="md-radiobtn">
                                        <label for="checkbox1_11">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> Se Ignora </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="portlet-body flip-scroll">
                <div id="datosTablavancergs">

                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        {{-- ROW ONLY FOR DEVELOPMENT TEST --}}
        <div class="col-md-1">
            <strong>Nombre:</strong>
        </div>
        <div class="col-md-1">{{$USRlgd->Nombre}}</div>
        <div class="col-md-1">
            <strong>Nivel:</strong>
        </div>
        <div class="col-md-1">{{$USRlgd->idTipoNivel}}</div>
        <div class="col-md-1">
            <strong>Asignado:</strong>
        </div>
        <div class="col-md-1">{{$USRlgd->uidNivel}}</div>
    </div>
</div>
</div>
@endsection
@section('content_modals')
        <div class="modal fade bs-modal-lg in" id="ModalInfo" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="modalRGS" type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title" id="ModalTitleInfo">Formulario</h4>
                    </div>
                    <div id="ModalBody" class="modal-body"></div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
@endsection
@section('content_js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="{{ asset('js/jquery.rgs.js')}}"></script>
<script src="{{ asset('js/rgs-progress_graph.js')}}"></script>
@stop
