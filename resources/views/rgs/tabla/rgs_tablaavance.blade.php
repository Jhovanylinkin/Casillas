<table class="table table-bordered table-striped table-condensed flip-content" id="tablargs">
    <thead class="flip-content">
        <tr>
            <th> Region. </th>
            <th width="12%"> Municipio </th>
            <th> Poligono </th>
            <th> Sección </th>
            <th> Clave Electoral </th>
            <th> Nombre </th>
            {{-- <th> Tipo Casilla </th>  --}}
            <th> Casillas </th>
            <th> </th>

        </tr>
    </thead>
    <tbody>
        @foreach($Tabla as $Dato)
        <tr>
            <td class="uppercase">{{$Dato->Region}}</td>
            <td class="uppercase">{{$Dato->Municipio}}</td>
            <td >{{$Dato->POLIGONO}}</td>
            <td>{{$Dato->SECCION}}</td>
            <td id="editINEP" style="cursor: pointer;" title="Editar" class="uppercase"><a>{{$Dato->INE}}</a></td>
            <td id="editNOMBREP" style="cursor: pointer;" title="Editar" class="uppercase"><a>{{$Dato->Nombre}}</a></td>
            <td class=" text-center active">{{$Dato->TotalCasillas}}</td>
            <td class="text-center">
                @if($Dato->INE !== null)
                <a id="addCasillargsBtn" href="javascript:;" class="btn btn-outline btn-circle btn-sm purple"><i class="fa fa-plus"></i> Agregar casilla</a>
                @endif
            </td>
            <td style="display:none;">{{$Dato->id}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<script>
    let cpa = $('#tipocas');
    if (cpa.text()=='1'){
        cpa.text('Urbana');
    }
    $('#tablargs').on('click','#editINEP', function(e) {
     let currow = $(this).closest('tr');
     var col1 = currow.find('td:eq(4)').text();
     replace(col1);
    });
    $('#tablargs').on('click','#editNOMBREP', function(e) {
     let currow = $(this).closest('tr');
     var col1 = currow.find('td:eq(4)').text();
     replace(col1);
    });
    $('#tablargs').on('click','#addCasillargsBtn', function(e) {
        e.preventDefault();
        let currow = $(this).closest('tr');
        let col = currow.find('td:eq(8)').text();
        f_popup_info(col,'rgs_addcasilla','Asignar casilla a RG',2)
    });
    function replace(valuetofind) {
        $.ajax({
            url: '/rgs_toreplace',
            type: "post",
            dateType: 'json',
            data: ({ INE: valuetofind }),
            success: function(data) {
                let xD = $('#replacespace');
                xD.html(data);
            }
        })
    }
    function f_popup_info(data, url, title) { //Ver planilla 
    $("#ModalTitleInfo").html(title);
    $.ajax({
        type: 'POST',
        url: url,
        cache: false,
        data: ({ ID:data }),
        async: true,
        success: function (datos) {
            $("#ModalBody").html(datos);
            $('#ModalInfo').modal();
        },
        timeout: 60000,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
        }
    });
}
</script>
{{$Tabla->links('pagination::bootstrap-4')}}