@extends('layouts.metronic_modals')

@section('content_popup_modals')
    <form id="setcasillargs" action="" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">Región</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-th"></i>
                        <select class="form-control" name="REGION" id="RegionModal" required>
                            <option value="" disabled selected>Región</option>
                            @foreach($Regiones as $region)
                                <option value="{{ $region->Region }}">{{ $region->Region }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">Municipio</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-home"></i>
                        <select class="form-control" name="Municipio" id="MunicipiosModal" required>
                            <option value="" disabled selected>Municipio</option>
                            @if($USERLOGGED->idTipoNivel==4 || $USERLOGGED->idTipoNivel == 5 || $USERLOGGED->idTipoNivel == 6)
                                <option value="{{$cat_municipios->Clave}}">{{$cat_municipios->Municipio}}</option>
                            @else
                                @foreach($cat_municipios as $municipio)
                                    <option value="{{$municipio->Clave}}">{{$municipio->Municipio}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">Sección</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-th-large"></i>
                        <select class="form-control" name="SECCION" id="seccionIDD" required>
                            <option value="" disabled selected>Sección</option>
                            @foreach($Secciones as $secciones)
                            <option value="{{$secciones->SECCION}}">{{$secciones->SECCION}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="display:none;">
                <div class="form-group">
                    <label class="control-label">idRGS</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-th-large"></i>
                        <input class="form-control" style="" value="{{$idRGS}}" name="idRGS" type="text">
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">Casilla</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-cube"></i>
                        <select class="form-control" name="Casilla" id="CasillaModal" required>
                            <option value="" disabled selected>Casilla</option>
                           
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <br>
                   <button href="" type="submit" class="btn btn-success">Asignar</button>
                </div>
            </div>
        </div>
    </form>
    <script>
        let regiones  = $('#RegionModal');
            regiones.on('change', function () {
                if (regiones.val() !== '-1' && regiones.val() !== '') {
                    $.ajax({
                        url: '/regionesselect',
                        type: "POST",
                        dateType: 'json',
                        data: ({ regionID: regiones.val() })
                    }).done(function (response) {
                            let $municipios = $('#MunicipiosModal');
                            if (response.status == 'success') {
                                $municipios.html(response.html);
                            }
                        }).fail(function (jqXHR, textStatus, error) {
                            console.log("Post error: " + error);
                        });
                }
            });
        let municipio = $('#MunicipiosModal');
        let seccion = $('#seccionIDD');
        seccion.on('change', function () {
            if (seccion.val() !== '-1' && seccion.val() !== '') {
                $.ajax({
                    url: '/casillasselect',
                    type: "POST",
                    dateType: 'json',
                    data: ({ casillaID: seccion.val() })
                })
                    .done(function (response) {
                        let $municipios = $('#CasillaModal');
                        if (response.status == 'success') {
                            $municipios.html(response.html);
                        }
                    }).fail(function (jqXHR, textStatus, error) {
                        console.log("Post error: " + error);
                    });
            }
        });
        municipio.on('change', function () {
            if (municipio.val() !== '-1' && municipio.val() !== '') {
                $.ajax({
                    url: '/seccionselect',
                    type: "POST",
                    dateType: 'json',
                    data: ({ municipioID: municipio.val() })
                })
                    .done(function (response) {
                        let $seccionesSelect = $('#seccionIDD');
                        if (response.status == 'success') {
                            $seccionesSelect.html(response.html);
                        }
                    }).fail(function (jqXHR, textStatus, error) {
                        console.log("Post error: " + error);
                    });
            }
        });
        $('#setcasillargs').on('submit',function(e) {
            e.preventDefault();
            $.ajax({
            type: 'POST',
            url: '/rgs_setcasillas',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function (data) {
                bootbox.alert(`<strong></strong><br><br><h4>${data.message}</h4>`);
            }
        });
        });
    </script>
@endsection
@section('content_popup_js')
@endsection
