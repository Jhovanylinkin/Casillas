<form id="replacergs" method="POST" name="form23" class="horizontal-form">
    {{ csrf_field() }}
    <div class="form-body">
        <h3 class="card-title text-muted">Datos Personales</h3>
        <div style="display:none;" class="col-md-6">
                <div class="form-group">
                    <label class="control-label">INE</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-tasks"></i>
                        @forelse($TOEDIT as $edit)
                            <input class="form-control uppercase" value="{{$edit->INE}}" name="INEANTERIOR" id="inputINEAnterior" minlength="18" maxlength="18" type="text" placeholder="INE" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Municipio<span class="required">*</span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-home"></i>
                        <select class="form-control" name="ID_MUNICIPIO" id="MUNICIPIOselect" required>
                            @forelse($TOEDIT as $edit)
                                    <option value="{{$edit->ID_MUNICIPIO}}" selected>{{$edit->NombreMunicipio}}</option>
                                @empty
                            @endforelse
                            @if($USRLGD->idTipoNivel!=5)
                                @if($USRLGD->idTipoNivel==4 || $USRLGD->idTipoNivel==5)
                                        <option value="{{$municipios->Clave}}">{{$municipios->Municipio}}</option>
                                    @else
                                    @foreach($municipios as $municipio)
                                        <option value="{{$municipio->Clave}}">{{$municipio->Municipio}}</option>
                                    @endforeach
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Colonia<span class="required">*</span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-th"></i>
                        <select class="form-control" name="Colonia" id="Colonias" required>
                        @forelse($TOEDIT as $edit)
                        <option name="Colonia" value="{{$edit->Colonia}}" class="form-control" type="text" placeholder="Colonia" required>{{$edit->Colonia}}</option>
                        @empty
                        @endforelse
                        <option value=""></option>
                        <option value=""></option>
                        <option value=""></option>
                        <option value=""></option>
                        <option value=""></option>
                        <option value=""></option>
                        <option value=""></option>
                        <option value=""></option>
                        <option value=""></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Código postal</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-envelope-square"></i>
                        @forelse($TOEDIT as $edit)
                        <input class="form-control" value="{{$edit->Cpostal}}"  type="text" placeholder="Código postal" name="Cpostal" minlength="5" maxlength="6">
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Calle<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-road"></i>
                        @forelse($TOEDIT as $edit)
                        <input class="form-control uppercase" value="{{$edit->Calle}}" type="text" placeholder="Calle" name="Calle" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Número exterior<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-home"></i>
                        @forelse($TOEDIT as $edit)
                        <input class="form-control" value="{{$edit->NoExterior}}" type="text" placeholder="Número exterior" name="NoExterior" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Número interior</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-home"></i>
                        @forelse($TOEDIT as $edit)
                        <input class="form-control" value="{{$edit->NoInterior}}" type="text" placeholder="Número interior" name="NoInterior">
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">CURP<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-credit-card"></i>
                        @forelse($TOEDIT as $edit)
                        <input class="form-control uppercase" type="text" value="{{$edit->CURP}}" name="CURP" placeholder="CURP" minlength="18" maxlength="18" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Nombre<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-user"></i>
                        @forelse($TOEDIT as $edit)
                        <input name="Nombre" value="{{$edit->Nombre}}" class="form-control uppercase" type="text" placeholder="Nombre" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Apellido Paterno</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-user"></i>
                        @forelse($TOEDIT as $edit)
                        <input name="Paterno" value="{{$edit->Paterno}}" class="form-control uppercase" type="text" placeholder="Apellido Paterno" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Apellido Materno<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-user"></i>
                        @forelse($TOEDIT as $edit)
                        <input name="Materno" value="{{$edit->Materno}}" class="form-control uppercase" type="text" placeholder="Apellido Materno" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Sexo<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-venus-mars"></i>
                        <select class="form-control" name="Sexo" required>
                            @forelse($TOEDIT as $edit);
                            <option id="asdsexo" value="{{$edit->Sexo}}" selected></option>
                            @empty
                            @endforelse
                            <option value="M">Mujer</option>
                            <option value="H">Hombre</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Fecha de Nacimiento</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-venus-mars"></i>
                        @forelse($TOEDIT as $edit)
                        <input class="form-control form-control-inline date-picker" value="{{$edit->FechaNacimiento}}" name="FechaNacimiento" type="date" value="">
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
        <h3 class="card-title text-muted">Datos de Elector</h3>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Sección <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-th-large"></i>
                        <select class="form-control" name="SECCION" id="seccionIDD" required>
                            <option value="" disabled selected>Seleccione la Sección</option>
                            @forelse($TOEDIT as $edit);
                            <option value="{{$edit->SECCION}}" selected>{{$edit->SECCION}}</option>
                            @empty
                            @endforelse
                            @forelse($Secciones as $seccion)
                            <option value="{{$seccion->SECCION}}">{{$seccion->SECCION}}</option>
                            @empty
                            <p>No hay contenido que mostrar</p>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Clave Electoral <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa fa-credit-card"></i>
                        @forelse($TOEDIT as $edit)
                            <input class="form-control uppercase" value="{{$edit->INE}}" name="INE" id="inputINE" minlength="18" maxlength="18" type="text" placeholder="INE" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">OCR <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-qrcode"></i>
                        @forelse($TOEDIT as $edit)
                        <input type="text"  value="{{$edit->OCR}}" class="form-control uppercase" name="OCR" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Partido Político <span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-circle"></i>
                         <select class="form-control" name="idPartido" required>
                            <option class="form-control" value="1" selected>Seleccione un elemento</option>
                            @forelse($TOEDIT as $edit)
                                <option class="form-control" value="{{$edit->idPartido}}" selected>{{$edit->Partido}}</option>
                            @empty
                            @endforelse
                            @forelse($Partidos as $p)
                            <option class="form-control" value="{{$p->id}}">{{$p->Partido}}</option>
                            @empty
                            <p>No hay contenido que mostrar</p>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Distrito Federal</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-codepen"></i>
                        @forelse($TOEDIT as $edit)
                        <input class="form-control" readonly  value="{{$edit->DFederal}}" id="DFederal" name="DFederal" placeholder="Distrito Federal" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Distrito Local</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-codepen"></i>
                        @forelse($TOEDIT as $edit)
                        <input class="form-control" id="DLocal" readonly  value="{{$edit->DLocal}}" name="DLocal" placeholder="Distrito Local" required>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Casa Azul</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i style="color:blue;" class="fa fa-home"></i>
                        <select class="form-control" name="idCasaAzul" id="idCasaAzul">
                            @forelse($TOEDIT as $edit)
                                @if($edit->idCasaAzul===0)
                                    <option class="form-control" value="{{$edit->idCasaAzul}}" selected>Por definir</option>
                                @else
                                    <option class="form-control" value="{{$edit->idCasaAzul}}" selected>{{$edit->NombreCasaAzul}}</option>
                                @endif
                            @empty
                            @endforelse
                            <option class="form-controls" value="0" disabled>Casa Azul</option>
                            <option class="form-controls" value="0">Por definir</option>
                            @forelse($CasaAzul as $C)
                                <option class="form-control" value="{{$C->id}}">{{$C->NombreResponsable}}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Abogado</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-user"></i>
                        <select class="form-control" name="idAbogado" id="idAbogado">
                            @forelse($TOEDIT as $edit)
                                @if($edit->idAbogado===0)
                                    <option class="form-control" value="{{$edit->idAbogado}}" selected>Por definir</option>
                                @else
                                    <option class="form-control" value="{{$edit->idAbogado}}" selected>{{$edit->NombreAbogado}}</option>
                                @endif
                            @empty
                            @endforelse
                            <option class="form-controls" value="0" disabled>Casa Azul</option>
                            <option class="form-controls" value="0">Por definir</option>
                            @forelse($Abogados as $A)
                                <option class="form-control" value="{{$A->id}}">{{$A->Nombre}}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-7">
                <h3 class="card-title text-muted">Datos de Contacto</h3>
            </div>
            <div class="col-md-5">
                <h3 class="card-title text-muted">Comentarios</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Lada</label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-phone"></i>
                                @forelse($TOEDIT as $edit)
                                <input name="LADA" value="{{$edit->LADA}}" class="form-control" type="text" placeholder="Lada">
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Telefono de Casa</label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-phone"></i>
                                @forelse($TOEDIT as $edit)
                                <input name="TelCasa" value="{{$edit->TelCasa}}" class="form-control" type="text" placeholder="Telefono Celular">
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Telefono Celular<span class="required"> * </span></label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-mobile"></i>
                                @forelse($TOEDIT as $edit)
                                <input name="Celular" value="{{$edit->Celular}}" class="form-control" type="text" placeholder="Telefono Celular" required>
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">e-mail</label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-at"></i>
                                @forelse($TOEDIT as $edit)
                                <input name="email" value="{{$edit->email}}" class="form-control" type="text" placeholder="email@example.com">
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">FaceBook</label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-facebook-f"></i>
                                @forelse($TOEDIT as $edit)
                                <input name="facebook" value="{{$edit->facebook}}" class="form-control" type="text" placeholder="FaceBook">
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Twitter</label>
                            <div class="input-icon left tooltips" data-placement="top">
                                <i class="fa fa-twitter"></i>
                                @forelse($TOEDIT as $edit)
                                <input name="twitter" value="{{$edit->twitter}}" class="form-control" type="text" placeholder="@Twitter">
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">Comentarios</label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-commenting"></i>
                        @forelse($TOEDIT as $edit)
                        <textarea name="Comentarios" rows="5" value="{{$edit->Comentarios}}"  class="form-control" type="text" placeholder="Comentarios">{{$edit->Comentarios}}</textarea>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
                <div class="col-md-7"></div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">¿Esta persona fué promovida por alguien?</label>
                        <div class="input-icon left tooltips" data-placement="top">
                            <i class="fa fa-user"></i>
                            @forelse($TOEDIT as $edit)
                            <input name="Promotor" value="{{$edit->Promotor}}" class="form-control" type="text" placeholder="Nombre del promotor">
                            @empty
                            @endforelse    
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                    <label class="control-label">¿Recibió capacitación?<span class="required"> * </span></label>
                    <div class="input-icon left tooltips" data-placement="top">
                        <i class="fa fa-circle"></i>
                         <select class="form-control" name="Capacitacion" required>
                            @forelse($TOEDIT as $edit)
                            <option id="cpa" value="{{$edit->Capacitacion}}">{{$edit->Capacitacion}}</option>
                            @empty
                            @endforelse
                            <option class="form-control" value="0" disabled>¿Si/No?</option>
                            <option value="1">Si</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>
                </div>
        </div>
        <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-1">
                <button value="Refresh Page" onClick="location.href=location.href" type="reset" style="float:right;" class="btn btn-danger">Volver</button>&nbsp;
            </div>
            <div class="col-md-1">
                <button href="/rcsavance" style="float:right;" type="submit" class="btn btn-success">Guardar</button>&nbsp;
            </div>
        </div>
    </div>
</form>
<script>
    let cpa = $('#cpa');
    if (cpa.val()==1){
        cpa.text('Si');
        cpa.val('1');
    }
    if (cpa.val()==0){
        cpa.text('No');
        cpa.val('0');
    }
    let cslla=$('#casillaslected');
    if (cslla.val()==1) {
        cslla.text('Por definir');
        cslla.val('1');
    }
    let Sexo = $('#asdsexo')
    if (Sexo.val()=="M"){Sexo.text('Mujer');}else{Sexo.text('Hombre');}
    let municipio = $('#MUNICIPIOselect');
    
        let a = true;
    $('#CASILLASselect').on('click',
    function CASILLAinit (){
        if (a) {
            a=false;
            $.ajax({
                url: '/casillasselect',
                type: "POST",
                dateType: 'json',
                data: ({ casillaID: $('#seccionIDD').val() })
            })
                .done(function (response) {
                    let $municipios = $('#CASILLASselect');
                    if (response.status == 'success') {
                        $municipios.html(response.html);
                    }
                }).fail(function (jqXHR, textStatus, error) {
                    console.log("Post error: " + error);
                });
        }
    });
    let b = true;
    $('#seccionIDD').on('click',
    function SECCIONinit(){
        if (b) {
            b=false
         $.ajax({
                url: '/seccionselect',
                type: "POST",
                dateType: 'json',
                data: ({ municipioID: municipio.val() })
            })
                .done(function (response) {
                    let $seccionesSelect = $('#seccionIDD');
                    if (response.status == 'success') {
                        $seccionesSelect.html(response.html);
                    }
                }).fail(function (jqXHR, textStatus, error) {
                    console.log("Post error: " + error);
                });   
        }
    });

    let seccion = $('#seccionIDD');
    seccion.on('change', function () {
        if (seccion.val() !== '-1' && seccion.val() !== '') {
            $.ajax({
                url: '/casillasselect',
                type: "POST",
                dateType: 'json',
                data: ({ casillaID: seccion.val() })
            })
                .done(function (response) {
                    let $municipios = $('#CASILLASselect');
                    if (response.status == 'success') {
                        $municipios.html(response.html);
                    }
                }).fail(function (jqXHR, textStatus, error) {
                    console.log("Post error: " + error);
                });
        }
    });
    municipio.on('change', function () {
        if (municipio.val() !== '-1' && municipio.val() !== '') {
            $.ajax({
                url: '/seccionselect',
                type: "POST",
                dateType: 'json',
                data: ({ municipioID: municipio.val() })
            })
                .done(function (response) {
                    let $seccionesSelect = $('#seccionIDD');
                    if (response.status == 'success') {
                        $seccionesSelect.html(response.html);
                    }
                }).fail(function (jqXHR, textStatus, error) {
                    console.log("Post error: " + error);
                });
        }
    });
    let SeccionforDFDL = $('#seccionIDD');
    SeccionforDFDL.on('change',function (e) {
        if (SeccionforDFDL.val() !== '-1' && SeccionforDFDL.val() !== '') {
        $.ajax({
            url: '/dfflselect',
            type: "POST",
            dateType: 'json',
            data: ({ SECCION: SeccionforDFDL.val() })
        }).done(
            function (response) {
                $('#DFederal').val(response.df.DISTRITO);
                $('#DLocal').val(response.df.DL);
            }
        ).fail(
            function (jqXHR, textStatus, error) {
                bootbox.alert(`<strong>Ocurrio un error.</strong><br><br><pre>${error}</pre>`);
            }
            
        );
    }
    });
    let c = true;
    $('#Colonias').on('click',function (e) {
        e.preventDefault();
        if (c) {
            c=false;
          if (municipio.val() !== '-1' && municipio.val() !== '') {
            $.ajax({
                url: '/colonias',
                type: "POST",
                dateType: 'json',
                data: ({ municipioID: municipio.val() })
            })
                .done(function (response) {
                    let $Colonias = $('#Colonias');
                    if (response.status == 'success') {
                        $Colonias.html(response.html);
                    }
                }).fail(function (jqXHR, textStatus, error) {
                    console.log("Post error: " + error);
                });
        }  
        }
    });
    municipio.on('change', function(event) {
        if (municipio.val() !== '-1' && municipio.val() !== '') {
            $.ajax({
                url: '/colonias',
                type: "POST",
                dateType: 'json',
                data: ({ municipioID: municipio.val() })
            })
                .done(function (response) {
                    let $Colonias = $('#Colonias');
                    if (response.status == 'success') {
                        $Colonias.html(response.html);
                    }
                }).fail(function (jqXHR, textStatus, error) {
                    console.log("Post error: " + error);
                });
        }
    });
    $('#Colonias').on('change',function (event) {
        let x = $('#Colonias');
        if (x.val()==1) {
            console.log('funcional el js');
            
            f_popup_info('', '/addcolonia','Agregrar Colonia');
        }
    });
    
</script>