@unless($Municipios->count()>0)
	<option value="">Sin Municipios</option>
@else
	@if($Municipios->count()>=1)
	<option value="" selected>Municipio</option>
	<option value="0" style="color:red;">PARA TODOS</option>
	@endif
 @foreach($Municipios as $M)
    <option value="{{ $M->Clave}}">{{ $M->Municipio}}</option>
@endforeach
@endunless
