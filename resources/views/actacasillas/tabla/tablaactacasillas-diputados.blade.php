<table class="table table-bordered table-striped table-condensed flip-content" id="tablaactacasillas">
    <thead class="flip-content">
    {{--<th>N°</th>--}}
    <th>Tipo de elección</th>
    <th>Partido</th>
    <th>Distrito</th>
    <th>Cabecera</th>
    <th>Municipio</th>
    <th></th>
    </thead>
    <tbody>
    @foreach ($Tabla as $T)
        <tr>
            {{--<td width="10%">{{++$loop->index}}</td>--}}
            <td>{{$T->TipoEleccion}}</td>
            <td>{{$T->Partido}}</td>
            <td>{{$T->Distrito}}</td>
            <td>{{$T->Cabecera}}</td>
            <td>{{$T->Municipio}}</td>
            <td class="text-center">
                @if($T->Partido !== null)
                    <a id="DeleteBoleta" href="javascript:;" class="btn btn-outline btn-circle btn-sm red"><i class="fa fa-trash"></i> Eliminar</a>
                @endif
            </td>
            <td class="idTE" style="display:none;">{{$T->idTE}}</td>
            <td class="idMun" style="display:none;">{{$T->idM}}</td>
            <td class="idPartido" style="display:none;">{{$T->idPartido}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
{{$Tabla->links('pagination::bootstrap-4')}}
<script >
    $('#tablaactacasillas').on('click','#DeleteBoleta',function(ev) {
        ev.preventDefault();
        let currow = $(this).closest('tr');
        let TipoEleccion = currow.find('td.idTE').text();
        let Municipio = currow.find('td.idMun').text();
        let Partido = currow.find('td.idPartido').text();
        deletee(TipoEleccion,Municipio,Partido);
    })

    function deletee(TipoEleccion,Municipio,Partido) {
        $.ajax({
            url: '/acta_casillas/delete',
            type: "post",
            dateType: 'json',
            data: ({
                TipoEleccion:TipoEleccion,
                Municipio:Municipio,
                Partido:Partido
            }),
            success: function (data) {
                Glow(`${data.message}`, 'warning');
                get_datosTablaIni();
            },
            error: function(data) {
                Glow(`[${data.status}] Ocurrió un error al eliminar el elemento`, 'danger');
            }
        });
    }
</script>