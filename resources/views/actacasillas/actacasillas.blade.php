@extends('layouts.metronic')
@section('Activo')
<a href="/acta_casillas">
    <span class="selected">Acta casillas</span>
</a>
@endsection
@section('content')
  <div class="container">
    <h1>Acta casillas</h1>
    <form id="actacasillasForm" class="form" method="post">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
              <label class="control-label">Tipo de elección<span class="required"> * </span></label>
              <div class="input-icon left tooltips" data-placement="top">
                  <i class="fa fa-cube"></i>
                  <select type="text" id="TipoEleccion" class="form-control" name="TipoEleccion" required>
                    <option value="" default>Tipo Elección</option>
                    @forelse ($TipoE as $TE)
                      <option value="{{$TE->id}}">{{$TE->TipoEleccion}}</option>
                    @empty
                      <option value="">TipoEleccion is null</option>
                    @endforelse
                  </select>
              </div>
          </div>
        </div>
        <div class="col-md-4" id="comboMunicipio">
          <div class="form-group">
              <label id="labelC" class="control-label">Municipio<span class="required"> * </span></label>
              <div class="input-icon left tooltips" data-placement="top">
                  <i class="fa fa-map-marker"></i>
                  <select type="text" class="form-control" id="Municipio" name="Municipio" required>
                    <option value="" default>Municipio</option>
                    <option style="color:red;" value="0">PARA TODOS</option>
                    @forelse ($Muni as $M)
                      <option value="{{$M->Clave}}">{{$M->Municipio}}</option>
                    @empty
                      <option value="">Sin Municipios</option>
                    @endforelse
                  </select>
              </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
              <label class="control-label">Partido político<span class="required"> * </span></label>
              <div class="input-icon left tooltips" data-placement="top">
                  <i class="fa fa-circle"></i>
                  <select type="text" class="form-control" id="Partido" name="Partido" required>
                    <option value="" default>Partido político</option>
                    @forelse ($Partidos as $P)
                      @if($P->isCoalicion == 1)
                      <option value="{{$P->id}}">Coalicón - {{$P->Partido}}</option>
                      @else
                      <option value="{{$P->id}}">Partido - {{$P->Partido}}</option>
                      @endif
                    @empty
                      <option value="">Sin Partidos</option>
                    @endforelse
                  </select>
              </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
        </div>
        <div class="col-md-1">
          <div  style="margin-left:100%;" class="form-group">
              <label class="control-label"></label>
              <div style="color:#ffffff;" class="input-icon left tooltips" data-placement="top">
                  <i class="fa fa-plus"></i>
                  <button type="post" class="btn green" name="button">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agregar</button>
              </div>
          </div>
        </div>
      </div>
    </form>
    <div class="portlet-body flip-scroll">
        <div id="ActadeResultados">

        </div>
    </div>
  </div>
@endsection
@section('content_js')
<script src="{{ asset('js/jquery.bootstrap-growl.min.js') }}"></script>
<script src="{{ asset('js/actacasillas.js') }}"></script>
@endsection
