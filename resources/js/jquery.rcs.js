//funcion para realzar el submit en el formulario de rcs
$(document).ready(function () {
    $('.datepicker').datepicker();
    var urlZ = $(location).attr('href').split('rcsa')[1];
    if (urlZ == 'vance') {
        get_datosTablaIni();
    }
    $(document).on('submit', '#newrcs', function (e) {
        e.preventDefault();
        if ($('#CASILLASselect').val()==1) {
            $.ajax({
                type: 'POST',
                url: '/rcs_addAspirante',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                timeout: 60000,
                success: function (data) {
                    parent.bootbox.alert(`<strong>Perfecto!</strong><br><br><pre>${data.success}</pre>`);
                    clearForm();
                }});
            
        }else{
        $.ajax({
            type: 'POST',
            url: '/rcs_add',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function (data) {
                parent.bootbox.alert(`<strong>Perfecto!</strong><br><br><pre>${data.success}</pre>`);
                clearForm();
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                parent.bootbox.alert(`<strong>Ocurrió un error.</strong><br><br><pre>${jqXHR.responseJSON.message}</pre>`);
            }
        });
        }
    });
    $(document).on('submit', '#replacercs', function (e) {
        e.preventDefault();
        if ($('#CASILLASselect').val() == 1) {
            bootbox.alert('<h1>Ya se encuentra registrado en aspirantes.</h1>');
        }
        if ($('#aspitante').val() == 1 && $('#CASILLASselect').val() != 1) {
            $.ajax({
                type: 'POST',
                url: '/rcs_add',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                timeout: 60000,
                success: function (data) {
                    parent.bootbox.alert(`<strong>Perfecto!</strong><br><br><pre>${data.success}</pre>`);
                    clearForm();
                }
            });
        }
        else{
        $.ajax({
            type: 'POST',
            url: '/rcs_savereplace',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function (data) {
                parent.bootbox.alert(`<strong>Mensage</strong><br><br><pre>${data.success}</pre>`);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                parent.bootbox.alert(`<strong>Ocurrió un error.</strong><br><br><pre>${jqXHR.responseJSON.message}</pre>`);
            }
        });
        }
    });
    
    $(document).on('click', '#abrir-filtro', function (e) {
        let formulariobusqueda = document.getElementById('form-filtro');
        let buttoncerrar = document.getElementById('cerrar-filtro');
        let btnbusqueda = document.getElementById('busqueda-filtro');
        formulariobusqueda.style.display = "";
        buttoncerrar.style.display = "";
        btnbusqueda.style.display = "";
        let button = this;
        button.style.display = "none";
    });
    $(document).on('click', '#cerrar-filtro', function (e) {
        let formulariobusqueda = document.getElementById('form-filtro');
        let buttonabrir = document.getElementById('abrir-filtro');
        let btnbusqueda = document.getElementById('busqueda-filtro');
        buttonabrir.style.display = "";
        formulariobusqueda.style.display = "none";
        let button = this;
        button.style.display = "none";
        btnbusqueda.style.display = "none";
    });
    let regiones = $('#REGIONselect');
    regiones.on('change', function () {
        if (regiones.val() !== '-1' && regiones.val() !== '') {
            $.ajax({
                url: '/regionesselect',
                type: 'POST',
                dateType: 'json',
                data: ({ regionID: regiones.val() })
            }).done(function (response) {
                    let $municipios = $('#MUNICIPIOselect');
                    if (response.status == 'success') {
                        $municipios.html(response.html);
                    }
                }).fail(function (jqXHR, textStatus, error) {
                    console.log("Post error: " + error);
                });
        }
    });
    let seccion = $('#seccionIDD');
    seccion.on('change', function () {
        if (seccion.val() !== '-1' && seccion.val() !== '') {
            $.ajax({
                url: '/casillasselect',
                type: "POST",
                dateType: 'json',
                data: ({ casillaID: seccion.val() })
            })
                .done(function (response) {
                    let $municipios = $('#CASILLASselect');
                    if (response.status == 'success') {
                        $municipios.html(response.html);
                    }
                }).fail(function (jqXHR, textStatus, error) {
                    console.log("Post error: " + error);
                });
        }
    });

    let municipio = $('#MUNICIPIOselect');
    municipio.on('change', function () {
        if (municipio.val() !== '-1' && municipio.val() !== '') {
            $.ajax({
                url: '/seccionselect',
                type: "POST",
                dateType: 'json',
                data: ({ municipioID: municipio.val() })
            })
                .done(function (response) {
                    let $seccionesSelect = $('#seccionIDD');
                    if (response.status == 'success') {
                        $seccionesSelect.html(response.html);
                    }
                }).fail(function (jqXHR, textStatus, error) {
                    console.log("Post error: " + error);
                });
        }
    });
    municipio.on('change', function(event) {
        if (municipio.val() !== '-1' && municipio.val() !== '') {
            $.ajax({
                url: '/colonias',
                type: "POST",
                dateType: 'json',
                data: ({ municipioID: municipio.val() })
            })
                .done(function (response) {
                    let $Colonias = $('#Colonias');
                    if (response.status == 'success') {
                        $Colonias.html(response.html);
                    }
                }).fail(function (jqXHR, textStatus, error) {
                    console.log("Post error: " + error);
                });
        }
    });
    let SeccionforDFDL = $('#seccionIDD');
    SeccionforDFDL.on('change',function (e) {
        if (SeccionforDFDL.val() !== '-1' && SeccionforDFDL.val() !== '') {
        $.ajax({
            url: '/dfflselect',
            type: "POST",
            dateType: 'json',
            data: ({ SECCION: SeccionforDFDL.val() })
        }).done(
            function (response) {
                $('#DFederal').val(response.df.DISTRITO);
                $('#DLocal').val(response.df.DL);
            }
        ).fail(
            function (jqXHR, textStatus, error) {
                parent.bootbox.alert(`<strong>Ocurrió un error.</strong><br><br><pre>${error}</pre>`);
            }
            
        );
    }
    });

    $('#Colonias').on('change',function (event) {
        let x = $('#Colonias');
        if (x.val()==1) {
            f_popup_info('', '/addcolonia','Agregrar Colonia');
        }
    });

    $(document).on('submit', '#form-filtro', function (e) {
        let $datosTabla = $('#datosTablavancercs');
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/rcsavanc',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function (data) {
                $datosTabla.html(data)
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                parent.bootbox.alert(`<strong>Ocurrió un error.</strong><br><br><pre>${jqXHR.responseJSON.message}</pre>`);
            }
        });
    });
});
function get_datosTablaIni() {
    let $datosTabla = $('#datosTablavancercs');
    $.ajax({
        url: '/rcsavanc',
        type: "GET",
        dateType: 'json',
        data: ({ sid: Math.random() }),
        success: function (data) {
            $datosTabla.html(data)
        }

    })
}
function f_popup_info(data, url, title) { //Ver planilla 
    $("#ModalTitleInfo").html(title);
    $.ajax({
        type: 'POST',
        url: url,
        cache: false,
        data: ({ ID: data }),
        async: true,
        success: function (datos) {
            $("#ModalBody").html(datos);
            $('#ModalInfo').modal();
        },
        timeout: 60000,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            parent.bootbox.alert("<strong>Ocurrió un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
        }
    });
}
function clearForm(){
    let urlxD = $(location).attr('href').split('rcs')[1];
    if (urlxD == '_add') {
        document.getElementById('newrcs').reset();
    }
}