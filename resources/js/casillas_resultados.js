$(document).ready(() => {
    $("#TipoEleccion").on('change', ev => {
        if ($("#TipoEleccion").val() == 3 || $("#TipoEleccion").val() == 5) {
            $("#TipoEleccion").val() == 3 ? getDistritos(27,'FEDERAL') : getDistritos(27,'LOCAL');
        }else{
        $.ajax({
                url: "/casillas_resultados/Municipios",
                type: "POST",
                dateType: "json",
                data: {
                    TipoEleccion: $('#TipoEleccion').val()
                }
            }).done(function (response) {
                $("#Municipio").html(response.html);
            }).fail(function (jqXHR, textStatus, error) {Glow(`${error}`, "danger");});
        }
    });
    $("#Municipio").on("change", ev => {
        if ($("#TipoEleccion").val() == 0) {
            Glow("Se necesita el tipo de elección", 'danger');
        }else{
            $.ajax({
              url: "/casillas_resultados/Casillas",
              type: "POST",
              dateType: "json",
              data: {
                  Municipio:$('#Municipio').val(),
                  TipoEleccion:$('#TipoEleccion').val()
              }
            })
              .done(function(response) {
                  $("#Casilla").html(response.html);
              })
              .fail(function(jqXHR, textStatus, error) {
                Glow(`${error}`, "danger");
              });
            // Partidos 
            $.ajax({
                url: "/casillas_resultados/partidos",
                type: "POST",
                dateType: "json",
                data: {
                    Municipio: $('#Municipio').val(),
                    TipoEleccion: $('#TipoEleccion').val()
                }
            })
                .done(function (response) {
                    $("#Partido").html(response.html);
                })
                .fail(function (jqXHR, textStatus, error) {
                    Glow(`${error}`, "danger");
                });
        }
    });

    $("#capturaResultadosForm").submit(ev =>{
        ev.preventDefault();
        $.ajax({
          type: "POST",
          url: "/casillas_resultados/storageResults",
          data: new FormData(document.getElementById('capturaResultadosForm')),
          contentType: false,
          cache: false,
          processData: false,
          timeout: 60000
        }).done(response =>{
            Glow(`${response.message}`,`${response.status}`);
            }).fail((jqXHR, textStatus, error)=>{
                Glow(`${error}`, "danger");
        });
    });
});
function getDistritos(Estado,Distrito) {
    $.ajax({
        url: "/casillas_resultados/Distritos",
        type: "POST",
        dateType: "json",
        data: {
            ESTADO:Estado,
            DISTRITO:Distrito 
        }
    }).done(function (response) {
        $("#comboDistrito").html(response.html);
    }).fail(function (jqXHR, textStatus, error) {
        Glow(`${error}`, "danger");
    });
}

function Glow(Message, TipeMessage) {
  setTimeout(function() {
    $.bootstrapGrowl(
      `${Message}`,
      {
        ele: "body", // which element to append to
        type: `${TipeMessage}`, // (null, 'info', 'danger', 'success')
        offset: { from: "top", amount: 130 }, // 'top', or 'bottom'
        align: "right", // ('left', 'right', or 'center')
        width: 300, // (integer, or 'auto')
        delay: 2000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
        allow_dismiss: false, // If true then will display a cross to close the popup.
        stackup_spacing: 10 // spacing between consecutively stacked growls.
      },
      100
    );
  });
}
