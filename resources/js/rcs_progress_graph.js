let a;
$.ajax({
    type: 'GET',
    url: '/chartrcs',
    contentType: false,
    cache: false,
    processData: false,
    timeout: 60000,
    success: function (data) {
        a=data;
    },
    async: false
});
Highcharts.chart('graph', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Avance Estructura Electoral'
    },
    xAxis: {
        categories: ['RC P1', 'RC P2', 'RC S1', 'RC S2','PAN','PRD','MC']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total avance'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
            }
        }
    },
    series: [{
        name: 'Capturado',
        data: [a.Propietario1, a.Propietario2, a.Suplente1, a.Suplente2, a.PAN, a.PRD,a.MVC]
    }, {
        name: 'No Capturado',
        data: [a.TotalRCS - a.Propietario1, a.TotalRCS - a.Propietario2, a.TotalRCS - a.Suplente1, a.TotalRCS - a.Suplente2,0,0,0]
    }]
});
