$(document).ready(function () {
    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        var page = $(this).attr('href').split('?page=')[1];
        let $datosTabla = $('#datosTablaplanillas');
        let tipoE = $('#tiposdeelecciones').val();
        let estadoE = $('#ESTADO').val();
        let municipioE = $('#municipios').val();
        let partidoP = $('#partidos').val();
        $.ajax({
            type: 'post',
            url: `/planillatablainit?page=${page}&TIPOELECCION=${tipoE}&ESTADO=${estadoE}&MUNICIPIO_DISTRITO=${municipioE}&PARTIDO=${partidoP}`,
            data: '',
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function (data) {
                $datosTabla.html(data)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
            }

        })
    });
    $('#ESTADO').on('change', function (ev) {
        let val = $('#ESTADO');
        if (selecttipoelecciones.val() == 'DIPUTADO FEDERAL' || selecttipoelecciones.val() == 'DIPUTADO LOCAL') {
            $.ajax({
                url: '/planillasdiputados',
                type: 'POST',
                dateType: 'json',
                data: ({
                    DIPUTADO: selecttipoelecciones.val() ,
                    ESTADO: val.val(),
                })
            }).done(function (response) {
                $('#fxmunicipio').text('Distritos');
                $('#municipios').html(response.html);
            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        }
        else{
        $.ajax({
            url: '/planillasfiltroEstadoM',
            type: 'POST',
            dateType: 'json',
            data: ({ ESTADO: val.val() })
        }).done(function (response) {
            $('#fxmunicipio').text('Municipios');
            $('#municipios').html(response.html);

        }).fail(function (jqXHR, textStatus, error) {
            console.log("Post error: " + error);
        });}
    });

    let selecttipoelecciones = $('#tiposdeelecciones');
    selecttipoelecciones.on('change',function (ev) {
        $.ajax({
            url: '/partidosslect',
            type: 'POST',
            dateType: 'json',
            data: ({ TIPOELECCION: selecttipoelecciones.val() })
        }).done(function (response) {
            $('#partidos').html(response.html);

        }).fail(function (jqXHR, textStatus, error) {
            console.log("Post error: " + error);
        });
        if (selecttipoelecciones.val() == 'PRESIDENTE REPUBLICA') {
            filtroPR();
        }
        if (selecttipoelecciones.val() == 'DIPUTADO FEDERAL') {
            $('#municipios').html('<option value="" selected>Distritos</option><option style="color:red;" value="" disabled>Seleccione primero el estado</option>');
            $('#fxmunicipio').text('Distritos');
            filtroDF();
        }
        if (selecttipoelecciones.val() == 'DIPUTADO LOCAL') {
            $('#municipios').html('<option value="" selected>Distritos</option><option style="color:red;" value="" disabled>Seleccione primero el estado</option>');
            $('#fxmunicipio').text('Distritos');
            filtroDF();
        }
        if (selecttipoelecciones.val() == 'PRESIDENTE MUNICIPAL') {
            $('#municipios').html('<option value="" selected>Municipios</option><option style="color:red;" value="" disabled>Seleccione primero el estado</option>');
            $('#fxmunicipio').text('Municipios');
            filtroDF();
        }
        if (selecttipoelecciones.val() == 'GOBERNADOR ') {
            filtroGB();
        }
        if (selecttipoelecciones.val() == 'SENADOR') {
            filtroSENADOR();
        }
    });

    $('#filtroplanillas').on('submit',function (ev) {
        ev.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/planillatablainit',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function (data) {
                let table = $('#datosTablaplanillas');
                table.html(data);
            }
        })
        
    });
    $(document).on('submit','#formEXCEL',function (ev) {
        ev.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/cargarplanillas',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            async:true,
            beforeSend: function () {
                App.blockUI({
                    target: "#planillascontainer",
                    message: `<img src="https://cdn.dribbble.com/users/600626/screenshots/2944614/loading_12.gif" width="50px" height="50px" alt="">
                    Actualizando la tabla `,
                    boxed: !0
                });
            },
            success: function (data) {
                App.unblockUI("#planillascontainer")
                bootbox.alert(`<strong>Perfecto!</strong><br><br><pre>${data.success}</pre>`);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                App.unblockUI("#planillascontainer")
                bootbox.alert(`<strong>Ocurrio un error.</strong><br><br><pre>${errorThrown}</pre>`);
            }
        });
    });
});

tablaplanillas();
function tablaplanillas() {
    $.ajax({
        type: 'GET',
        url: '/planillatablainit',
        success: function (datos) {
            let table = $('#datosTablaplanillas');
            table.html(datos);

        },
        timeout: 60000,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Error al cargar tabla planillas</pre>");
        }
    });
}
function f_popup_info(ID, url, title, TIPOELECCION, CARGO, ESTADO, MUNICIPIO_DISTRITO, PARTIDO) { //Ver planilla 
    $("#ModalTitleInfo").html(title);
    $.ajax({
        type: 'POST',
        url: url,
        cache: false,
        data: ({
            ID: ID,
            TIPOELECCION: TIPOELECCION,
            CARGO: CARGO,
            ESTADO: ESTADO,
            MUNICIPIO_DISTRITO: MUNICIPIO_DISTRITO,
            PARTIDO: PARTIDO
        }),
        async: true,
        beforeSend: function () {
            App.blockUI({
                target: "#datosTablaplanillas",
                message: `<img src="https://cdn.dribbble.com/users/600626/screenshots/2944614/loading_12.gif" width="50px" height="50px" alt="">
                Cargando planilla`,
                boxed: !0
            });
        },
        success: function (datos) {
            $("#ModalBody").html(datos);
            $('#ModalInfo').modal();
            App.unblockUI("#datosTablaplanillas")
        },
        timeout: 60000,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            App.unblockUI("#datosTablaplanillas")
            bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
        }
    });
}

let selectestados = document.getElementById('filtro2');
let selectmunicipios = document.getElementById('filtro3');
function filtroPR(){
    selectestados.style.display='none';
    selectmunicipios.style.display='none';
    restoreSelects();
}
function filtroDF() {
    selectestados.style.display = '';
    selectmunicipios.style.display = '';
    defaultGuanajuato();
}
function filtroGB() {
    selectestados.style.display ='';
    selectmunicipios.style.display = 'none';
    defaultGuanajuato();
}
function filtroSENADOR() {
    selectestados.style.display = 'none';
    selectmunicipios.style.display = 'none';
    restoreSelects();
}
function defaultGuanajuato() {
    $('#ESTADO').val(11);
    let selecttipoelecciones = $('#tiposdeelecciones');
    if (selecttipoelecciones.val() == 'DIPUTADO FEDERAL' || selecttipoelecciones.val() == 'DIPUTADO LOCAL') {
        $.ajax({
            url: '/planillasdiputados',
            type: 'POST',
            dateType: 'json',
            data: ({
                DIPUTADO: selecttipoelecciones.val(),
                ESTADO: 11,
            })
        }).done(function (response) {
            $('#fxmunicipio').text('Distritos');
            $('#municipios').html(response.html);
        }).fail(function (jqXHR, textStatus, error) {
            console.log("Post error: " + error);
        });
    } else {
        $.ajax({
            url: '/planillasfiltroEstadoM',
            type: 'POST',
            dateType: 'json',
            data: ({
                ESTADO: 11
            })
        }).done(function (response) {
            $('#fxmunicipio').text('Municipios');
            $('#municipios').html(response.html);

        }).fail(function (jqXHR, textStatus, error) {
            console.log("Post error: " + error);
        });
    }
}
function restoreSelects(){
    $('#ESTADO').val($('#ESTADO > option:first').val());
    $('#municipios').val($('#municipios > option:first').val());
    $('#partidos').val($('#partidos > option:first').val());
}