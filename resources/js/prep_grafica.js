"use strict";
$(document).ready(() => {
  $("#prepform").on("submit", ev => {
    ev.preventDefault();
    $.ajax({
      url: "/prep_grafica/tabla",
      type: "POST",
      dateType: "json", 
      data: {
        TipoEleccion: $("#TipoEleccion").val(),
        Municipio: $("#Municipio").val()
      }
    })
      .done(function(response) {
        postChart();
        $("#tablePrep").html(response);
      })
      .fail(function(jqXHR, textStatus, error) {
        console.log("Post error: " + error);
        Glow(`Ocurrio un error al cargar los datos[${error}]`, `danger`);
      });
  });

    $("#TipoEleccion").on("change", function (ev) {
        if ($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 4) {
            $("#labelC").text("Municipio");
            rollbackfilter();
            document.getElementById("Municipio").required = false;
            getMunicipios(27);
        }
        if ($(this).val() == 3) {
            $("#labelC").html('Distritos federales <span class="required"> * </span>');
            rollbackfilter();
            document.getElementById("Municipio").required = true;
            getDFLC("DIPUTADO FEDERAL", 27);
        }
        if ($(this).val() == 5) {
            $("#labelC").html('Distritos locales <span class="required"> * </span>');
            rollbackfilter();
            document.getElementById("Municipio").required = true;
            getDFLC("DIPUTADO LOCAL", 27);
        }
        if ($(this).val() == 6) {
            $("#labelC").html('Municipio <span class="required"> * </span>');
            document.getElementById("Municipio").required = true;
            rollbackfilter();
            getMunicipios(27);
        }
    });
});
function getDFLC(DIPUTADO, ESTADO) {
  $.ajax({
    url: "/prep_grafica/diputados",
    type: "POST",
    dateType: "json",
    data: {
      DIPUTADO: DIPUTADO,
      ESTADO: ESTADO
    }
  })
    .done(function(response) {
      $("#Municipio").html(response.html);
    })
    .fail(function(jqXHR, textStatus, error) {
      Glow(`${error}`, "danger");
    });
}
function getMunicipios(ESTADO) {
  $.ajax({
    url: "/prep_grafica/municipios",
    type: "POST",
    dateType: "json",
    data: {
      ESTADO: ESTADO
    }
  })
    .done(function(response) {
      $("#Municipio").html(response.html);
    })
    .fail(function(jqXHR, textStatus, error) {
      Glow(`${error}`, "danger");
    });
}
function rollbackfilter() {
    document.getElementById("comboMunicipio").style.display = "";
    document.getElementById("Municipio").disabled = false;
}
function Glow(Message, TipeMessage) {
  setTimeout(function() {
    $.bootstrapGrowl(
      `${Message}`,
      {
        ele: "body", // which element to append to
        type: `${TipeMessage}`, // (null, 'info', 'danger', 'success')
        offset: { from: "top", amount: 130 }, // 'top', or 'bottom'
        align: "right", // ('left', 'right', or 'center')
        width: 300, // (integer, or 'auto')
        delay: 2000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
        allow_dismiss: false, // If true then will display a cross to close the popup.
        stackup_spacing: 10 // spacing between consecutively stacked growls.
      },
      100
    );
  });
}
//////////////////////////////////////////////////JS for table
$(document).on('click', '.pagination a', function (e) {
  e.preventDefault();
  let TipoEleccion = $("#TipoEleccion").val();
  let Municipio = $("#Municipio").val();
  var page = $(this).attr('href').split('?page=')[1];
  $.ajax({
    type: 'POST',
    dateType: "json",
    url: `prep_grafica/tabla?page=${page}`,
    data: {
      TipoEleccion: $("#TipoEleccion").val(),
      Municipio: $("#Municipio").val(),
      Partido: $("#Partido").val()
    },
    timeout: 60000,
    success: function (data) {
      $('#tablePrep').html(data);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      bootbox.alert("<strong>Ocurrio un error.</strong><br><br><pre>Intente de Nuevo ha excedido el limite de Tiempo</pre>");
    }
  });
});